//js_system.js

module.exports.InitSystem = function()
{
	Object.prototype.Inh = function( parent )
	{
		if( arguments.length > 1 )
		{
			parent.apply( this, Array.prototype.slice.call( arguments, 1 ) );
		}
		else
		{
			parent.call( this );
		}
	}

	Function.prototype.Inh = function( parent )
	{
		this.prototype = new parent();
		this.prototype.constructor = this;
	}   
}