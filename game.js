//game.js

var uPlayer = require('./player')
var uPrefs = require('./preferences')
var uMisc = require('./misc')
var uMsg = require('./message')
var uDbg = require('./debug')
var uLogic = require('./logic')
var uConsts = require('./constants')
var uEf = require('./abilities/effects')
var uGlobalEf = require('./abilities/globalEffects')
var uGameProfile = require('./profile/gameProfile')
var uAbility = require('./abilities/ability')
var uAffect = require('./abilities/abilityAffect')
var uMt19937 = require('./mMt19937')
var uItem = require('./abilities/item')
var uMongo = require('./mongo.js')

//var uCreateAbility = require('./abilities/createAbility')

var ex = module.exports

var _lastGameId = 10

ex.MOVES_FOUND = 0
ex.NO_MOVES_ON_END_TURN = 1
ex.NO_MOVES_AFTER_REMOVAL_ABILITY = 2
ex.NO_MOVES_AFTER_OTHER_ABILITY = 3

function PrintStartGameMessageContents(m)
{
   for (var i = 0; i < uPrefs.fieldHeight; i++)
   {
      var str = "";
      for (var j = 0; j < uPrefs.fieldWidth; j++)
      {
         if (m.m_data.field == undefined || m.m_data.field[j] == undefined || m.m_data.field[j][i] == undefined)
            str += "N "
         else
            str += String(m.m_data.field[j][i].type)+" "
      }
      uDbg.Log(str,"\n")
   }   
}

ex.TURN_PHASE_NORMAL = 0
ex.TURN_PHASE_PASSING_TURN = 1

ex.Game = function(plrs)
{
   this.sd = uMisc.GetServerTimeMsecs()
   this.mt = new uMt19937.mt(this.sd)
   this.players = plrs
   this.pickPhase = true
   this.turnsSkipped = [ 0, 0 ]
   
   if (this.players[0].profile.tut && this.players[1].ai)
   {
      this.tut = true
      this.ss = uConsts.BALL_BLACK
      this.soA = false //stone of attack
   }
   else
   {
      this.CreateSixthGem()
      //this.ss = uPrefs.allSS
   }
   if (this.players[1].ai)
      this.players[1].socket = this.players[0].socket
   for (var i = 0; i < this.players.length; i++)
   {
      if (i === 0)
         var enemy = this.players[1]
      else
         enemy = this.players[0]
      var pl = this.players[i]
      pl.ballDamageAddition = []
      pl.ballDamageMultiplier = []
      pl.ballManaAddition = []
      pl.ballManaMultiplier = []
      pl.effects = []
      
      for (var j = 0; j < uConsts.NUMBER_OF_BALL_TYPES; j++)
      {            
         pl.ballDamageAddition.push(0)
         pl.ballDamageMultiplier.push(1)
         pl.ballManaAddition.push(0)
         pl.ballManaMultiplier.push(1)
      }           
      pl.number = i
      if (!pl.ai)
      {
         if (!enemy.monster)
            pl.profile.gp++
         pl.SaveProfile()
      }
      

      uDbg.Log("adding player",i,this.players[i].id)
      pl.hp = this.players[i].hpMax
      pl.repicksLeft = this.players[i].profile.rp
      pl.shield = 0
      pl.repickedEnemyAbility = false
      pl.hasFreeEnemyAbilityRepick = false
      pl.repickedYourItem = false
      pl.ready = false
      pl.outOfSync = false
      for (var j = 0; j < pl.mana.length; j++)
         pl.mana[j] = 0
      
      if (pl.item && pl.item.ability.abilityData.chargeable)
      {
         pl.item.ability.chargeNow = 0
      }
   }
   
   this.id = _lastGameId++
   this.inProgress = true
   this.lastTurn = false
   
   //log
   var plrs = [ this.players[0].id, this.players[1].id ]
   
   if (this.players[0].monster)
      plrs[0] = "mon"
   else if (this.players[0].ai)
      plrs[0] = "ai"   
   if (this.players[1].monster)
      plrs[1] = "mon"
   else if (this.players[1].ai)
      plrs[1] = "ai"
   
   this.logDataObj = { gid : this.id, plrs : plrs, start : uMisc.GetServerTime() }

   this.logMsg = []   
   
   if (this.tut)
      this.BeginTutorial()
   else
      this.BeginPick()   
}

ex.Game.prototype.LogMsg = function(msg)
{
   if (this.players[0] && this.players[1])
      msg = "tr "+ String(this.turnNumber) + " | hp1: " + String(this.players[0].hp) + ", hp2: " + String(this.players[1].hp) +" | " + msg
   console.log('msg:',msg)
   this.logMsg.push(msg)
}

ex.Game.prototype.CreateSixthGem = function()
{
   //Sixth Gem
   var lastRan = uConsts.NUMBER_OF_BALL_TYPES
   if (this.players[0].profile.lv >= uPrefs.noSSLevel && (this.players[1].ai || this.players[1].profile.lv >= uPrefs.noSSLevel))
      lastRan++
   // if (this.players[0].profile.lv >= uPrefs.allSSLevel && (this.players[1].ai || this.players[1].profile.lv >= uPrefs.allSSLevel))
      // lastRan++
   this.ss = uMisc.Random(uConsts.BALL_BLACK, lastRan)
   if (this.ss === uConsts.NO_SS_DICE)
      this.ss = uPrefs.noSS
   else if (this.ss === uConsts.ALL_SS_DICE)
      this.ss = uPrefs.allSS
   
   //stone of attack
   if (this.players[0].profile.lv >= uPrefs.stoneOfAttackLevel && (this.players[1].ai || this.players[1].profile.lv >= uPrefs.stoneOfAttackLevel))
   {
      if (uMisc.ThrowDice(uPrefs.stoneOfAttackPresenceChance))
      {
         this.soA = true
      }
      else
      {
         this.soA = false
      }
   }
}

ex.Game.prototype.MtRandom = function(min,max)
{
   return (this.mt.genrand_int32() % (max-min))+min
}

ex.Game.prototype.CreateRandomAbilities = function()
{
   for (var i = 0; i < this.players.length; i++)
   {
      var pl = this.players[i]
      pl.abilities = []
      if (!pl.monster)
      {
         for (var j = 0; j < uPrefs.numberOfManas; j++)
         {
            var ab = uAbility.CreateRandomAbility(j,pl.abilities,pl.ai)
            pl.AddAbility(ab)
         }
         
         if (pl.ai)
         {
            pl.AddItem(uItem.GetRandomAiItem())
         }
      }
      else
      {
         for (var j = 0; j < pl.absNum; j++)
         {          
            ab = uAbility.CreateRandomAbility(j,pl.abilities,pl.ai,pl.possAbs)
            pl.AddAbility(ab)
         }
      }
      
      if (pl.item)
      {
         pl.item.ability.used = false
         pl.item.ability.consumed = false
      }
   }
}

ex.Game.prototype.CreateAbilities = function()
{
   var abs = 
   [ 
      [  { t : uAbility.ABILITY_SHATTER_COLUMN, c : uConsts.BALL_RED }, 
         { t : uAbility.ABILITY_CHAIN_LIGHTNING, c : uConsts.BALL_PURPLE }, 
         { t : uAbility.ABILITY_SHATTER_ROW, c : uConsts.BALL_BLUE },
         { t : uAbility.ABILITY_SHATTER, c : uConsts.BALL_GREEN } ],
         
      [  { t : uAbility.ABILITY_SHATTER_COLUMN, c : uConsts.BALL_RED }, 
         { t : uAbility.ABILITY_FREEZING_RAIN, c : uConsts.BALL_PURPLE }, 
         { t : uAbility.ABILITY_SHATTER_ROW, c : uConsts.BALL_BLUE },
         { t : uAbility.ABILITY_SHATTER, c : uConsts.BALL_GREEN } ]
   ]
   for (var i = 0; i < this.players.length; i++)
   {
      if (i === 0)
         var item = new uItem.Item(uItem.ITEM_SHIELD)
      else
         item = new uItem.Item(uItem.ITEM_SYMBOL_OF_SNOW)
      
      //var item = new uItem.Item(uItem.ITEM_AXE)
      
      this.players[i].AddItem(item)
      for (var j = 0; j < abs[i].length; j++)
      {
         var ab = new uAbility.Ability(uAbility._abilityDatas[abs[i][j].t],null,abs[i][j].c)
         this.players[i].AddAbility(ab)
      }
      
      if (this.players[i].item)
      {
         this.players[i].item.ability.used = false
         this.players[i].item.ability.consumed = false
      }      
   }
}

ex.Game.prototype.CreateTutorialAbilities = function()
{
   var abs = 
   [ 
      [  { t : uAbility.ABILITY_METEORITE_RAIN, c : uConsts.BALL_RED }, 
         { t : uAbility.ABILITY_FREEZE, c : uConsts.BALL_PURPLE }, 
         { t : uAbility.ABILITY_SHIELD_OF_ICE, c : uConsts.BALL_BLUE },
         { t : uAbility.ABILITY_MATCH_THREE, c : uConsts.BALL_GREEN } ],
         
      // [  { t : uAbility.ABILITY_METEORITE_RAIN, c : uConsts.BALL_RED }, 
         // { t : uAbility.ABILITY_FREEZE, c : uConsts.BALL_PURPLE }, 
         // { t : uAbility.ABILITY_SHIELD_OF_ICE, c : uConsts.BALL_BLUE },
         // { t : uAbility.ABILITY_MATCH_THREE, c : uConsts.BALL_GREEN } ]
   ]   

   for (var i = 0; i < this.players.length; i++)
   {
      if (!this.players[i].ai)
      {
         for (var j = 0; j < abs[i].length; j++)
         {
            var ab = new uAbility.Ability(uAbility._abilityDatas[abs[i][j].t],null,abs[i][j].c)
            this.players[i].AddAbility(ab)
         }  
      }
   }
}

ex.Game.prototype.BeginTutorial = function()
{
   this.CreateTutorialAbilities()
   this.startPick = 0
   var msg = new uMsg.Message(uMsg.Names.kGameMessage, uMsg.Names.ksTutorialStart, 
   { 
      sd: this.sd,
      pl : [ this.players[0].GetSendInfo(), this.players[1].GetSendInfo() ],
      id: this.id, 
      tut: true 
   }) 
   
   this.globalEffects = []

   this.field =
   [
      [ 2, 2, 2, 5, 5, 5 ],
      [ 1, 3, 3, 4, 0, 0 ],
      [ 0, 0, 2, 4, 5, 0 ],
      [ 1, 1, 1, 2, 5, 0 ],
      [ 0, 3, 4, 2, 2, 0 ],
      [ 0, 2, 4, 5, 5, 2 ]
   ]
   this.aiTurns = [ [ [ 0, 5 ], [ 0, 4 ], [ 0, 3 ] ] ]
   for (var i = 0; i < this.field.length; i++)
   {
      for (var j = 0; j < this.field[i].length; j++)
      {
         this.field[i][j] = new Ball(this.field[i][j],i,j,this,0)
      }
   }
   this.turn = 0      
   
   this.lastSelected = undefined
   this.selectedBalls = []
   this.endTime = null
   this.startTime = uMisc.GetServerTime()
   this.targetingAbility = null
   this.turnPhase = ex.TURN_PHASE_NORMAL
   this.turnNumber = 1
   this.SetTurnStartTime()
   
   msg.m_data.ss = this.ss
   msg.m_data.soA = this.soA  //if stone of attack is present
   msg.m_data.firstTurn = this.turn
   msg.m_data.field = this.field
   msg.m_data.maxHP = uPrefs.maxHP
   msg.m_data.g = this.id
   //msg.m_data.abs = []
   msg.m_data.st = this.turnStartTime
     
   for (var i = 0; i < this.players.length; i++)
   {
      for (var j = 0; j < this.players.length; j++)
      {
         // msg.m_data.abs.push([]) 
         // for (var key in this.players[j].abs)
         // {
            // msg.m_data.abs[j].push(this.players[j].abs[key].type)         
         // }
         if (j !== i)
            msg.m_data.enemyProfile = this.players[j].profile
         else
            msg.m_data.yourProfile = this.players[j].profile       
      }
      msg.m_data.aiTurns = this.aiTurns
      msg.m_data.n = i
      uDbg.Log("sending message: ",msg)
      this.players[i].inGame = this.id
      uMsg.SendMessage(this.players[i], msg)
   }
   this.InitializePassives()
   this.InitializaItemAbilities()
   uDbg.Log('turn:',this.turn)
   ex.PrintFieldToConsole(this.field)         
}

ex.Game.prototype.BeginPick = function()
{
    this.pickPhase = true
    
   //test
   
   this.CreateRandomAbilities()
   //this.CreateAbilities()
   
   this.startPick = uMisc.GetServerTime()
   if (this.players[0].monster || this.players[1].monster)
      this.againstMonster = true
   else
      this.againstMonster = false
   
   for (i = 0; i < this.players.length; i++)
   {
      var msg = new uMsg.Message(uMsg.Names.kGameMessage, uMsg.Names.ksOpponentFound, 
      { 
         sd: this.sd, 
         pl : [ this.players[0].GetSendInfo(), this.players[1].GetSendInfo() ],
         st: this.startPick, 
         id: this.id,
         ss: this.ss,
         am: this.againstMonster,
         soA: this.soA
      })
      if (this.tut)
         msg.m_data.tut = true      
      msg.m_data.n = i
      uDbg.Log("sending message: ",msg)
      this.players[i].inGame = this.id
      uMsg.SendMessage(this.players[i], msg)
   }
   
   this.logDataObj.stp_msg = msg
   
   this.InitializaItemAbilities()
   this.pickTO = setTimeout(this.Begin.bind(this),uPrefs.pickTimeout*1000)
}

ex.Game.prototype.Begin = function()
{
   clearTimeout(this.pickTO)
   this.pickPhase = false
   uDbg.Log('this.players length: ',this.players.length)
   uDbg.Log("number of players passed: ",this.players.length)
   uDbg.Log("Started game #",this.id,"between player",this.players[0].id,"and player",this.players[1].id) 
   
   this.globalEffects = []
   
   this.turn = uMisc.Random(0,this.players.length) 
   this.CreateField()
   this.lastSelected = undefined
   this.selectedBalls = []
   
   this.endTime = null
   this.startTime = uMisc.GetServerTime()
   this.targetingAbility = null
   this.turnPhase = ex.TURN_PHASE_NORMAL
   this.turnNumber = 1
   this.SetTurnStartTime()
   
   for (var i = 0; i < this.players.length; i++)
   {   
      uDbg.Log("Start game message arguments:",this.id, this.turn, this.field)     
      var msg = new uMsg.GameStartMessage(i, this)
      //PrintStartGameMessageContents(msg)
      this.LogMsg("sending start game msg to player "+String(this.players[i].id))
      uMsg.SendMessage(this.players[i], msg)
   }
   
   this.logDataObj.stg_msg = msg
   
   this.InitializePassives()
   this.TickEffects(true)
   this.TickEffects(false)
   uDbg.Log('turn:',this.turn)
   ex.PrintFieldToConsole(this.field)              
}

ex.Game.prototype.InitializaItemAbilities = function()
{
   
}

ex.Game.prototype.SetTurnStartTime = function()
{
   if (!this.tut)
   {
      this.turnStartTime = uMisc.GetServerTime()
      this.turnTimeoutId = setTimeout(this.OnTurnTimeout.bind(this),uPrefs.turnTimeout*1000)
   }
}

ex.Game.prototype.OnTurnTimeout = function()
{
   this.DeselectAllBalls()
   var pl = this.players[this.turn]
   this.LogMsg("player id " + String(pl.id) + " turn timeout")
   this.turnsSkipped[this.turn]++
   pl.Damage(this.turnsSkipped[this.turn]*uPrefs.damageOnTurnTimeout)
   this.NewTurn(true)
}


ex.Game.prototype.RefreshAbilities = function()
{
   for (var i = 0; i < this.players.length; i++)
   {
      var pl = this.players[i]
      if (pl.item)
      {
         pl.item.ability.used = false
         if (i === this.turn)
         {
            if (pl.item.ability.abilityData.chargeable)
            {
               pl.item.ability.chargeNow++
            }
            if (pl.item.ability.chargeNow > pl.item.ability.abilityData.charge)
            {
               pl.item.ability.chargeNow = pl.item.ability.abilityData.charge
            }
         }
      }
      for (var key in pl.abilities)
      {
         pl.abilities[key].used = false
      }
   }
}

// ex.Game.prototype.UtilizePassives = function(timeType)
// {
   // for (var i = 0; i < this.players.length; i++)
   // {
      // for (var key in this.players[i].abilities)
      // {
         // var ab = this.players[i].abilities[key]
         // if (ab.actType === uAbility.ACTIVATION_TYPE_PASSIVE_ON_YOUR_TURN || ab.actType === uAbility.ACTIVATION_TYPE_PASSIVE_ON_ENEMY_TURN)
         // {
            
         // }
         // if (ab.actType === uAbility.ACTIVATION_TYPE_PASSIVE_BEFORE_PICK || ab.actType === uAbility.ACTIVATION_TYPE_PASSIVE_AFTER_PICK)
         // {
            // ab.Use(this)
         // }
      // }
   // }
// }


ex.Game.prototype.InitializePassives = function()
{              
   // for (var i = 0; i < this.players.length; i++)
   // {
      // var rage = null
      // var numberOfRages = 0
      // for (var key in this.players[i].abilities)
      // {
         // var ab = this.players[i].abilities[key]
         // if (ab.passive && ab.subType === "rage")
         // {
            // numberOfRages++
            // rage = ab
         // }
      // }
      // if (numberOfRages >= 2)
         // rage.Use(null,this,-1)   
      // for (var key in this.players[i].abilities)
      // {
         // var ab = this.players[i].abilities[key]
         // if (ab.passive && (ab.subType !== "rage" || numberOfRages < 2))
         // {
            // ab.Use(null,this)
         // }
      // }
   // }
}

ex.Game.prototype.AddGlobalEffect = function(globalEffect)
{
   globalEffect.OnAdded(this)
   this.globalEffects.push(globalEffect)
}

ex.Game.prototype.RemoveGlobalEffect = function(type)
{
   var index = this.GetGlobalEffectIndex(type)
   if (index !== -1)
   {
      this.globalEffects[index].OnRemoved(this)
      this.globalEffects.splice(index,1)
      return true
   }
   return false
}

ex.Game.prototype.GetGlobalEffectIndex = function(type)
{
   for (var i = 0; i < this.globalEffects.length; i++)
   {
      if (this.globalEffects[i].type === type)
         return i
   }
   return -1
}

ex.Game.prototype.IsGlobalEffectPresent = function(type)
{
   return (this.GetGlobalEffectIndex(type) !== -1)
}

ex.Game.prototype.NewTurn = function(timeout,afterNoMoves)
{
   uDbg.Log("_i","In NewTurn()")
   clearTimeout(this.turnTimeoutId)
   if (!this.inProgress)
      return
   this.turnPhase = ex.TURN_PHASE_PASSING_TURN
   uDbg.Log("_i","Changing turn")   
   this.turn++
   if (this.turn > this.players.length-1)
      this.turn = 0   
   uDbg.Log("_i","Refreshing abilities")
   this.RefreshAbilities() 
    
   uDbg.Log("_i","Players' new turn update")  
   for (var i = 0; i < this.players.length; i++)
   {
      this.players[i].NewTurnUpdate(this.turn)  
   }
   
   uDbg.Log("_i","Ticking effects (before moving)")
   if (!afterNoMoves)
      this.TickEffects(true)
   uDbg.Log("_i","Moving balls after removing")   
   this.MoveBallsAfterRemoving("endTurn")
   uDbg.Log("_i","Ticking effects (after moving)")
   this.TickEffects(false)
   uDbg.Log("Field after removing: ")
   ex.PrintFieldToConsole(this.field)
   uDbg.Log("_i","Sending player data refresh")   
   this.SendRefreshPlayerData(true)
   uDbg.Log("_i","Checking for end game")  
   this.CheckForEndGame()     
   this.DeselectAllBalls()
   this.turnNumber++
   clearTimeout(this.turnTimeoutId)
   if (!this.inProgress)
      return   
   this.SetTurnStartTime()
   uDbg.Log("_i","Sending next turn message to players")
   var m = new uMsg.NextTurnMessage(this)
   if (timeout)
      m.m_data.to = true
   for (var i = 0; i < this.players.length; i++)
      uMsg.SendMessage(this.players[i], m)
   this.turnPhase = ex.TURN_PHASE_NORMAL
}

ex.Game.prototype.CheckIfNoMoves = function()
{
   uDbg.Log("_i","Checking if no moves")
   //uMisc.LogField(this.field)
   // if (uPrefs.failNoMovesAcc)
   // {
      // uPrefs.failNoMovesAcc = false
      // uPrefs.failNoMovesCau = false
      // return ex.NO_MOVES_ON_END_TURN
   // }
   
   // if (uPrefs.failNoMovesCau)
   // {
      // uPrefs.failNoMovesAcc = false
      // uPrefs.failNoMovesCau = false
      // return ex.NO_MOVES_CAUSED_BY_PLAYER
   // }   
   
   for (var i = 0; i < this.field.length; i++)
   {
      for (var j = 0; j < this.field[i].length; j++)
      {
         var chain = this.GetChain(this.field[i][j],true)
         uDbg.Log("_i","Checking chain from ball (",i,j,"), chain length is",chain.length)
         if (chain.length >= uPrefs.minChainLength)
         {
            uDbg.Log("Chain length from ball",this.field[i][j].x,this.field[i][j].y,"is",chain.length,"- possible move found")
            uDbg.Log("Chain contents: ",chain)
            uDbg.Log("_i","Move found, returning false")
            return ex.MOVES_FOUND
         }
      }
   }
   
   var result = ex.NO_MOVES_ON_END_TURN
   
   // outerLoop: for (var i = 0; i < this.field.length; i++)
   // {
      // for (var j = 0; j < this.field[i].length; j++)
      // {
         // var chain = this.GetChain(this.field[i][j],false)
         //uDbg.Log("_i","Checking chain from ball (",i,j,"), chain length is",chain.length)
         // if (chain.length >= uPrefs.minChainLength)
         // {
            // uDbg.Log("Chain length from ball",this.field[i][j].x,this.field[i][j].y,"is",chain.length,"- possible move found")
            // uDbg.Log("Chain contents: ",chain)
            // result = ex.NO_MOVES_CAUSED_BY_PLAYER
            // break outerLoop
         // }
      // }
   // }   
   
   uDbg.Log("_i","No moves found, checking if any ability of moving player (",this.turn,") can negate this")
   if (!this.players[this.turn].ai)
   {
      for (var key in this.players[this.turn].abilities)
      {
         var ab = this.players[this.turn].abilities[key]
         uDbg.Log("_i","Checking ability",ab.type)
         if (ab.CanEliminateNoMoves() && ab.CanBeUsed())
         {
            this.LogMsg("No moves, but negating ability "+String(ab.type)+" of player "+String(this.players[this.turn].id)+" found")
            uDbg.Log("_i","Negating ability found! Returning false!")
            return ex.MOVES_FOUND
         }
      }
      
      var item = this.players[this.turn].item
      if (item && item.ability.CanEliminateNoMoves() && item.ability.CanBeUsed())
      {
         this.LogMsg("No moves, but negating item "+String(item.ability.type)+" of player "+String(this.players[this.turn].id)+" found")
         uDbg.Log("_i","Negating item found! Returning false!")
         return ex.MOVES_FOUND
      }
   }
   uDbg.Log("_i","No negating ability, there are no moves!")
   return result
}

ex.Game.prototype.GetAiPlayer = function()
{
   if (this.players[0].ai)
      return this.players[0]
   else if (this.players[1].ai)
      return this.players[1]
   else
      return null
}

ex.Game.prototype.OnGameMessage = function(player,message)
{
   if (this.lastTurn)
      return

   if (message.s === uMsg.Names.ksReconstruct)
   {
      player.outOfSync = false
   }
   
   if (this.players[0].outOfSync || this.players[1].outOfSync)
      return
   
   uDbg.Log("On game message")
   if (message.m_data && message.m_data.ai)
   {
      var pai = this.GetAiPlayer()
      if (pai)
      {
         player = pai
      }
   }

   if (message.s === uMsg.Names.ksBallSelected)
   {
      ResendMessageToOtherPlayers(message,player,this.players)
      uDbg.Log("Message: ball selected")
      
      var x = message.m_data.selectedBall.x
      var y = message.m_data.selectedBall.y
      //uDbg.Log('m_data x and y:',x,y)
      var ballToBeSelected = this.field[x][y]
      
      //if (this.lastSelected !== undefined && (this.lastSelected.type !== ballToBeSelected.type || !BallsClose(this.lastSelected,ballToBeSelected)))
      if (!this.BallCanBeSelected(ballToBeSelected))
      {
         //uDbg.Log('_ii','cheater - wrong selection, BallCanBeSelected() returned false!')
         ex.Cheater(player,"cheater - wrong selection, BallCanBeSelected() returned false!",this)
         return
      }
      
      for (var i = 0; i < ballToBeSelected.effects.length; i++)
      {
         if (ballToBeSelected.effects[i].triggerOnSelect === true)
         {               
            if (ballToBeSelected.effects[i].Trigger(this.turn) && ballToBeSelected.effects[i].removeOnTrigger === true)
            {
               ballToBeSelected.RemoveEffectByIndex(i)
               i--
               continue
            }
         }
         if (ballToBeSelected.effects[i].removeOnSelect === true)
         {
            ballToBeSelected.RemoveEffectByIndex(i)
            i--
         }            
      }
      this.SendRefreshPlayerData()

      ballToBeSelected.selected = true
      this.selectedBalls.push(ballToBeSelected)
      this.lastSelected = ballToBeSelected
      uDbg.Log('Really last selected:',this.lastSelected.x,this.lastSelected.y) 
      ex.PrintFieldWithSelectedToConsole(this.field)
   }
   else if (message.s === uMsg.Names.ksLastBallDeselected)
   {
      ResendMessageToOtherPlayers(message,player,this.players)
      uDbg.Log('Last selected:',this.lastSelected.x,this.lastSelected.y)         
      var ballToBeDeselected = this.field[message.m_data.deselectedBall.x][message.m_data.deselectedBall.y]
      uDbg.Log('To be deselected:',ballToBeDeselected.x,ballToBeDeselected.y)
      if (!ballToBeDeselected.Equals(this.lastSelected))
      {
         //uDbg.Log('_ii','cheater - wrong ball to be deselected, it is not Equal to the lastSelected ball!')
         ex.Cheater(player,"cheater - wrong ball to be deselected, it is not Equal to the lastSelected ball!",this)
         return            
      }
      this.lastSelected.selected = false
      this.selectedBalls.pop()
      if (this.selectedBalls.length === 0)
         this.lastSelected = undefined
      else
         this.lastSelected = this.selectedBalls[this.selectedBalls.length-1]
      ex.PrintFieldWithSelectedToConsole(this.field)
   }
   else if (message.s === uMsg.Names.ksDeselectAll)
   {
      ResendMessageToOtherPlayers(message,player,this.players)     
      this.DeselectAllBalls()
      ex.PrintFieldWithSelectedToConsole(this.field)
   }
   else if (message.s === uMsg.Names.ksRemoveBalls)
   {
      uDbg.Log("_i","Remove balls message received")
      ResendMessageToOtherPlayers(message,player,this.players)
      var playerBallsToRemove = CreateBallArrayFromMessageData(message.m_data.balls, this.field)
      var serverBallsToRemove = this.selectedBalls
      if (this.selectedBalls.length >= uPrefs.firstEffectChainLength && this.selectedBalls.length < uPrefs.secondEffectChainLength)
      {
         serverBallsToRemove = this.selectedBalls.concat(this.GetCrossNeighbours(this.selectedBalls[this.selectedBalls.length-1])) 
         //serverBallsToRemove = this.selectedBalls.concat(GetBallsForExplosion(this.selectedBalls[this.selectedBalls.length-1],this.field))            
      }
      else if (this.selectedBalls.length >= uPrefs.secondEffectChainLength)
      {
         serverBallsToRemove = this.selectedBalls.concat(GetBallsForExplosion(this.selectedBalls[this.selectedBalls.length-1],this.field)) 
         //serverBallsToRemove = this.selectedBalls.concat(this.GetBallsForEliminateRows(this.selectedBalls[this.selectedBalls.length-1],this.field)) 
      }
      uDbg.Log("_i","Removing same balls")
      serverBallsToRemove = this.RemoveSameBalls(serverBallsToRemove)
      //uDbg.Log('server balls to remove: ',serverBallsToRemove)
      //uDbg.Log('player balls to remove: ',playerBallsToRemove)
      uDbg.Log("_i","Checking ball arrays for equality")
      if (!CheckBallArraysForEquality(serverBallsToRemove,playerBallsToRemove))
      {
         //uDbg.Log('_ii','cheater - player balls do not match server balls!')
         ex.Cheater(player,'cheater - player balls do not match server balls!', this)
         return
      }
      uDbg.Log("_i","Subtracting hp")
      this.DamageEnemy(serverBallsToRemove)
      uDbg.Log("_i","Charging abilities")
      this.ChargeAbilities(serverBallsToRemove)
      //this.SendRefreshPlayerData()
      uDbg.Log("_i","Removing balls")
      this.JustRemoveBalls(serverBallsToRemove)
      uDbg.Log("_i","Proceeding to next turn")
      this.NewTurn()
   }
   else if (message.s === uMsg.Names.ksCheckField)
   {
      uDbg.Log("_i","Checking field...")
      uDbg.Log('Message data: ',message.m_data)
      var match = true
      for (var i = 0; i < uPrefs.fieldWidth; i++)
      {
         for (var j = 0; j < uPrefs.fieldHeight; j++)
         {
            if (message.m_data[i][j].x != this.field[i][j].x || message.m_data[i][j].y != this.field[i][j].y || message.m_data[i][j].type != this.field[i][j].type)
            {
               match = false
               uDbg.Log("_i","Balls at (',i,j,') do not match")
            }
         }
      }
      message.m_data = {}
      message.m_data.match = match
      uMsg.SendMessage(player,message)
      if (match)
         uDbg.Log('Balls match')
      else
         uDbg.Log("Balls don't match")
   }
   else if (message.s === uMsg.Names.ksSelectedAbilityTargets || message.s === uMsg.Names.ksDeselectedAbilityTargets)
   {
      if (!this.targetingAbility.abilityData.hiddenSelect)
         ResendMessageToOtherPlayers(message,player,this.players)
   }
   else if (message.s === uMsg.Names.ksAbilityButtonActivated)
   {
      uDbg.Log("_i","Ability button activated message")
      var ab = player.GetAbility(message.m_data.aT)
      if (!ab)
      {
         var item = player.GetItem(message.m_data.aT)
         if (item)
            ab = item.ability
      }      
      if (ab === undefined || !ab.CanBeUsed(player))
      {
         if (ab === undefined)
         {
            this.LogMsg("undefined ability:"+message.m_data.aT)
         }
         ex.Cheater(player,"cheater - this ability can't be used!",this)
         return
      }
      this.targetingAbility = ab
      ResendMessageToOtherPlayers(message,player,this.players)
   }
   else if (message.s === uMsg.Names.ksAbilityUsed)
   {
      uDbg.Log("_i","Ability used message from player",player.id)
      uDbg.Log("_i","Using ability ",message.m_data.aT)
      var ab = player.GetAbility(message.m_data.aT)
      if (!ab)
      {
         var item = player.GetItem(message.m_data.aT)
         if (item)
            ab = item.ability
      }
      if (ab === undefined)
      {
         ex.Cheater(player, "cheater - wrong ability or item!", this)
         return
      }
      var serverTargets = ab.CanBeUsedWithTheseTargetsAndArguments(this,message.m_data.acs)
      if (serverTargets === undefined)
      {
         ex.Cheater(player, "cheater - wrong ability or item targets!", this)
         return
      }
      
      uDbg.Log("_i","Using ability")
      ab.Use(this,serverTargets,message.m_data.acs)
      message.m_data.n = player.number
      uDbg.Log("_i","Resending ability used message: ", message)
      if (!ab.abilityData.actType)
         ResendMessageToOtherPlayers(message,player,this.players)
      uDbg.Log("_i","Sending refresh player data")
      this.SendRefreshPlayerData()
      uDbg.Log("_i","Checking for game end")
      if (this.field)
      {
         if (ab.removal)
            this.MoveBallsAfterRemoving("ab_rem")
         else
            this.MoveBallsAfterRemoving("ab_no_rem")     
         this.CheckForEndGame()
      }
   }   
   // else if (message.s === uMsg.Names.ksAbilityUsed)
   // {
      // uDbg.Log("_i","Ability used message from player",player.id)
      // uDbg.Log("_i","Using ability ",message.m_data.abilityType)
      // var ab = player.GetAbility(message.m_data.abilityType)         
      // if (ab === undefined)
      // {
         // uDbg.Log("_ii","cheater - wrong ability!")
         // ex.Cheater(player)
         // return
      // }
      // uDbg.Log("Args: ",message.m_data.args)
      // var args = message.m_data.args
      // var serverTargets = ab.CanBeUsedWithTheseTargetsAndArguments(player,this,message.m_data.targets,message.m_data.allTargets,args)
      // if (serverTargets === undefined)
      // {
         // uDbg.Log("_ii","cheater - wrong ability targets!")
         // ex.Cheater(player)
         // return
      // }
      
      // uDbg.Log("_i","Using ability")
      // ab.Use(serverTargets,this,args)
      // uDbg.Log("_i","Resending ability used message: ", message)
      // ResendMessageToOtherPlayers(message,player,this.players)
      // uDbg.Log("_i","Sending refresh player data")
      // this.SendRefreshPlayerData()
      // uDbg.Log("_i","Checking for game end")
      // this.MoveBallsAfterRemoving()
      // this.CheckForEndGame()
   // }
   else if (message.s === uMsg.Names.ksDetargetAll)
   {
      uDbg.Log("_i","DetargetAll message from player",player.id)
      ResendMessageToOtherPlayers(message,player,this.players)
   }
   else if (message.s === uMsg.Names.ksRepick) 
   {
      uDbg.Log("_i","Repick message from player",player.id)
      uDbg.Log("_i","Repick message.m_data.aT",message.m_data.aT)
      if (player.repicksLeft > 0)
      {
         player.repicksLeft--
         var oldAbility = player.GetAbility(message.m_data.aT)
         uDbg.Log("_i","oldAbility",oldAbility)
         var newAbility = uAbility.CreateRandomAbility(oldAbility.clr, player.abilities, player.ai)
         player.RemoveAbility(message.m_data.aT)
         player.AddAbility(newAbility)
         oldAbility = null
         message.m_data.naT = newAbility.type
         message.m_data.naC = newAbility.clr
         if (player.number === 1)
            message.m_data.n = 1
         else
            message.m_data.n = 0
         uDbg.Log("_i","Sending repick message with naT:",message.m_data.naT,"at:",message.m_data.aT,"n:",message.m_data.n)
         ResendMessageToOtherPlayers(message,undefined,this.players)
      }
   }
   else if (message.s === uMsg.Names.ksEnemyAbilityReroll) 
   {
      uDbg.Log("_i","Enemy ability reroll message from player",player.id)
      uDbg.Log("_i","Enemy ability reroll message.m_data.aT",message.m_data.aT)
      if ((player.profile.labs.ear && player.repicksLeft >= uPrefs.enemyAbilityRerollCost && !player.repickedEnemyAbility) || player.hasFreeEnemyAbilityRepick)
      {
         var enemy = this.GetEnemyOfPlayer(player)
         if (player.hasFreeEnemyAbilityRepick)
         {
            player.hasFreeEnemyAbilityRepick = false          
         }
         else
         {            
            player.repickedEnemyAbility = true
            enemy.hasFreeEnemyAbilityRepick = true
            message.m_data.fr = true
         }
         player.repicksLeft -= uPrefs.enemyAbilityRerollCost
         var oldAbility = enemy.GetAbility(message.m_data.aT)
         uDbg.Log("_i","oldAbility",oldAbility)
         var newAbility = uAbility.CreateRandomAbility(oldAbility.clr, enemy.abilities)
         enemy.RemoveAbility(message.m_data.aT)
         enemy.AddAbility(newAbility)
         oldAbility = null
         message.m_data.naT = newAbility.type
         message.m_data.naC = newAbility.clr
         message.m_data.n = player.number
         uDbg.Log("_i","Sending repick message with naT:",message.m_data.naT,"at:",message.m_data.aT,"n:",message.m_data.n)
         ResendMessageToOtherPlayers(message,undefined,this.players)
      }
   }   
   else if (message.s === uMsg.Names.ksAiNoMoves)
   {
      this.LogMsg("Ai no moves!")
      this.JustRemoveField()
      uMsg.SendNoMoves(this,this.turn,ex.NO_MOVES_AFTER_OTHER_ABILITY)
      this.players[this.turn].Damage(uPrefs.damageIfNoTurns)
      this.NewTurn(false)      
   }
   else if (message.s === uMsg.Names.ksReequipItem)
   {
      if (!player.repickedYourItem && player.EquipItem("it", message.m_data.st))
      {
         player.repickedYourItem = true
         message.m_data.n = player.number
         ResendMessageToOtherPlayers(message,undefined,this.players)
      }
   }
   else if (message.s === uMsg.Names.ksReady)
   {
      if (this.pickPhase)
      {
         player.ready = message.m_data.r
         ResendMessageToOtherPlayers(message,player,this.players)
         if (player.ready && this.GetEnemyOfPlayer(player).ready)
         {
            this.Begin()
         }
      }
   }
   else if (message.s === uMsg.Names.ksSurrender)
   {
      if (player.ai)
         player = this.GetEnemyOfPlayer(player)
      this.LoseFor(player)
   }
}

ex.Game.prototype.JustRemoveField = function()
{
   uDbg.Log("_i","Removing field")
   for (var i = 0; i < this.field.length; i++)
   {
      for (var j = 0; j < this.field[i].length; j++)
      {
         uDbg.Log("Setting ball (",i,j,") to undefined")
         this.field[i][j] = undefined
      }
   }
}

ex.Game.prototype.JustRemoveBall = function(ball)
{
   this.field[ball.x][ball.y] = undefined
}   

ex.Game.prototype.JustRemoveBalls = function(balls)
{
   uDbg.Log("Removing balls")
   for (var i = 0; i < balls.length; i++)
   {
      var ball = this.field[balls[i].x][balls[i].y]
      ball.RemoveAllEffects()
      this.field[balls[i].x][balls[i].y] = undefined
   }   
}

ex.Game.prototype.SendRefreshPlayerData = function(turnStart)
{
   uDbg.Log("_i","Sending refresh player data")
   for (var j = 0; j < this.players.length; j++)
   {
      uDbg.Log("Sending refresh player data to player",j)
      uMsg.SendRefreshPlayerData(this.players[j],this.players,this.id,turnStart)
   }      
}

ex.Game.prototype.TickEffects = function(before)
{
   uDbg.Log("_i","Ticking ball effects")
   var priority = 1
   var hadRemoval = false
   do
   {
      var higherPriorityPresent = false
      for (var i = 0; i < uPrefs.fieldWidth; i++)
      {
         jLoop: for (var j = 0; j < uPrefs.fieldHeight; j++)
         {
            //uDbg.Log("_i","Ticking effects of ball {",i,j,"}")
            var thisBall = this.field[i][j]         
            if (!thisBall)
            {
               //uDbg.Log("_i","No such ball, continuing")
               continue
            }             
            
            for (var e = 0; e < thisBall.effects.length; e++)
            {
               var ef = thisBall.effects[e]
               if (ef.tickPriority === priority)
               {
                  if (before)
                  {
                     if (ef.TickBeforeMoving())
                     {
                        if (ef.removal)
                           hadRemoval = true
                        uDbg.Log("_i","Tick true, removing effect by index",e)
                        thisBall.RemoveEffectByIndex(e)
                        e--
                     }
                  }
                  else
                  {
                     if (ef.TickAfterMoving())
                     {
                        if (ef.removal)
                           hadRemoval = true                        
                        uDbg.Log("_i","Tick true, removing effect by index",e)
                        thisBall.RemoveEffectByIndex(e)
                        e--
                     }                     
                  }
               }
               else if (ef.tickPriority > priority)
               {
                  higherPriorityPresent = true
               }
            }
         }
      }
      priority++
   } while(higherPriorityPresent)
   
   uDbg.Log("_i","Ticking player effects")
   for (var i = 0; i < this.players.length; i++)
   {
      uDbg.Log("_i","Ticking effects of player number",i)
      var thisPlayer = this.players[i]
      uDbg.Log("_i","Effects length:",thisPlayer.effects.length)
      for (var j = 0; j < thisPlayer.effects.length; j++)
      {
         uDbg.Log("_i","Ticking effect number",j)
         if (before)
         {
            if (thisPlayer.effects[j].TickBeforeMoving())
            {
               uDbg.Log("_i","Tick true, removing effect by index",j)
               thisPlayer.RemoveEffectByIndex(j)
               j--
            }
         }
         else
         {
            if (thisPlayer.effects[j].TickAfterMoving())
            {
               uDbg.Log("_i","Tick true, removing effect by index",j)
               thisPlayer.RemoveEffectByIndex(j)
               j--
            }                     
         }
      }
   }
   
   uDbg.Log("_i","Ticking global effects")
   for (var a = 0; a < this.globalEffects.length; a++)
   {
      if (before)
      {
         if (this.globalEffects[a].TickBeforeMoving())
         {
            this.globalEffects.splice(a,1)
            a--
         }
      }
      else
      {
          if (this.globalEffects[a].TickAfterMoving())
         {
            this.globalEffects.splice(a,1)
            a--
         }              
      }
   }
   if (!before)
      this.MoveBallsAfterRemoving("endTurn")
}

ex.Game.prototype.BallCanBeSelected = function(ball)
{
   uDbg.Log("In BallCanBeSelected()")
   
   if (ball.specialType != 0)
   {
      ex.Cheater(this.players[this.turn],"Tried to select a special ball!",this)
      return false
   }
   if (this.lastSelected !== undefined && (!this.lastSelected.TypeEqual(this.lastSelected, ball) || !BallsClose(this.lastSelected,ball)))
   {
      ex.Cheater(this.players[this.turn],"Types not equal or balls too far!",this)
      return false
   }
   
   if (!ball.CanBeSelected())
   {
      ex.Cheater(this.players[this.turn],"ball.CanBeSelected() returned false",this)
      return false
   }
   
   uDbg.Log("Can select ball",ball)
   for (var i = 0; i < ball.effects.length; i++)
      uDbg.Log("effect: ",ball.effects[i].type)
   return true
}

ex.Game.prototype.DeselectAllBalls = function()
{
   this.lastSelected = undefined
   this.selectedBalls = []
   for (var i = 0; i < uPrefs.fieldHeight; i++)
   {
      for (var j = 0; j < uPrefs.fieldWidth; j++)
      {
         this.field[i][j].selected = false
      }
   }
}

ex.Game.prototype.LoseFor = function(loser)
{
   if (loser.number === 0)
      this.EndGame(this.players[1])
   else
      this.EndGame(this.players[0])
}

ex.Game.prototype.EndGame = function(winner,disconnect)
{
   clearTimeout(this.pickTO)
   clearTimeout(this.turnTimeoutId)
   
   if (!this.inProgress)
      return

   this.inProgress = false
   
   if (winner !== undefined)
   {
      if (winner.number === 0)
         var loser = this.players[1]
      else
         loser = this.players[0]
      if (!this.players[0].monster && !this.players[1].monster)
      {
         var eloChange = uGameProfile.CountElo(this.players[0].profile, this.players[1].profile, winner.number+1)
      }
      // if (winner.number === 0)
         // var eloChange = uGameProfile.CountElo(this.players[0].profile, this.players[1].profile, 1)
      // else
         // eloChange = uGameProfile.CountElo(this.players[0].profile, this.players[1].profile, 2)
   }

   this.endTime = uMisc.GetServerTime()

   for (var i = 0; i < this.players.length; i++)
   {
      var pl = this.players[i]
      if (i === 0)
         var enemy = this.players[1]
      else
         enemy = this.players[0]
      if (pl.IsConnected())
      {
         if (!pl.ai)
         {
            if (pl.number === winner.number)
            {
               var result = uConsts.RESULT_WON
            }
            else
            {
               if (disconnect)
                  result = uConsts.RESULT_DISCONNECT
               else
                  result = uConsts.RESULT_LOST
            }
            var resultData = pl.OnEndGame(result,enemy)
            if (eloChange)
            {
               if (i === 0)
                  resultData.elo = eloChange.p1
               else
                  resultData.elo = eloChange.p2
            }
            else
            {
               resultData.elo = 0
            }
            uMsg.SendMessage(this.players[i], new uMsg.Message(uMsg.Names.kGameMessage, uMsg.Names.ksEndGame, { g : this.id, r: resultData, p : pl.profile } ))
         }
      }
      else
      {
         pl.OnEndGame(uConsts.RESULT_DISCONNECT,enemy)
      }
   }
   
   if (winner)
      this.logDataObj.w = winner.id
   this.logDataObj.dur = (uMisc.GetServerTime()-this.logDataObj.start)/60
   this.logDataObj.msg = this.logMsg
   
   uMongo.LogToCollection("games", this.logDataObj)
   
   // if (winner)
      // uDbg.Log("_ii","Game #",this.id,"ended, winner: player",winner.id)
   // else
      // uDbg.Log("_ii","Game #",this.id,"ended, no winner")
   
   uLogic.EndGame(this.id)
}


ex.Game.prototype.OnDisconnect = function(pl)
{
   uDbg.Log('On disconnect')
   uDbg.Log('Disconnected player id: ',pl.id)
   if (this.inProgress)
   {
      for (var i = 0; i < this.players.length; i++)
      {
         uDbg.Log("Checking player with id ",this.players[i].id)
         if (i !== pl.number)
         {
            uDbg.Log('Found not disconnected player: ',this.players[i].id)
            if (this.players[i].IsConnected())
            {
               uDbg.Log('Sending disconnect message and ending game')
               //uMsg.SendText(this.players[i],"Your opponent has disconnected")
               this.EndGame(this.players[i],true)
               return
            }
         }
      }
      uDbg.Log('Nobody left online')
      this.EndGame()
   }
}

ex.Game.prototype.GetNeighbours = function(ball)
{
   uDbg.Log("In get neighbours")
   var startx = ball.x - 1
   if (startx < 0) 
      startx = 0
   var endx = ball.x + 1
   if (endx >= uPrefs.fieldWidth) 
      endx = uPrefs.fieldWidth-1
   var starty = ball.y - 1
   if (starty < 0) 
      starty = 0
   var endy = ball.y + 1
   if (endy >= uPrefs.fieldHeight) 
      endy = uPrefs.fieldHeight-1
   var toRet = []
   for (var i = startx; i <= endx; i++)
   {
      for (var j = starty; j <= endy; j++)
      {
         if (i === ball.x && j === ball.y)
            continue
         toRet.push(this.field[i][j])
      }
   }
   uDbg.Log("Returning neighbours: ",toRet) 
   return toRet      
}

ex.Game.prototype.GetCrossNeighbours = function(ball)
{
   var toRet = []
   if (ball.x > 0)
      toRet.push(this.field[ball.x-1][ball.y])
   if (ball.x < uPrefs.fieldWidth-1)
      toRet.push(this.field[ball.x+1][ball.y])
   if (ball.y > 0)
      toRet.push(this.field[ball.x][ball.y-1])
   if (ball.y < uPrefs.fieldHeight-1)
      toRet.push(this.field[ball.x][ball.y+1])
   return toRet      
}

ex.Game.prototype.AffectBalls = function(removedBalls)
{
   uDbg.Log("in AffectBalls")
   for (var i = 0; i < removedBalls.length; i++)
   {
      if (removedBalls[i] == undefined)
         removedBalls.splice(i,1)
   }   
   for (var i = 0; i < removedBalls.length; i++)
   {
      uDbg.Log("checking ball {",removedBalls[i].x,",",removedBalls[i].y,"}")
      if (removedBalls[i].HasEffect(uEf.EFFECT_FROZEN))
      {
         uDbg.Log("ball {",removedBalls[i].x,",",removedBalls[i].y,"} has EFFECT_FROZEN, removing...")
         removedBalls[i].RemoveEffect(uEf.EFFECT_FROZEN)
         removedBalls.splice(i,1)
         i--
      }
   }
}

ex.Game.prototype.DamageEnemy = function(removedBalls,damagingPlayer)
{
   this.AffectBalls(removedBalls)
   var damage = 0
   var heal = 0
   var selfDamage = 0
   uDbg.Log("Damage enemy, yourself and heal yourself):")
   for (var i = 0; i < removedBalls.length; i++)
   {
      uDbg.Log("Removing special type",removedBalls[i].specialType)
      if (removedBalls[i].specialType === uConsts.BALL_SPECIAL_TYPE_HEALTH)
      {
         uDbg.Log("Destroying health stone - adding heal")
         heal += uPrefs.healthStoneHeal
      }
      else if (removedBalls[i].specialType === uConsts.BALL_SPECIAL_TYPE_ATTACK)
      {
         uDbg.Log("Destroying attack stone - adding damage")
         damage += uPrefs.attackStoneDamage
      }
      if (removedBalls[i].type === uConsts.BALL_BLACK)
      {
         selfDamage += removedBalls[i].damage       
      }
      else if (removedBalls[i].type === uConsts.BALL_PINK)
      {
         damage += 2*removedBalls[i].damage*this.players[this.turn].ballDamageMultiplier[removedBalls[i].type] + this.players[this.turn].ballDamageAddition[removedBalls[i].type]
      }
      else if (removedBalls[i].type === uConsts.BALL_FUCHSIA)
      {
         heal += 1
      }
      else
      {
         damage += removedBalls[i].damage*this.players[this.turn].ballDamageMultiplier[removedBalls[i].type] + this.players[this.turn].ballDamageAddition[removedBalls[i].type]
      }
      // else if (removedBalls[i].type === uConsts.BALL_YELLOW)
         // heal += 1
   }
   if (uPrefs.ballsPerOneDamage != 1)
   {
      damage = Math.floor(damage/uPrefs.ballsPerOneDamage)
      selfDamage = Math.floor(selfDamage/uPrefs.ballsPerOneDamage)
   }
   this.ActuallyDamageSelf(selfDamage,damagingPlayer)
   this.ActuallyDamageEnemy(damage,damagingPlayer)
   this.ActuallyHealSelf(heal,damagingPlayer)
}

ex.Game.prototype.ActuallyDamageSelf = function(damage,owner)
{
   owner = owner || this.GetMovingPlayer()
   if (damage > 0)
      owner.Damage(damage)
}

ex.Game.prototype.ActuallyDamageEnemy = function(damage,owner)
{
   if (owner)
      var enemy = this.GetEnemyOfPlayer(owner)
   else
      enemy = this.GetEnemyOfMovingPlayer()
   if (damage > 0)
      enemy.Damage(damage) 
}

ex.Game.prototype.ActuallyHealSelf = function(heal,owner)
{
   owner = owner || this.GetMovingPlayer()   
   if (heal > 0)
      this.HealPlayer(owner.number,heal) 
}      

ex.Game.prototype.CheckForEndGame = function()
{
   if (this.inProgress)
   {
      for (var i = 0; i < this.players.length; i++)
      {
         if (this.players[i].hp <= 0)
         {
            //this.inProgress = false
            this.lastTurn = true
            clearTimeout(this.pickTO)
            clearTimeout(this.turnTimeoutId)            
            setTimeout(this.EndGame.bind(this),3000,this.GetEnemyOfPlayerNumber(i))
            //this.EndGame(this.GetEnemyOfPlayerNumber(i))
            break
         }
      }   
   }   
}

ex.Game.prototype.HealPlayer = function(playerNumber,heal)
{
   uDbg.Log("Healing player",this.players[playerNumber].id)
   uDbg.Log("Current hp: ",this.players[playerNumber].hp)
   this.players[playerNumber].hp += heal
   uDbg.Log("Hp after heal: ",this.players[playerNumber].hp)
}

ex.Game.prototype.SendEndGameMessagesToPlayersAndEndGame = function()
{
   for (var i = 0; i < this.players.length; i++)
   {
      if (this.players[i].IsConnected())
         uMsg.SendMessage(this.players[i], new uMsg.Message(uMsg.Names.kGameMessage,uMsg.Names.ksEndGame, { g : this.id, w: false, p: this.players[i].profile } ))
   }
   uLogic.EndGame(this.id)
}

ex.Game.prototype.PrintEntropy = function()
{
   //uDbg.Log('_ii','Server entropy:',this.entropy)
}

ex.Game.prototype.PrintDice = function()
{
   uDbg.Log('_ii','Server dice:',this.dice)
}   
  
ex.Game.prototype.ChargeAbilities = function(removedBalls,owner)
{
   uDbg.Log("Charging abilities");
   owner = owner || this.players[this.turn]
   var chargeByType = [ 0, 0, 0, 0]
   // for (var i = 0; i < uPrefs.numberOfManas; i++)
      // chargeByType.push(0)
   for (var i = 0; i < removedBalls.length; i++)
   {
      if (removedBalls[i] == undefined)
         continue
      chargeByType[removedBalls[i].type] += 1 * owner.ballManaMultiplier[removedBalls[i].type] + owner.ballManaAddition[removedBalls[i].type]
      if (removedBalls[i].type === uConsts.BALL_YELLOW)
      {
         //var chargeType = uMisc.Random(0,uPrefs.numberOfManas)
         var chargeType = this.MtRandom(0,uPrefs.numberOfManas)
         chargeByType[chargeType] += 1 * owner.ballManaMultiplier[removedBalls[i].type] + owner.ballManaAddition[removedBalls[i].type]
      }            
   }        
   
   for (var i = 0; i < uPrefs.numberOfManas; i++)
   {
      chargeByType[i] = Math.floor(chargeByType[i]/uPrefs.ballsPerOneCharge)
   }
   uDbg.Log("Charge by type:",chargeByType)
     
   uDbg.Log("Charging abilities of player",owner.id)
   for (var i = 0; i < uPrefs.numberOfManas; i++)
   {
      for (var abKey in owner.abilities)
      {
         if (owner.abilities[abKey].clr === i)
         {
            owner.abilities[abKey].Charge(chargeByType[i])
            break
         }
      }
      // owner.mana[i] += chargeByType[i]
      // if (owner.mana[i] > uPrefs.maxMana)
         // owner.mana[i] = uPrefs.maxMana
   }
}

ex.Game.prototype.MoveBallsAfterRemoving = function(calledFrom)
{
   var emptyFound = false
   var someEmpty = false
   uDbg.Log("_i","Move balls after removing")
   do
   {
      emptyFound = false
      for (var i = 0; i < this.field.length; i++)
      {
         for (var j = 0; j < this.field[i].length; j++)
         {
            if (this.field[i][j] === undefined)
            {
               someEmpty = true
               uDbg.Log("_i","No ball on position (",i,j,")")
               if (j == 0)
               {
                  uDbg.Log("_i","Creating a new ball")
                  this.field[i][j] = this.CreateBall(i,j)
               }
               else
               {
                  uDbg.Log("_i","Moving another ball from top")
                  emptyFound = true
                  this.field[i][j] = this.field[i][j-1]
                  this.field[i][j].x = i
                  this.field[i][j].y = j
                  this.field[i][j-1] = undefined
               }
            }
         }
      }
   } while (emptyFound)
   
   if (someEmpty)
   {
      uDbg.Log("_i","Some positions were empty and new balls were created")
   }
   // if (tickEffects)
   // {
      // this.TickEffects()
   // }
   
   var noMovesCheckResult = this.CheckIfNoMoves()
   
   var fail = false
   
   if (uPrefs.failNoMovesOnTurnEnd)
   {
      fail = true
      noMovesCheckResult = ex.NO_MOVES_ON_END_TURN
      uPrefs.failNoMovesOnTurnEnd = false
   }
   else if (uPrefs.failOnRemoval)
   {
      fail = true
      noMovesCheckResult = ex.NO_MOVES_AFTER_REMOVAL_ABILITY
      uPrefs.failOnRemoval = false
   }
   else if (uPrefs.failOnNonRemoval)
   {
      fail = true
      noMovesCheckResult = ex.NO_MOVES_AFTER_OTHER_ABILITY   
      uPrefs.failOnNonRemoval = false
   }
   
   if (noMovesCheckResult !== ex.MOVES_FOUND)
   {
      if (!fail)
      {
         if (calledFrom === "endTurn")
         {
            this.LogMsg("no moves on end turn, now turn of player no."+String(this.turn))
            noMovesCheckResult = ex.NO_MOVES_ON_END_TURN
         }
         else if (calledFrom === "ab_rem")
         {
            this.LogMsg("no moves after removal, now turn of player no."+String(this.turn))
            noMovesCheckResult = ex.NO_MOVES_AFTER_REMOVAL_ABILITY
         }
         else if (calledFrom === "ab_no_rem")
         {
            this.LogMsg("no moves after non-removal, now turn of player no."+String(this.turn))
            noMovesCheckResult = ex.NO_MOVES_AFTER_OTHER_ABILITY
         }
      }
      uDbg.Log("_i","No moves, changing field")
      // if (this.turnPhase === ex.TURN_PHASE_PASSING_TURN)
      // {
         // uDbg.Log("_i","Removing freezing rain effect")
         // this.RemoveGlobalEffect(uGlobalEf.EFFECT_FREEZING_RAIN)
      // }
      
      if (this.GetGlobalEffectIndex(uGlobalEf.EFFECT_FREEZING_RAIN) > -1)
      {
         this.RemoveGlobalEffect(uGlobalEf.EFFECT_FREEZING_RAIN)
         //noMovesCheckResult = ex.NO_MOVES_AFTER_OTHER_ABILITY
      }
      
      this.JustRemoveField()
      uDbg.Log("_i","Checking if there are any moves after change")
      var movesPresent = this.MoveBallsAfterRemoving()        
      if (movesPresent)
      {
         uDbg.Log("_i","Moves found!")
         uDbg.Log("_i","Sending NoMoves message:")
         var causedBy = noMovesCheckResult
         uMsg.SendNoMoves(this,this.turn,causedBy)
         this.players[this.turn].Damage(uPrefs.damageIfNoTurns)
         this.NewTurn(false)
      }
      else
      {
         uDbg.Log("_i","No moves found!")
      }
      return false         
   }
   return true
}

ex.Game.prototype.CreateBall = function(x,y)
{
   var specialType = 0
   var t = 0
   
   if (this.soA)
   {
      var d = this.MtRandom(0,100)
      if (d < uPrefs.attackStoneChance)
         specialType = uConsts.BALL_SPECIAL_TYPE_ATTACK 
   }
   // if (t === uConsts.BALL_GREEN)
   // {
      // uDbg.Log('Yellow ball, throwing dice:')
      // var d = this.MtRandom(0,100)
      // uDbg.Log('Dice result: ',d)
      // if (d < uPrefs.healthStoneChance)
         // specialType = uConsts.BALL_SPECIAL_TYPE_HEALTH
   // }
   // else if (t === uConsts.BALL_RED)
   // {
      // uDbg.Log('Red ball, throwing dice:')
      // var d = this.MtRandom(0,100)
      // uDbg.Log('Dice result: ',d)
      // if (d < uPrefs.healthStoneChance)
         // specialType = uConsts.BALL_SPECIAL_TYPE_ATTACK         
   // }   
   
   if (specialType === 0)
   {
      if (uPrefs.useAllBalls || this.GetSixthGem() === uPrefs.allSS)
      {
         var t = this.MtRandom(0,uConsts.NUMBER_OF_BALL_TYPES)
      }
      else
      {
         if (this.GetSixthGem() === uPrefs.noSS)
         {
            t = this.MtRandom(0,uConsts.NUMBER_OF_USED_BALL_TYPES-1)
         }
         else
         {
            t = this.MtRandom(0,uConsts.NUMBER_OF_USED_BALL_TYPES)
            if (t === uConsts.SIXTH_STONE)
               t = this.ss
         }      
      }
   }
   
   uDbg.Log('creating ball of type',t)
   
   return new Ball(t,x,y,this,specialType)
}

ex.Game.prototype.CreateAndDispatchEntropy = function()
{
   for (var i = 0; i < this.players.length; i++)
   {
      //uMsg.SendMessage(this.players[i], new uMsg.EntropyMessage(this.entropy,this.dice,this.id))
      uDbg.Log('Entropy sent!')
   }
}

ex.Game.prototype.ChargeAll = function()
{
   // for (var i = 0; i < this.players.length; i++)
   // {
      // for (var key in this.players[i].abilities)
      // {
         // this.players[i].abilities[key].currentCharge = uPrefs.maxCharge
      // }
   // }
   for (var i = 0; i < this.players.length; i++)
   {
      for (var j = 0; j < this.players[i].mana.length; j++)
         this.players[i].mana[j] = uPrefs.maxMana
   }      
   this.SendRefreshPlayerData()
}

ex.Game.prototype.GetEnemyOfPlayerNumber = function(number)
{
   if (number === 0)
      return this.players[1]
   else
      return this.players[0]
}

ex.Game.prototype.GetMovingPlayer = function()
{
   return this.players[this.turn]
}

ex.Game.prototype.GetEnemyOfMovingPlayer = function()
{
   if (this.turn === 0)
      return this.players[1]
   else
      return this.players[0]
}

ex.Game.prototype.GetEnemyOfPlayer = function(player)
{
   if (player.number === 0)
      return this.players[1]
   return this.players[0]
}

ex.Game.prototype.GetRandomBalls = function(num, withThese)
{
   var result = []
   if (withThese != undefined)
      result = withThese
   var numberOfBalls = uPrefs.fieldWidth*uPrefs.fieldHeight
   var ball = null
   for (var i = 0; i < num; i++)
   {        
      do
      {
         var sameBall = false
         var d = this.MtRandom(0, numberOfBalls)
         var y = Math.floor(d/uPrefs.fieldWidth)
         var x = d % uPrefs.fieldWidth
         ball = this.field[x][y]
         for (var j = 0; j < result.length; j++)
         {
            if (result[j].x === x && result[j].y === y)
            {
               sameBall = true
               break
            }
         }
      } while(sameBall)
      result.push(ball)
   }
   return result
}

ex.Game.prototype.GetBallsForEliminateRows = function(ball,excludeThisBall)
{
   var toRet = []
   for (var i = 0; i < uPrefs.fieldWidth; i++)
   {
      if (excludeThisBall === undefined || ball.x != i)
         toRet.push(this.field[i][ball.y])
   }
   for (var i = 0; i < uPrefs.fieldHeight; i++)
   {
      if (excludeThisBall === undefined || ball.y != i)
         toRet.push(this.field[ball.x][i])
   }
   return toRet
}

ex.Game.prototype.GetBallsInRow = function(ball,excludeThisBall)
{
   var toRet = []
   for (var i = 0; i < uPrefs.fieldWidth; i++)
   {
      if (!excludeThisBall || ball.x != i)
         toRet.push(this.field[i][ball.y])
   }
   return toRet
}

ex.Game.prototype.GetBallsInColumn = function(ball,excludeThisBall)
{
   var toRet = []
   for (var i = 0; i < uPrefs.fieldHeight; i++)
   {
      if (!excludeThisBall || ball.y != i)
         toRet.push(this.field[ball.x][i])
   }
   return toRet   
}

ex.Game.prototype.GetChain = function(ball,selectable)
{
   uDbg.Log("_i","in GetChain(), looking for selectable chains?:",selectable)
   var chain = []   
   if ((selectable && !ball.CanBeSelected()) || ball.specialType != 0)
   {
      uDbg.Log("_i","ball.CanBeSelected() returned false, returning empty chain...")
      return chain
   }
   chain.push(ball)
   var n = 0
   do
   {
      var neighbours = this.GetNeighbours(chain[n])
      iLoop:
      for (var i = 0; i < neighbours.length; i++)
      {         
         for (var j = 0; j < chain.length; j++)
         {
            if (chain[j].x === neighbours[i].x && chain[j].y === neighbours[i].y)
               continue iLoop
         }
         if (neighbours[i].TypeEqual(chain[n]) && (!selectable || neighbours[i].CanBeSelected()))
         {
            chain.push(neighbours[i])
         }
      }
      n++
   } while(n < chain.length)
   return chain
}

ex.Game.prototype.GetReconstructMessage = function()
{
   this.DeselectAllBalls()
   
   var msg = { n : uMsg.Names.kGameMessage, s : uMsg.Names.ksReconstruct, m_data : { g : this.id } }
   var f = []
   for (var i = 0; i < this.field.length; i++)
   {
      f[i] = []
      for (var j = 0; j < this.field[i].length; j++)
      {
         var ball = this.field[i][j]
         f[i][j] = { t : ball.type, st : ball.specialType}
         var efs = []
         for (var e = 0; e < ball.effects.length; e++)
         {
            var ef = ball.effects[e]
            var efObj = { tp : ef.type, tm : ef.time }
            if (ef.owner)
               efObj.ow = ef.owner.number            
            efs.push(efObj)
         }
         f[i][j].efs = efs
      }
   }   
   msg.m_data.f = f
   
   var plEf = [ [], [] ]
   for (var i = 0; i < this.players.length; i++)
   {
      for (var e = 0; e < this.players[i].effects.length; e++)
      {
         var ef = this.players[i].effects[e]
         var efObj = { tp : ef.type, tm : ef.time }
         if (ef.owner)
            efObj.ow = ef.owner.number
         plEf[i].push(efObj)
      }
   }   
   msg.m_data.plEf = plEf
   
   var glEf = []   
   for (var i = 0; i < this.globalEffects.length; i++)
   {
      var ef = this.globalEffects[i]
      var efObj = { tp : ef.type, tm : ef.time }
      if (ef.owner)
         efObj.ow = ef.owner.number
      glEf.push(efObj)
   }   
   msg.m_data.glEf = glEf
   
   msg.m_data.tr = this.turn   
   msg.m_data.trN = this.turnNumber   
   msg.m_data.trStart = this.turnStartTime
   
   this.sd = uMisc.GetServerTimeMsecs()
   this.mt = new uMt19937.mt(this.sd)
   
   msg.m_data.sd = this.sd
   
   return msg
}

// ex.Game.prototype.GetDice = function()
// {
   // return this.dice.pop()
// }

ex.Cheater = function(pl, msg, game)
{
   if (msg)
   {
      msg = "Player " + String(pl.id) + " cheater | "+msg
      game.LogMsg(msg)
      var stack = String(new Error().stack)
      uMongo.LogError({ msg : msg, gid : game.id, stack : stack }, "cheater")
   }
   uMsg.SendMessage(pl, new uMsg.CheatMessage())     

   if (game)
   {
      var reconstructMsg = game.GetReconstructMessage()
      for (var i = 0; i < game.players.length; i++)
      {
         if (!game.players[i].ai)
         {
            game.players[i].outOfSync = true
            uMsg.SendMessage(game.players[i], reconstructMsg)
         }
      }
      game.SendRefreshPlayerData()
   }   
}

function ResendMessageToOtherPlayers(message,sender,plrs)
{
   for (var i = 0; i < plrs.length; i++)
   {
      if (sender == undefined || i != sender.number)
      {
         uMsg.SendMessage(plrs[i], message)
      }
   }
}

// function CreateEntropy(game)
// {
   // var l = uPrefs.fieldWidth*uPrefs.fieldHeight*2
   // var entropy = []
   // for (var i = 0; i < l; i++)
   // {
      // var t = uMisc.Random(0,uConsts.NUMBER_OF_USED_BALL_TYPES)
      // if (t === uConsts.SIXTH_STONE)
         // t = game.ss
      // entropy.push(t)
   // }
   // return entropy
// }

// function CreateDice()
// {
   // var l = uPrefs.fieldWidth*uPrefs.fieldHeight*2
   // var dice = []
   // for (var i = 0; i < l; i++)
      // dice.push(uMisc.Random(0,100))
   // return dice
// }

function CreateBallArrayFromMessageData(data, field)
{
   var ballArray = []
   //uDbg.Log('Player remove message data: ',data)
   //uDbg.Log('Remove data length: ',data.length)
   for (var i = 0; i < data.length; i++)
   {
      uDbg.Log('inserting element',i,'x: ',data[i].x,'y: ',data[i].y)
      ballArray.push(field[data[i].x][data[i].y])
   }
   return ballArray
}

function GetBallsForExplosion(ball,field)
{
   var startx = ball.x - 1
   if (startx < 0) 
      startx = 0
   var endx = ball.x + 1
   if (endx >= uPrefs.fieldWidth) 
      endx = uPrefs.fieldWidth-1
   var starty = ball.y - 1
   if (starty < 0) 
      starty = 0
   var endy = ball.y + 1
   if (endy >= uPrefs.fieldHeight) 
      endy = uPrefs.fieldHeight-1
   var toRet = []
   for (var i = startx; i <= endx; i++)
   {
      for (var j = starty; j <= endy; j++)
      {
         toRet.push(field[i][j])
      }
   }
   uDbg.Log('returning for explosion: ',toRet)
   return toRet
}

ex.Game.prototype.RemoveSameBalls = function(array1)
{
   var array2 = []
   var sameFound = false
   for (var i = 0; i < array1.length; i++)
   {
      sameFound = false
      for (var j = 0; j < array2.length; j++)
      {
         if (array1[i].Equals(array2[j]))
         {
            sameFound = true;
            break;
         }
      }
      if (!sameFound)
         array2.push(array1[i])
   }
   return array2
}

ex.Game.prototype.CreateField = function()
{
   this.field = []
   do
   {
      for (var i = 0; i < uPrefs.fieldWidth; i++)
      {
         this.field[i] = []
         for (var j = 0; j < uPrefs.fieldHeight; j++)
         {
            this.field[i][j] = new Ball('rnd',i,j,this)
         }
      }
   } while (this.CheckIfNoMoves() != ex.MOVES_FOUND)
}

ex.Game.prototype.GetSixthGem = function()
{
   return this.ss
}

function CheckBallArraysForEquality(array1,array2)
{
   for (var i = 0; i < array1.length; i++)
   {
      var found = false
      for (var j = 0; j < array2.length; j++)
      {
         if (array1[i].Equals(array2[j]))
            found = true
      }
      if (!found)
         return false
   }
   return true
}

function BallsClose(ball1,ball2)
{
   uDbg.Log('checking if balls (',ball1.x,ball1.y,') and (',ball2.x,ball2.y,') are close')
   if (ball1 === undefined || ball2 === undefined)
   {
      uDbg.Log('One of the balls undefined, balls not close')
      return false
   }
   var diffx = Math.abs(ball1.x-ball2.x)
   var diffy = Math.abs(ball1.y-ball2.y)
   if (diffx > 1 || diffy > 1)
   {
      uDbg.Log('diffx or diffy > 1, balls not close')
      return false
   }
   return true
}

ex.PrintFieldToConsole = function(field)
{
   for (var i = 0; i < uPrefs.fieldHeight; i++)
   {
      var str = "";
      for (var j = 0; j < uPrefs.fieldWidth; j++)
      {
         str += String(field[j][i].type)+" "
      }
      uDbg.Log(str,"\n")
   }
}

ex.PrintFieldWithSelectedToConsole = function(field)
{
   uDbg.Log("Printing selected: ")
   for (var i = 0; i < uPrefs.fieldHeight; i++)
   {
      var str = "";
      for (var j = 0; j < uPrefs.fieldWidth; j++)
      {
         var ball = field[j][i]
         if (ball.selected == true)
            str += "S "
         else
            str += String(ball.type)+" "
      }
      uDbg.Log(str,"\n")
   }
}

var Ball = function(t,x,y,game,special)
{
   uDbg.Log("Creating new ball, gameId received: ",game.id)
   this.gameId = game.id
   uDbg.Log("Ball game id: ",this.gameId)
   this.specialType = special || 0
   this.x = x
   this.y = y
   this.damage = 1
   this.type = t
   
   if (t === 'rnd')
   {
      if (uPrefs.useAllBalls || game.GetSixthGem() === uPrefs.allSS)
      {
         this.type = uMisc.Random(0,uConsts.NUMBER_OF_BALL_TYPES)
      }
      else
      {
         if (game.GetSixthGem() === uPrefs.noSS)
         {
            this.type = uMisc.Random(0,uConsts.NUMBER_OF_USED_BALL_TYPES-1)
         }
         else
         {
            this.type = uMisc.Random(0,uConsts.NUMBER_OF_USED_BALL_TYPES)
            if (this.type === uConsts.SIXTH_STONE)
               this.type = game.GetSixthGem()    
         }         
      }
   }
   this.selected = false
   this.effects = []      
   
   if (game.IsGlobalEffectPresent(uGlobalEf.EFFECT_FREEZING_RAIN))
   {
      if (game.turnPhase === ex.TURN_PHASE_PASSING_TURN)
         this.AddEffect(new uEf.EffectFrozen(1))
      else
         this.AddEffect(new uEf.EffectFrozen(2))
   }   
}

Ball.prototype.CanBeSelected = function()
{
   if (this.HasEffect(uEf.EFFECT_FROZEN))
   {
      //uDbg.Log("_ii","Ball {",this.x,",",this.y,"} has EFFECT_FROZEN, can't select!")
      return false
   }
   return true
}

Ball.prototype.CanBeSelectedWithoutPlayerEffects = function()
{
   return true
}

Ball.prototype.FindIndex = function(effectName)
{
   uDbg.Log("Finding index of effect",effectName)
   for (var i = 0; i < this.effects.length; i++)
   {
      if (this.effects[i].type === effectName)
      {
         uDbg.Log("Index found:",i)
         return i
      }
   }
   return -1
}
   
Ball.prototype.GetEffect = function(effectName)
{
   var index = this.FindIndex(effectName)
   if (index !== -1)
      return this.effects[index]
   else
      return undefined
}

Ball.prototype.HasEffect = function(effectName)
{
   uDbg.Log("Checking if ball has effect",effectName)
   uDbg.Log("All effects:",this.effects)
   return (this.FindIndex(effectName) !== -1)
}

Ball.prototype.HasOwningEffect = function(playerNumber)
{
   for (var i = 0; i < this.effects.length; i++)
   {
      if (this.effects[i].owningEffect && (playerNumber === undefined || playerNumber === this.effects[i].owner.number))
      {
         return true
      }
   }
   return false
}

Ball.prototype.AddEffect = function(effect)
{      
   effect.host = this
   uDbg.Log("Adding effect",effect)
   this.effects.push(effect)
}

Ball.prototype.RemoveEffect = function(effectName)
{
   var index = this.FindIndex(effectName)
   if (index === -1)
   {
      uDbg.Log("_ii","Tried to remove unexistant effect!")
      return
   }
   this.effects.splice(index,1)
}

Ball.prototype.RemoveEffectByIndex = function(index)
{
   uDbg.Log("In RemoveEffectByIndex")
   this.effects.splice(index,1)
   uDbg.Log("effects:",this.effects)
}

Ball.prototype.RemoveAllEffects = function()
{
   this.effects = []
}

Ball.prototype.Equals = function(otherBall)
{
   if (this.x == otherBall.x && this.y == otherBall.y && this.type == otherBall.type && this.specialType == otherBall.specialType)
      return true
   return false
}

Ball.prototype.TypeEqual = function(otherBall)
{
   if (this.type === otherBall.type && this.specialType === otherBall.specialType)
      return true
   return false
}

Ball.prototype.RemoveThisBall = function()
{
   uDbg.Log("RemoveThisBall(), gameId: ",this.gameId)
   uDbg.Log("game: ",uLogic._games[this.gameId])
   game.JustRemoveBall(this)
}   