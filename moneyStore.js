var uHttp = require('http')
var uUrl = require('url')
var uCrypto = require('crypto')

var uPrefs = require('./preferences')
var uPlayer = require('./player')
var uDbg = require('./debug')
var uMessage = require('./message')
var uMongo = require('./mongo')

var uBase64Url = require('base64url')

var ex = module.exports

var kongregateApiKey = "514c7806-424c-4d59-a0cf-7b9c7052eacf"

var port = 13333

function GetMoneyShopItem(name)
{
   for (var i = 0; i < uPrefs.moneyShop.length; i++)
   {  
      if (uPrefs.moneyShop[i].name === name)
         return uPrefs.moneyShop[i]
   }
   return null
}

ex.Init = function()
{
   var server = uHttp.createServer(function (request, response)
   {
      uDbg.Log("_i","got request, headers:",request.headers)
      var query = ""
      
      request.on('data', function(chunk)
      {
         query += chunk
      })
      
      request.on('end', function()
      {
         var needsEnd = true
         
         if (request.url === "/kong")
         {            
            uDbg.Log("_i","got query:",query)
            var parts = query.split('=')
            if (parts[0] === "signed_request")
            {                  
               parts = parts[1].split('.')
               var signature = parts[0]
               uDbg.Log('_i','got signature:',signature)
               var payload = uBase64Url.decode(parts[1])        
               uDbg.Log('_i','got payload:',payload)
               payload = JSON.parse(payload)
               if (payload.algorithm == "HMAC-SHA256")
               {             
                  var hmac = uCrypto.createHmac('sha256',kongregateApiKey)
                  hmac.update(parts[1])
                  hmac = uBase64Url.encode(hmac.digest())
                  uDbg.Log('_i','hmac:',hmac)
                  if (hmac === signature)
                  {
                     if (payload.event === "item_order_request")   //kong item order request
                     {
                        uDbg.Log("_i","item order request, payload:",payload)
                        var obj = GetMoneyShopItem(payload.order_info)
                        uDbg.Log("_i","got money shop item:",obj)
                        if (obj)
                        {
                           uDbg.Log("_i","after if(obj)")
                           needsEnd = false
                           var nm = obj.title
                           if (obj.val)
                              nm = String(obj.val)+" "+nm
                           uDbg.Log("_i","nm = ",nm)
                           obj = JSON.stringify
                           ({
                              "items" :
                              [
                                 { 
                                    name : nm, 
                                    description : obj.desc, 
                                    price : obj.c,
                                    image_url : "https://aikei.ru/data/"+obj.title+".png"
                                 }
                              ]
                           })
                           uDbg.Log("_i","responding with",obj)
                           response.end(obj)           
                        }
                     }
                     else if (payload.event === "item_order_placed")
                     {
                        uDbg.Log("_i","item order placed")
                        needsEnd = false             
                        var socialData = { id : String(payload.buyer_id), type : "kong" }
                        payload.logData = { type : "orderDone", platform : "kong" }
                        uPlayer.GetPlayerBySocialData(socialData,function(player)
                        {                           
                           if (player)
                           {                        
                              uDbg.Log("_i","Got buying player id",player.id)
                              
                              payload.logData.id = player.id                            
                              
                              var obj = GetMoneyShopItem(payload.order_info)
                              if (obj)
                              {                               
                                 uDbg.Log("_i", "Got buy obj:",obj)
                                 if (obj.title === "crystals")
                                 {                           
                                    player.profile.cr += obj.val
                                    uMessage.SendMessage(player, { n : uMessage.Names.kRefreshCrystals, s : "", m_data : player.profile.cr })
                                 }
                                 //mark the user as paid
                                 player.profile.pd = true
                                 uDbg.Log("_i","responding with state completed")
                                 payload.logData.rsp = "completed"
                                 uMongo.LogObj(payload)
                                 response.end('{ "state" : "completed" }')
                                 player.SaveProfile()
                              }
                              else
                              {
                                 uDbg.Log("_i", "Buy cancelled, couldn't find obj to buy")
                                 payload.logData.rsp = "canceled - no obj to buy"
                                 uMongo.LogObj(payload)
                                 response.end('{ "state" : "canceled" }')
                              }
                           }
                           else
                           {
                              payload.logData.id = "not found"
                              payload.logData.rsp = "canceled - player not found"
                              uMongo.LogObj(payload)
                              response.end('{ "state" : "canceled" }')
                           }
                        })
                     }                  
                  }
               }
            }         
         }

         if (needsEnd)
            response.end()         
      })
      
   })

   server.listen(port)
}