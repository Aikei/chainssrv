//player.js
var uFs = require('fs')

var uMisc = require('./misc')
var uDbg = require('./debug')
var uLogic = require('./logic')
var uConsts = require('./constants')
var uGameProfile = require('./profile/gameProfile')
var uEffects = require('./abilities/effects')
var uPrefs = require('./preferences')
var uMonsters = require('./monsters')
var uStore = require('./store.js')
var uItem = require('./abilities/item.js')
var uQuest = require('./quest.js')
var uMyDate = require('./myDate.js')
var uMongo = require('./mongo.js')
var uMsg = require('./message')
var uAuth = require('./profile/auth.js')
var uTimedPerk = require('./timedPerk.js')

var ex = module.exports

ex._players = {}

ex.HTYPE_MAGE = 0
ex.HTYPE_SKELETON_1 = 1
ex.HTYPE_SKELETON_2 = 2
ex.HTYPE_SLIME_PINK = 3
ex.HTYPE_SLIME_GREEN = 4
ex.HTYPE_BAT = 5
ex.HTYPE_EYE_BAT = 6
ex.HTYPE_ORC = 7
ex.HTYPE_OGRE = 8
ex.HTYPE_DARK_KNIGHT = 9

ex.size = 0

// var aiNames = [ "oozie", "Enkidu", "Arko", "Gohoribu", "Freego", "Beastiest", "Bad_ass", "Krueger", "Odoacer", "Honiker", "Mongojo", "greg", "Beatrix", "Moonknight", "Darko", "Edto", "Mira", "Bell", "Koi", "yojo" ]

var aiNames = uFs.readFileSync('names', { encoding : 'utf8' })
aiNames = aiNames.split(' ')


var monsters = [ ex.HTYPE_SKELETON_1, ex.HTYPE_SKELETON_2, ex.HTYPE_SLIME_PINK, ex.HTYPE_SLIME_GREEN, ex.HTYPE_BAT, ex.HTYPE_EYE_BAT, ex.HTYPE_ORC, ex.HTYPE_OGRE, ex.HTYPE_DARK_KNIGHT ]

ex.monstersByDifficulty =
[
   [ ex.HTYPE_SLIME_PINK, ex.HTYPE_SLIME_GREEN, ex.HTYPE_BAT, ex.HTYPE_EYE_BAT ],
   [ ex.HTYPE_SKELETON_1, ex.HTYPE_SKELETON_2, ex.HTYPE_ORC ],
   [ ex.HTYPE_OGRE, ex.HTYPE_DARK_KNIGHT ]
]

var timeToDisconnect = 120

ex.AddPlayer = function (player)
{
   uDbg.Log("_i","adding player with id",player.id)
   ex._players[player.id] = player
   ex.size++
}

ex.RemovePlayerByStr = function (str)
{
   uDbg.Log("_i","Removing player by socket str",str)
   var pl = ex._players[str]
   if (pl != undefined)
   {
      uLogic.RemoveFromLookingForGame(pl)
      if (uLogic._games[pl.inGame] !== undefined)
      {
         uLogic._games[pl.inGame].OnDisconnect(pl)
      }
      pl.profile.online = false
      pl.profile.qt = uMisc.GetServerTime()
      pl.SaveProfile()
      delete ex._players[str]
      ex.size--
   }
}

ex.GetPlayerBySocialData = function(socialProfile,cb)
{
   uMongo.GetIdDataBySocialData(socialProfile, function(result)
   {
      if (result != undefined)
      {
         if (ex._players[result.id])
            cb(ex._players[result.id])
         else
            cb(null)
      }
      else
      {
         cb(null)
      }
   })
}

ex.SendMessageToAll = function(txt)
{
   var msg = { n : uMsg.Names.kServerMessage, s : "", m_data : txt }
   for (var key in ex._players)
   {
      var pl = ex._players[key]
      if (pl.IsConnected())
      {
         uMsg.SendMessage(pl, msg)
      }
   }
}

ex.Ai = function()
{
   return new ex.Player()
}

ex.Monster = function(specificHt,pl)
{
   var mpl = new ex.Player()
   
   if (specificHt === undefined)
      uMonsters.MakeMonster(mpl,pl)
   else
      uMonsters.MakeSpecificMonster(mpl,specificHt)
   return mpl
}

ex.TutorialMonster = function()
{
   var pl = new ex.Monster(ex.HTYPE_SKELETON_1)
   pl.absNum = 0
   pl.hpMax = 40
   pl.gain.co = 1
   return pl
}

ex.Player = function (socket, profile)
{
   this.item = null  
   if (socket)
   {
      this.profile = profile
      this.socket = socket
      // this.inventory = new uStore.Inventory(profile.inv)
      // if (this.inventory.eqIt)
      // {
         // var actualItem = new uItem.Item(this.inventory.eqIt.st)
         // this.AddItem(actualItem)
      // }
      // if (this.profile.qu == null)
      // {
         // this.profile.qu = uQuest.GetQuest()
      // }
      // this.questHandler = new uQuest.QuestHandler(this.profile.qu)
   }
   else
   {
      this.profile = uGameProfile.GetProfile(null)
      this.profile.rp = uMisc.Random(1,5)
      this.profile.name = aiNames[uMisc.Random(0,aiNames.length)]
      this.profile.socialProfile.name = this.profile.name
      this.profile.socialProfile.lastName = ""
      this.socket = null
      this.ai = true
   }
   this.ready = false
   this.id = this.profile.id
   this.lastConnectTime = uMisc.GetServerTime()
   this.inGame = undefined
   this.ballDamageAddition = []
   this.ballDamageMultiplier = []
   this.ballManaAddition = []
   this.ballManaMultiplier = []
   this.effects = []  
   this.repickedEnemyAbility = false
   this.hasFreeEnemyAbilityRepick = false
   this.repickedYourItem = false
   this.abilities = {}
   this.hpMax = 100
   this.hp = this.hpMax
   this.shield = 0
   this.mana = []
   this.gain = {}
   this.repicksLeft = this.profile.rp
   for (var i = 0; i < uConsts.NUMBER_OF_BALL_TYPES; i++)
   {
      this.mana.push(0)
   }
   
   this.lastDamageToShield = 0
   this.number = 0
   
}

ex.Player.prototype.SendMessage = function(txt)
{
   var msg = { n : uMsg.Names.kServerMessage, s : "", m_data : txt }
   uMsg.SendMessage(this, msg)
}

ex.Player.prototype.OnLogin = function()
{
   if (this.ai)
      return
   
   // if (this.profile.mkl < this.profile.mklm)
   // {
      // var timeNow = uMisc.GetServerTime()
      // var timeDiff = timeNow-this.profile.lmft
      // var plus = Math.floor(timeDiff / (uPrefs.monsterLimitRefreshTime*60))
      // var rem = Math.floor(timeDiff % (uPrefs.monsterLimitRefreshTime*60))
      // this.profile.mkl += plus
      // if (this.profile.mkl >= this.profile.mklm)
      // {
         // this.profile.mkl = this.profile.mklm
         // this.profile.lmft = 0
      // }
      // else if (plus > 0)
      // {
         // this.profile.lmft = timeNow-rem
      // }      
   // }
   
   this.inventory = new uStore.Inventory(this.profile.inv)
   if (this.inventory.eqIt)
   {
      var actualItem = new uItem.Item(this.inventory.eqIt.st)
      this.AddItem(actualItem)
   }     
      
   if (this.profile.qu == null || !this.profile.qu.nwq)
   {    
      var curDate = new uMyDate.MyDate(uMongo.prevDate.getTime()/1000)
      if (this.profile.qct == 0 || curDate.d > this.profile.qct.d || curDate.m > this.profile.qct.m || curDate.y > this.profile.qct.y)
      {
         this.profile.qu = uQuest.GetQuest()           
      }    
   }   
   this.questHandler = new uQuest.QuestHandler(this.profile.qu)
   
   this.CheckTimedPerks()
   
   this.SaveProfile()
}

// ex.Player.prototype.CheckMonsterLimit = function()
// {
   // var timeNow = uMisc.GetServerTime()
   // var timeDiff = (timeNow-this.profile.lmft)/60
   // if (timeDiff >= uPrefs.monsterLimitRefreshTime)
   // {
      // this.profile.mkl++
      // if (this.profile.mkl == this.profile.mklm)
      // {
         // this.profile.mkl = this.profile.mklm 
         // this.profile.lmft = 0
      // }
      // else
      // {
         // this.profile.lmft = timeNow
      // }
   // }
// }

ex.Player.prototype.CheckMonsterLimit = function()
{
   if (this.profile.mkl >= this.profile.mklm)
   {
      this.profile.lmft = 0
   }
   else
   {
      var timeNow = uMisc.GetServerTime()
      if (this.profile.lmft === 0)
         this.profile.lmft = timeNow      
      var timeDiff = (timeNow-this.profile.lmft)/60
      if (timeDiff >= uPrefs.monsterLimitRefreshTime)
      {
         this.profile.mkl++
         if (this.profile.mkl >= this.profile.mklm)
         {
            this.profile.lmft = 0
         }
         else
         {
            this.profile.lmft = timeNow
         }
      }
   }
}

ex.Player.prototype.BuyItem = function(t,st,withCoins)
{
   var item = uStore.Store[uStore.GetItemHash(t,st)]
   if (item)
   {
      uDbg.Log("Item found")
      var bought = false
      if (withCoins)
      {
         uDbg.Log("Buying item with coins")
         if (this.profile.co >= item.c)
         {
            bought = true
            this.profile.co -= item.c
         }
      }
      else
      {
         uDbg.Log("Buying item with crystals")
         if (this.profile.cr >= item.cr)
         {
            bought = true
            this.profile.cr -= item.cr
         }
      }
      
      if (bought)
      {
         if (item.t === "tpr")
         {
            return this.AddTimedPerk(item.st, item.tm)           
         }
         else
         {
            var invItem = this.inventory.AddOneItem(item)
            if (invItem.t === "ad")
            {
               if (invItem.st === "monL")
               {
                  this.profile.mkl += 10
                  //this.CheckMonsterLimit()
               }
            }
            else if (invItem.t === "pr" && invItem.n === 1)
            {
               if (invItem.st === "vip")
               {
                  this.profile.vp = true
                  this.profile.rp++
                  this.profile.brc = 25
               }
               else if (invItem.st === "maxL")
               {
                  this.profile.mklm = 10
               }
            }
         }
         this.SaveProfile()         
      }
      
      return bought
   }
   uDbg.Log("_ii", "Couldn't find item")
   return null
}

ex.Player.prototype.AddTimedPerk = function(st,durationInHours)
{
   var durationInSeconds = durationInHours*3600
   var index = this.GetTimedPerkIndex(st)
   if (index != -1)
   {
      var timedPerk = this.profile.tpr[index]
      timedPerk.end += durationInSeconds
   }
   else
   {
      timedPerk = new uTimedPerk(st,uMisc.GetServerTime()+durationInSeconds)
      this.profile.tpr.push(timedPerk)
   }
   return timedPerk
}

ex.Player.prototype.GetTimedPerkIndex = function(st)
{
   var timeNow = uMisc.GetServerTime()
   for (var i = 0; i < this.profile.tpr.length; i++)
   {
      var pr = this.profile.tpr[i]
      if (timeNow >= pr.end)
      {
         this.profile.tpr.splice(i,1)
         i--
      }
      else
      {
         return i
      }
   }
   return -1
}

ex.Player.prototype.CheckTimedPerks = function()
{
   var timeNow = uMisc.GetServerTime()
   for (var i = 0; i < this.profile.tpr.length; i++)
   {
      var pr = this.profile.tpr[i]
      if (timeNow >= pr.end)
      {
         this.profile.tpr.splice(i,1)
         i--
      }
   }      
}

ex.Player.prototype.ChangeName = function(newName,cb)
{
   //this.profile.socialProfile.name = newName
   //this.profile.socialProfile.lastName = ""
   //this.profile.socialProfile.uName = this.profile.socialProfile.type+"_"+this.profile.socialProfile.name
   var f = (function(exists)
   {
      if (exists)
      {
         cb(false)
      }
      else
      {
         this.profile.name = newName
         this.profile.ne = true
         this.SaveProfile()
         cb(true)
      }
   }).bind(this)
   uMongo.NameExists(newName,f)
}

ex.Player.prototype.EquipItem = function(t,st)
{
   if (this.inventory.EquipItem(t,st))
   {
      if (t === "it")
      {
         this.inventory.EquipItem(t,st)
         var actualItem = new uItem.Item(st)
         this.AddItem(actualItem)
         this.SaveProfile()
         return true
      }
   }
   return false
}

ex.Player.prototype.GetSendInfo = function()
{
   var info = 
   { 
      hp : this.hpMax,
      p : this.profile, 
      ab : [], 
      it : null 
   }
   for (var key in this.abilities)
   {
      var ab = this.abilities[key]
      info.ab.push( { t : ab.type, c : ab.clr } )
   }
   if (this.item)
      info.it = this.item.ability.type
   return info
}

ex.Player.prototype.OnEndGame = function(result,enemy)
{
   var resultData = { xp: 0, lv: 0, rp: 0, r: result}  
   var game = uLogic._games[this.inGame]
       
   this.CheckTimedPerks()    
       
   if (!enemy || !enemy.monster)
   {
      if (result === uConsts.RESULT_LOST)
      {
         this.profile.gl++
         resultData = this.AddXp(uPrefs.loseXp)
      }
      else if (result === uConsts.RESULT_WON)
      {
         this.profile.gw++
         resultData = this.AddXp(uPrefs.winXp)
      }
      else if (result === uConsts.RESULT_DISCONNECT)
      {
         this.profile.gl++
         this.profile.gd++
      }
   }
   else if (enemy.monster)
   {
      resultData = this.AddXp(uPrefs.monsterXP)
      
      if (result === uConsts.RESULT_WON)
      {
         if (this.profile.tut)
            this.profile.tut = false  
   
         resultData.mk = 1
         this.profile.mk++
                 
         if (enemy.gain.co)
         {
            if (this.profile.vp)
               enemy.gain.co = enemy.gain.co*2
            this.AddCoins(enemy.gain.co)
            resultData.co = enemy.gain.co
         }
         
         var dropChance = enemy.dropChance
         
         if (this.GetTimedPerkIndex("dropCh") != -1)
         {
            dropChance *= 3
         }

         if (this.profile.quTC && (!this.profile.hIt || uMisc.ThrowDice(dropChance)))
         {
            
            if (!this.profile.hIt)
            {
               this.profile.hIt = true
               if (!this.inventory.IsEquipped())
                  resultData.itemTut = true
            }
            
            this.profile.iDrp++
            uAuth.SubmitStat(this, "ItemsDropped", this.profile.iDrp)
            
            if (enemy.rareChance && uMisc.ThrowDice(enemy.rareChance))
            {
               this.profile.riDrp++
               uAuth.SubmitStat(this, "RareItemsDropped", this.profile.riDrp)               
               resultData.dp = enemy.rareDrop[uMisc.Random(0,enemy.rareDrop.length)]
            }
            else
            {
               resultData.dp = enemy.possDrop[uMisc.Random(0,enemy.possDrop.length)]              
            }
            this.inventory.AddGameItem(resultData.dp)
         }                      
      }
   }
   
   if (!game.tut && !this.profile.quTC)
   {
      this.profile.quTC = true
      resultData.quTut = true       //quest tutorial
   }      
   
   if (result !== uConsts.RESULT_WON)
   {
      if (this.item && uMisc.ThrowDice(this.profile.brc))
      {
         resultData.br = true
         this.inventory.RemoveEquippedItem()
         if (!this.inventory.IsEquipped())
            this.item = null
      }      
   }
   
   if (!game.tut)
   {
      this.questHandler.OnEndGame(enemy,result)
      if (this.questHandler.ticked)
         resultData.qt = true       //quest ticked
      if (this.questHandler.MaybeComplete(this))
      {
         this.profile.nQcm++
         uAuth.SubmitStat(this, "QuestsCompleted", this.profile.nQcm)
         if (!this.profile.qComTut)
         {
            this.profile.qComTut = true
            resultData.qComTut = true
         }
         if (resultData.co)
            resultData.co += this.profile.qu.co_rw
         else
            resultData.co = this.profile.qu.co_rw
         
         var itemRewards = uQuest.GetItemRewards(this.profile.qu.it_rw)
         for (var i = 0; i < itemRewards.length; i++)
         {
            this.inventory.AddGameItem(itemRewards[i])
         }
         
         resultData.itrw = itemRewards         
         this.profile.qu = null
         this.profile.qct = new uMyDate.MyDate(uMisc.GetServerTime())
         //this.profile.qct = Math.round(uMisc.GetServerTime())

         delete resultData.qt
         resultData.qc = true
      }
   }
   
   if (!this.ai)
   {
      uAuth.SubmitStat(this, "Rating", this.profile.elo)
      uAuth.SubmitStat(this, "BattlesWon", this.profile.gw)
      uAuth.SubmitStat(this, "MonstersKilled", this.profile.mk)
      if (enemy)
         uAuth.SubmitStat(this, "EnemyHpOnVictory", enemy.hp)
   }
   
   this.inGame = undefined
   
   this.SaveProfile()
   resultData.r = result
   return resultData
}

ex.Player.prototype.AddCoins = function(addC)
{
   this.profile.co += addC
   this.profile.tco += addC
   uAuth.SubmitStat(this, "TotalCoins", this.profile.tco)
}

ex.Player.prototype.AddXp = function(xpToAdd)
{
   this.profile.xp += xpToAdd
   if (this.profile.lv-1 >= uPrefs.tnl.length)
      var req = uPrefs.tnl[uPrefs.tnl.length-1]
   else
      req = uPrefs.tnl[this.profile.lv-1]
   var resultData = { xp: xpToAdd, lv: 0, rp: 0, elo: 0, r: uConsts.RESULT_LOST }   //xp - xp received, lv - levels received, rp - repicks received, elo - elo rating change, r - won or lost (set in OnEndGame)
   while (this.profile.xp >= req)
   {
      this.profile.xp -= req
      resultData.lv++
      this.NextLevel(resultData)
      if (this.profile.lv-1 >= uPrefs.tnl.length)
         req = uPrefs.tnl[uPrefs.tnl.length-1]
      else
         req = uPrefs.tnl[this.profile.lv-1]      
   }
   return resultData
}

ex.Player.prototype.NextLevel = function(resultData)
{
   this.profile.lv++
   uAuth.SubmitStat(this, "Level", this.profile.lv)
   for (var i = 0; i < uPrefs.rewards.p1repickLv.length; i++)
   {
      if (this.profile.lv === uPrefs.rewards.p1repickLv[i])
      {
         this.profile.rp++
         resultData.rp++
      }
   }
   
   if (this.profile.lv === uPrefs.rewards.itemRepick)
   {
      this.profile.labs.irp = true
   }
   
   if (this.profile.lv === uPrefs.rewards.enemyAbilityReroll)
   {
      this.profile.labs.ear = true
   }
}

ex.Player.prototype.IsConnected = function()
{
   if (this.ai)
      return true
   return (this.socket.remoteAddress !== undefined)
}

ex.Player.prototype.Equals = function (other)
{
   if (socket.remoteAddress === other.socket.remoteAddress && id === other.id && socket.remotePort === other.socket.remotePort)
      return true
   return false      
}

ex.Player.prototype.AddAbility = function(ability)
{
   ability.owner = this
   this.abilities[ability.type] = ability
}

ex.Player.prototype.AddNewAbility = function(abilityType)
{
   var ab = new ex.Ability(ex._abilityDatas[abilityType])
   this.AddAbility(ab)
}

ex.Player.prototype.AddItem = function(item)
{
   item.ability.owner = this
   this.item = item
}

ex.Player.prototype.RemoveAbility = function(abilityType)
{
   if (this.abilities[abilityType] !== undefined)
   {
      delete this.abilities[abilityType]
      return true
   }
   return false
}

ex.Player.prototype.ClearAbilities = function()
{
   this.abilities = {}
}

ex.Player.prototype.HasAbility = function(abilityType)
{
   if (this.abilities[abilityType] !== undefined)
   {
      return true
   }
   return false
}

ex.Player.prototype.GetAbility = function(abilityType)
{
   return this.abilities[abilityType]
}

ex.Player.prototype.GetItem = function(itemAbilityType)
{
   if (this.item && this.item.ability.type === itemAbilityType)
      return this.item
   return null
}

ex.Player.prototype.Damage = function(damage)
{
   if (!this.HasEffect(uEffects.EFFECT_ICE_BLOCK))
   {
      if (this.shield === 0)
      {
         this.hp -= damage
         this.lastDamageToShield = 0
      }
      else
      {
         this.shield -= damage
         if (this.shield < 0)
         {
            this.lastDamageToShield = damage+this.shield
            this.hp += this.shield
            this.shield = 0
         }
         else
         {
            this.lastDamageToShield = damage
         }
      }
   }
}

ex.Player.prototype.Heal = function(heal)
{
   this.hp += heal
}

ex.Player.prototype.NewTurnUpdate = function(turn)
{
   if (turn === this.number)
   {
      this.shield = 0
   }
}

ex.Player.prototype.SaveProfile = function()
{
   if (this.ai)
      return
   uGameProfile.SaveProfile(this.profile)
}

ex.Player.prototype.AddEffect = function(effect)
{
   effect.host = this
   this.effects.push(effect)
}

ex.Player.prototype.HasEffect = function(effectType)
{
   for (var i = 0; i < this.effects.length; i++)
   {
      if (this.effects[i].type === effectType)
         return true
   }
   return false
}

ex.Player.prototype.RemoveEffect = function(effectType)
{
   for (var i = 0; i < this.effects.length; i++)
   {
      if (this.effects[i].type === effectType)
      {
         this.effects.splice(i,1)
         return true
      }
   }
   return false
}

ex.Player.prototype.RemoveEffectByIndex = function(index)
{
   if (index >= this.effects.length)
      throw "tried to remove effect with index higher than the length of effects!"
   this.effects.splice(index,1)
}

ex.RandomizePlayers = function(players)
{
   uDbg.Log("Randomize players")   
   var dice = uMisc.Random(0,2)  
   uDbg.Log("dice result:",dice)
   for (var i = 0; i < players.length; i++)
   {   
      players[i].ClearAbilities()
      if (i === dice)
      {
         uDbg.Log("Player",i,"hero class is mage")
         //players[i].heroClass = "mage"
         //players[i].AddAbility(uEf.CreateAbility(uEf.ABILITY_FLAME))
         //players[i].AddAbility(uEf.CreateAbility(uEf.ABILITY_HEAL))
         //players[i].AddAbility(uEf.CreateAbility(uEf.ABILITY_FROST))
         //players[i].AddAbility(uEf.CreateAbility(uEf.ABILITY_TURN))
      }
      else
      {
         uDbg.Log("Player",i,"hero class is shaman")
         //players[i].heroClass = "shaman"
         //players[i].AddAbility(uEf.CreateAbility(uEf.ABILITY_FLAME))
         //players[i].AddAbility(uEf.CreateAbility(uEf.ABILITY_HEAL))
         //players[i].AddAbility(uEf.CreateAbility(uEf.ABILITY_FROST))
         //players[i].AddAbility(uEf.CreateAbility(uEf.ABILITY_TURN))         
      }
   }
}

ex.ListPlayerData = function(pl)
{
   this.id = pl.id
   this.inGame = pl.inGame
   this.ip = pl.socket.remoteAddress
   this.port = pl.socket.remotePort
}

ex.PrintPlayers = function()
{
   for (var key in ex._players)
   {
      uDbg.Log("key: ",key)
      var pl = ex._players[key]
      uDbg.Log('\nPlayer')
      uDbg.Log('id: ', pl.id)
      uDbg.Log('ip: ', pl.socket.remoteAddress)
      uDbg.Log('port: ', pl.socket.remotePort,'\n')
   }
}

ex.RemoveInactivePlayers = function()
{
   // for (var key in ex._players)
   // {
      // var pl = ex._players[key]
      // var timeDiff = uMisc.GetServerTime() - pl.lastConnectTime

      // if (timeDiff > timeToDisconnect)
      // {
         // ex.RemovePlayerByStr(key)
      // }
   // }
}