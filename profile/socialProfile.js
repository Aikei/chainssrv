var lastTestId = 0

module.exports.profile = function(messageData)
{
   if (!messageData)
   {
      this.type = "ai"
      this.name = "ai"
      this.lastName = ""
      this.id = "ai"
   }
   else
   {
      this.type = messageData.type
      this.name = messageData.name
      this.lastName = messageData.lastName
      this.id = messageData.id
   }
}