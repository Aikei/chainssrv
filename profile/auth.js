var uQuery = require('querystring')
var uUrl = require('url')
var uHttp = require('http')

var uDbg = require('../debug.js')

var ex = module.exports

var kongApiKey = "514c7806-424c-4d59-a0cf-7b9c7052eacf"
var kongAuthUrl = "http://www.kongregate.com/api/authenticate.json?"

ex.Authorize = function(messageData,cb)
{
   if (messageData.sp.type === "kong")
   {
      if (messageData.sp.id === "guest")
      {
         uDbg.Log("_i","Auth - is guest, answering...")
         messageData.sp.name = "Wizard"
         cb("guest")
      }
      else
      {
         uDbg.Log("_i","Auth - not guest, querying")
         var query = { user_id : messageData.sp.id, game_auth_token : messageData.sp.token, api_key : kongApiKey }
         query = kongAuthUrl + uQuery.stringify(query)
         //query = uUrl.parse(query)
         var data = ""

         uHttp.get(query, function(res)
         {
          
            res.on('data', function(chunk)
            {
               data += chunk
            })
            
            res.on('end', function()
            {
               data = JSON.parse(data)
               if (data.success)
               {
                  uDbg.Log("_i","Auth - success, returning",data)
                  messageData.sp.id = String(data.user_id)
                  messageData.sp.name = data.username
                  messageData.sp.lastName = ""
                  cb(data)
               }
               else
               {
                  cb(null)
               }
            })
         })
      }
   }
   else if (messageData.sp.type === "web")
   {
      //cb(null)
      
      //test
      cb(messageData)
   }
}

ex.SubmitStat = function(player, statName, statValue)
{
   if (player.profile.socialProfile.type === "kong")
   {
      if (player.profile.socialProfile.id != "guest")
      {
         
         var postData =
         {
            'user_id' : player.profile.socialProfile.id,
            'api_key' : kongApiKey      
         }
         
         postData[statName] = statValue

         postData = uQuery.stringify(postData)      
         
         var options =
         {
            hostname: 'www.kongregate.com',
            path : '/api/submit_statistics.json',
            method: 'POST',
            headers: 
            {
               'Content-Type': 'application/x-www-form-urlencoded',
               'Content-Length': postData.length
            }                  
         }
         
         var req = uHttp.request(options, function(res)
         {
            res.setEncoding('utf8')
            var allData = ""
            
            res.on('data', function(chunk)
            {
               allData += chunk
            })
            
            res.on('end', function()
            {
               uDbg.Log("_i", "Stats response received", allData)
            })
         })
         
         req.on('error', function(e)
         {
            uDbg.Log("_i", "Problem with stats request:",e.message)
         })

         req.write(postData)
         req.end()
      
      }
   }
}