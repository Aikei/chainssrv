var uSocialProfile = require('./socialProfile')
var uFs = require('fs')
var uDbg = require('../debug')
var uMongo = require('../mongo.js')
var uAuth = require('./auth.js')
var uQuest = require('../quest')

var gameProfile = module.exports

gameProfile._profiles = {}

gameProfile.profile = function(socialProfile)
{
   this.socialProfile = socialProfile
   this.gp = 0          //games played
   this.gw = 0          //games won
   this.gl = 0          //games lost
   this.gd = 0          //games disconnected
   this.elo = 1200
   this.rp = 1          //rerolls
   this.lv = 1          //level
   this.xp = 0          //experience
   this.ht = 0          //hero/monster type
   this.co = 0          //coins
   this.tco = 0         //total coins
   this.cr = 0          //crystals
   this.mk = 0          //monsters killed
   this.mkl = 5         //monster kill limit left
   this.mklm = 5        //monster kill limit max
   this.lmft = 0        //last monster fight time - to calculate when to replenish the limit
   this.pd = false      //is paid user
   this.inv = {}        //inventory data
   this.qt = 0          //quit time
   this.qu = new uQuest.FirstQuest()       //quest
   this.qct = 0         //quest completion time
   this.nQcm = 0        //number of quests completed   
   this.iDrp = 0        //items dropped from monsters over all times
   this.riDrp = 0       //rare items dropped from monsters over all times  
   this.brc =  50       //break chance
   this.vp = false      //is vip
   this.ne = false      //name entered
   this.tpr = []        //timed perks
   
   //tuts
   this.tut = true      //show tutorial
   this.hIt = false     //has items / for items tutorial
   this.quTC = false    //quest tutorial complete
   this.qComTut = false //quest completed tutorial complete
   
   this.bnd = false     //if the player is banned
   this.labs = 
   {
      ear : false,      //enemy ablity reroll
      irp : false       //item repick
   }
   this.online = false
   this.name = "Wizard"
   
   //test
   this.lv = 6
   this.rp = 40
   this.co = 500
   this.cr = 500
   this.brc = 50
   this.ne = true
   this.tut = false
   this.labs = 
   {
      ear : false,    //enemy ablity reroll
      irp : false    //item repick
   }   

}

gameProfile.GetProfile = function(messageData,cb)
{
   if (messageData)
   {
      uDbg.Log("_i","Getting profile for messageData",messageData)
      uAuth.Authorize(messageData, function(authData)
      {
         uDbg.Log("_i","Got auth data",authData)
         if (!authData)
         {
            cb(null)
            return
         }
         
         var socialProfile = new uSocialProfile.profile(messageData.sp)
         uDbg.Log("_i", "Created social profile",socialProfile)
         
         uDbg.Log("_i", "Getting idData by messageData",messageData)
         
         //test
         messageData.gid = 0
         
         uMongo.GetIdData(messageData, function(idData)
         {
            uDbg.Log("_i", "Got idData",idData)
            gameProfile.LoadProfile(idData.id, function(p)
            {
               if (!p)
               {                
                  uDbg.Log("_ii","p is undefined, creating new profile")
                  p = new gameProfile.profile(socialProfile)
                  p.id = idData.id
                  //p.name = socialProfile.name
                  if (socialProfile.lastName.length > 0)
                     p.name += " "+socialProfile.lastName
                  gameProfile.SaveProfile(p)
               }
               uDbg.Log("_i", "found p",p)
               cb(p)
            })                 
         })   
         
      })
   }
   else
   {
      socialProfile = new uSocialProfile.profile(null)
      var gp = new gameProfile.profile(socialProfile)
      gameProfile.SaveProfile(gp)
      return gp
   }
}

gameProfile.AttachSocialProfile = function(messageData,cb)
{
   uMongo.UpdateIdData(messageData.id, messageData.sp.type, messageData.sp.id, function()
   {
      cb()
   })
}

gameProfile.GetSocialHash = function(socialProfile)
{
   return socialProfile.type+"_"+socialProfile.id
}

gameProfile.SaveProfile = function(profile)
{
   if (profile.socialProfile.type === "ai")
      return
   uMongo.SaveProfile(profile)
}

gameProfile.LoadProfile = function(id,cb)
{
   uMongo.LoadProfile(id,cb)
}

// gameProfile.SaveProfile = function(profile)
// {
   // if (profile.socialProfile.type === "ai")
      // return
   // uFs.writeFile('data/profiles/'+String(profile.id), JSON.stringify(profile), function(err)
   // {      
      // if (err)
         // throw err
   // })
// }

// gameProfile.LoadProfile = function(fileName,cb)
// {
   // uFs.readFile('data/profiles/'+fileName, function(err, data)
   // {
      // if (err)
         // cb(null)
      // var p = JSON.parse(data)
      // cb(p)
   // })
// }


gameProfile.PrintAllProfiles = function()
{
   for (var key in gameProfile._profiles)
   {
      uDbg.Log("_ii","Loaded Profile: ",gameProfile.GetSocialHash(gameProfile._profiles[key].socialProfile))
   }
}

gameProfile.CountElo = function(profile1,profile2,winnerNumber)
{
   var change = { p1: 0, p2: 0 }
   var exp1 = 1/(1 + Math.pow(10,(profile1.elo - profile2.elo)/400))
   var exp2 = 1 - exp1
   var K1 = 20
   var K2 = 20
   if (profile1.gp < 10)
      K1 = 40
   if (profile2.gp < 10)
      K2 = 40
   if (profile1.elo >= 2300)
      K1 = 10
   if (profile2.elo >= 2300)
      K2 = 10
   if (winnerNumber === 1)
   {
      //profile1.elo += Math.ceil(K1*(1 - exp1))
      change.p1 = Math.ceil(K1 * exp1) 
      change.p2 = -Math.ceil(K2 * (1 - exp2))
      profile1.elo += change.p1
      profile2.elo += change.p2 
   }
   else
   {
      change.p1 = -Math.ceil(K1*(1 - exp1))
      change.p2 = Math.ceil(K2*exp2)
      profile1.elo += change.p1
      profile2.elo += change.p2        
   }
   if (profile1.elo < 1015)
      profile1.elo = 1015
   if (profile2.elo < 1015)
      profile2.elo = 1015
   gameProfile.SaveProfile(profile1)
   gameProfile.SaveProfile(profile2)
   return change
}