//misc.js
var uDbg = require('./debug')
var ex = module.exports
var randomJs = require('random-js')
var uMurmurHash = require('murmurhash-js')

var uMongo = require('./mongo.js')
// var uMt19937 = require('./mMt19937')

// var mt = new uMt19937.mt(ex.GetServerTimeMsecs())

ex.Hash = function(value) 
{
    return (typeof value) + ' ' + (value instanceof Object ?
        (value.__hash || (value.__hash = ++arguments.callee.current)) :
        value.toString());
}

ex.GetServerTime = function()
{
	return ex.GetServerTimeMsecs()/1000;
}

ex.GetServerTimeMsecs = function()
{
	var d = new Date();
	return d.getTime();   
}

ex.DeSerialize = function (str)
{
   var obj = null
   try
   {
      obj = JSON.parse(str);
   }
   catch(e)
   {
      uDbg.Log("_ii", "error parsing JSON string:", str)
      uDbg.Log("_ii", "JSON exception:", e)
      obj = null
   }
   finally
   {
      return obj;
   }
}

ex.Serialize = function (obj)
{
	var str = JSON.stringify(obj);
	return str;
}

ex.DeleteNullChars = function (str)
{
   var newstr = str.replace(/\0/g,'')
   return newstr
}

ex.ReplaceNullCharsWithNewlines = function (str)
{
   var newstr = str.replace(/\0/g,"\n")
   return newstr
}

ex.AddVisibleSpaces = function (str)
{
   var newstr = str.replace(/ /g,'_').replace(/\0/g,'%0%').replace(/\n/g,'%N%').replace(/\t/g,'%T%')
   return newstr
}

ex.Random = function (low, high) 
{
   return randomJs.integer(low, high-1)(randomJs.engines.nativeMath)
   //return Math.floor(randomJs.real(low,high, false)(randomJs.engines.nativeMath))
   //return Math.floor(Math.random() * (high - low) + low)
}

ex.ThrowDice = function(trueChance)
{
   if (ex.Random(0,100) < trueChance)
      return true
   return false
}

ex.CopyArray = function(array)
{
   return array.slice()  
}

ex.Construct = function(constructor, args) 
{
    function F() 
    {
        return constructor.apply(this, args)
    }
    F.prototype = constructor.prototype
    return new F()
}

ex.LogField = function(field,preposition)
{
   preposition = preposition || ""
   var s = ""
   for (var i = 0; i < field.length; i++)
   {
      s += "\n"
      for (var j = 0; j < field[i].length; j++)
      {
         s += String(field[j][i].type)+" "
      }      
   }
   uDbg.Log(preposition,s)
}

ex.CheckTargetsForEquality = function(playerAllTargets,serverAllTargets)
{
   uDbg.Log("_i","Checking targets for equality")
   if (playerAllTargets.length !== serverAllTargets.length)
   {
      uDbg.Log("_ii","Targets lengths are not equal!")
      uDbg.Log("_ii","Client's targets length:",playerAllTargets.length,", server's targets length:",serverAllTargets.length)
      return false
   }
   uDbg.Log("_i","Lengths match")
   for (var i = 0; i < playerAllTargets.length; i++)
   {
      var found = false
      uDbg.Log("_i","Checking player target number",i)
      for (var j = 0; j < serverAllTargets.length; j++)
      { 
         uDbg.Log("_i","For equality with server target number",j)
         uDbg.Log("_i","player target type:",playerAllTargets[i].type)
         if (playerAllTargets[i].type === "b") //b = ball
         {
            uDbg.Log("_i","Target type is 'b' - ball")
            if (playerAllTargets[i].x === serverAllTargets[j].x && playerAllTargets[i].y === serverAllTargets[j].y)
            {
               found = true
               break
            }
         }
         else if (playerAllTargets[i].type === "p")  //p = player
         {
            uDbg.Log("_i","Target type is 'p' - player")
            uDbg.Log("_i","player target:",playerAllTargets[i])
            uDbg.Log("_i","Player target n is",playerAllTargets[i].n)
            uDbg.Log("_i","Server target number is",serverAllTargets[j].number)
            if (playerAllTargets[i].n === serverAllTargets[j].number)
            {
               found = true
               break
            }
         }
         else if (playerAllTargets[i].type === "a")   //a = ability
         {
            if (playerAllTargets[i].aT === serverAllTargets[j].type)
            {
               found = true
               break
            }
         }
      }
      if (!found)
      {
         uDbg.Log("_i","Equality not found, returning false")
         return false
      }
   }
   uDbg.Log("_i","Equality found, returning true")
   return true   
}

ex.GetCheckHash = function(str)
{
   var a = 3
   
   var l = str.length
   
   if (l < 4)
      a = 0
   
   var b = 7
   
   if (l === 1)
      b = 0
   else if (l === 2)
      b = 1 
   else if (l < 8)
      b = 2
   
   var hash = parseInt(uMurmurHash.murmur2(str, str.charCodeAt(a)+str.charCodeAt(b)))
   return hash
}

ex.IsMessageValid = function(msg)
{
   var str = String(msg.n)+String(msg.s)+JSON.stringify(msg.m_data)
   var n = msg.h //parseInt(msg.h,16)
   var h = ex.GetCheckHash(str)
   if (h == n)
   {
      return true
   }
   else
   {
      uMongo.LogError({ serverHash : h, clientHash : n, msg: msg },"MessageValidationFailed")
      return false
   }
}



ex.Hash.current = 0;

