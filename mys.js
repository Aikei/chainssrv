// var uJsSys = require('./js_system')
// uJsSys.InitSystem()

var net = require('net')
var uMisc = require('./misc')
var https = require('https')
var http = require('http')
var uPlayer = require('./player')
var uMessage = require('./message')
var uLogic = require('./logic')
var uDbg = require('./debug')
//var uHttpServer = require('./httpServer')
var uPrefs = require('./preferences')
var uLobby = require('./lobby')
var uAbility = require('./abilities/ability')
var uGameProfile = require('./profile/gameProfile')
//var uSavedData = require('./savedData')
var uMongo = require('./mongo.js')
var uItem = require('./abilities/item')
var uStore = require('./store.js')
var uMoneyStore = require('./moneyStore.js')
var uAuth = require('./profile/auth.js')
var uStdIn = require('./stdin.js')

//var uCreateAbility = require('./abilities/createAbility')

var ex = module.exports

//test
var port = 8000
var host = "localhost"

const kMessageTerminator = "\n";
const kNullCharacter = "\0";

/**
* Get the socket policy response for the given port number
*/
function GetPolicyResponse(port)
{
  var xml = '<?xml version="1.0"?>\n<!DOCTYPE cross-domain-policy SYSTEM'
          + ' "http://www.macromedia.com/xml/dtds/cross-domain-policy.dtd">\n<cross-domain-policy>\n';
 
  xml += '<allow-access-from domain="*" to-ports="*"/>\n';
  xml += '<site-control permitted-cross-domain-policies="master-only"/>\n';
  xml += '</cross-domain-policy>\n\0';
 
  return xml;
}


/*
uMessage.SendDataToSocket = function (socket,data)
{
   //test
   //setTimeout(socket.write.bind(socket),200,uMisc.Serialize(data)+kMessageTerminator+kNullCharacter)
	socket.write(uMisc.Serialize(data)+kMessageTerminator+kNullCharacter);
}
*/

function OnConnect(socket,messageData)
{
   uDbg.Log("_i","OnConnect - Got message data:",messageData)
   if (socket.str === undefined)
   {
      uGameProfile.GetProfile(messageData, function(profile)
      {
         uDbg.Log("_i","OnConnect - Got profile:",profile)
         
         if (!profile)
         {
            uDbg.Log("_i","no profile got - closing")
            uMessage.SendDataToSocket(socket,new uMessage.Message(uMessage.Names.kServerMessage, "", "Wrong login. Play this game on kongregate.com/games/Aikeii/chains-of-magic"))
            //socket.destroy()
            return
         }
         
         if (uPlayer._players[profile.id])
         {
            uDbg.Log("_i","player already online - closing previous")           
            OnClose(uPlayer._players[profile.id].socket)
         }
         
         if (profile.bnd)
         {
            uDbg.Log("_i","banned - closing")
            uMessage.SendDataToSocket(socket,new uMessage.Message(uMessage.Names.kServerMessage, "", "Banned"))
            return
         }
         
         socket.str = profile.id
         profile.online = true
         uDbg.Log('_ii','+++++++++++++++++++++++++++++++++++++++++++++')
         uDbg.Log('_ii','New player connected, ip: ', socket.remoteAddress,":",socket.remotePort,"id:",profile.id)  
         uDbg.Log('_ii',"authorization type: ", messageData.sp.type, "id:", messageData.sp.id,"name: ", messageData.sp.name)      
         var player = new uPlayer.Player(socket,profile)
         uPlayer.AddPlayer(player)
         uLogic.AddToLobby(player)
         //uPlayer.PrintPlayers()
         player.OnLogin()
         var msg = new uMessage.Message(uMessage.Names.kConfirmConnect,"",{ pr: uPrefs, st: uStore.StoreList, abs: uAbility._abilityDatas, itm: uItem._itemAbilityDatas, p: profile })
         uMessage.SendDataToSocket(socket,msg)
         
         // if (profile.tut)
         // {
            // uLogic.StartTutorial(player)
         // }
      })
   }
}

function OnClose(socket)
{
   // uDbg.Log('_ii','Close received')
   // uDbg.Log('_ii','_____________________________________________');  
   // uPlayer.PrintPlayers()
   uMessage.SendDataToSocketAndClose(socket,new uMessage.Message(uMessage.Names.kCloseConnection, "", {}))
   uPlayer.RemovePlayerByStr(socket.str)
   socket.done = true
   //socket.end()
}

function OnLeaderboardRequested(socket,msg)
{
   var pl = uPlayer._players[socket.str]   
   if (pl)
   {
      uMongo.GetLeaderboard(function(leaderboard)
      {
         msg.m_data = {}
         msg.m_data.lb = leaderboard
         uMongo.GetPlayerLbEntry(function(selfLbEntry)
         {
            msg.m_data.sf = selfLbEntry
            uMessage.SendDataToSocket(socket,msg)
         }, pl.id)
      })
   }
}

function ForgetPlayer(pl)
{
   uDbg.Log('Forgetting player: ',pl)
   uPlayer.RemovePlayerByStr(pl.id)
}

function OnTimeMessage(socket,obj)
{
   var serverTime = uMisc.GetServerTime()
   var timeToServer = serverTime - obj.m_data.m_clientTime;
   obj.m_data.m_serverTime = serverTime
   uMessage.SendDataToSocket(socket,obj)
   if (uPlayer._players[socket.str] != undefined)
      uPlayer._players[socket.str].lastConnectTime = uMisc.GetServerTime()   
}

function OnGameMessage(socket,msg)
{
   var pl = uPlayer._players[socket.str]
   if (pl === undefined)
   {
      uDbg.Log('Error! Player not connected!')
      return
   }
   uLogic.ProcessGameMessage(pl,msg)
}

function OnHuntMessage(socket, msg)
{
   uDbg.Log("Find monster message received")
   var pl = uPlayer._players[socket.str]
   if (pl === undefined)
   {
      uDbg.Log('Error! Player not registed on server!')
      return
   }
   uLogic.FindMonster(pl)
}

function OnFindMatchMessage(socket, msg)
{
   uDbg.Log("Find match message received")
   var pl = uPlayer._players[socket.str]
   if (pl === undefined)
   {
      uDbg.Log('Error! Player not registed on server!')
      return
   }
   uDbg.Log("Finding match for player",pl.id)
   uLogic.AddToLookingForGame(pl)
}

function OnServerInfo(socket,msg)
{
   msg.m_data = {}
   var addr = server.address()
   msg.m_data.ip = socket.localAddress
   msg.m_data.port = socket.localPort
   msg.m_data.numberOfPlayers = uPlayer.size
   msg.m_data.lookingForGame = uLogic._lookingForGame.length
   msg.m_data.numberOfGames = uLogic._games.size
   uMessage.SendDataToSocket(socket,msg)
}

function OnServerMessage(socket,msg)
{
   uDbg.Log("_ii","Server message received: ", msg)
   var pl = uPlayer._players[socket.str]   
   if (msg.s === "print_entropy")
   {
      if (pl != undefined && pl.inGame != undefined && uLogic._games[pl.inGame] != undefined)
      {
         uLogic._games[pl.inGame].PrintEntropy()
      }
   }
   else if (msg.s === "print_dice")
   {
      if (pl != undefined && pl.inGame != undefined && uLogic._games[pl.inGame] != undefined)
      {
         uLogic._games[pl.inGame].PrintDice()
      }
   }
   else if (msg.s === "givememana")
   {
      var pl = uPlayer._players[socket.str]
      if (pl != undefined && pl.inGame != undefined && uLogic._games[pl.inGame] != undefined)
      {
         uLogic._games[pl.inGame].ChargeAll()
      }
   }
   else if (msg.s === "failOnEnd")
   {
      uPrefs.failNoMovesOnTurnEnd = true
   }
   else if (msg.s === "failOnRemoval")
   {
      uPrefs.failOnRemoval = true
   }
   else if (msg.s === "failOnNonRemoval")
   {
      uPrefs.failOnNonRemoval = true
   }
   else if (msg.s === "win")
   {
      if (uLogic._games[pl.inGame])
         uLogic._games[pl.inGame].EndGame(pl)
   }
   else if (msg.s === "lose")
   {
      if (uLogic._games[pl.inGame])
         uLogic._games[pl.inGame].LoseFor(pl)     
   }
}

function OnBuyMessage(socket,obj)
{
   var str = socket.str
   var pl = uPlayer._players[str]
   if (pl)
   {
      var buyResult = pl.BuyItem(obj.m_data.t,obj.m_data.st,obj.m_data.wc)
      if (buyResult != null)
      {
         if (obj.m_data.wc)
            obj.m_data.nc = pl.profile.co
         else
            obj.m_data.ncr = pl.profile.cr
         if (obj.m_data.t === "tpr")
            obj.m_data.end = buyResult.end
         uMessage.SendDataToSocket(socket,obj)
      }
   }
}

function OnEquipMessage(socket,obj)
{
   var str = socket.str
   var pl = uPlayer._players[str]
   if (pl)
   {   
      if (pl.EquipItem(obj.m_data.t,obj.m_data.st))
      {
         uMessage.SendDataToSocket(socket,obj)
      }
   }
}

function OnNameEntered(socket,obj)
{
   var pl = uPlayer._players[socket.str]
   if (pl)
   {
      pl.ChangeName(obj.s, function(success)
      {
         obj.m_data.sc = success
         uMessage.SendDataToSocket(socket,obj)
      })      
   }
}

var server = net.createServer(function(socket)
{
	socket.setEncoding('utf8')
	var socketData = ""
	
	socket.on('data', function(data)
	{
      //uDbg.Log('received: ',uMisc.AddVisibleSpaces(data))
		socketData += uMisc.DeleteNullChars(data)
		//uDbg.Log('socket data: ',uMisc.AddVisibleSpaces(socketData))
		if (socketData === '<policy-file-request/>')
		{
         uDbg.Log("policy requested, sending xml: ",xml);
			var xml = GetPolicyResponse(port)			
			socket.write(xml)
			socketData = ""
			return
		}
      
      //OnConnect(socket)
         
		var substrings = socketData.split(kMessageTerminator)
		var lastMsg = substrings.length-1

		if (substrings[lastMsg].length !== 0 && substrings[lastMsg] !== kNullCharacter)
		{
			// partial data read, store for later
			socketData = substrings[lastMsg]
		}
		else
		{
			//full read, clear buffer
			socketData = ""
		}
 
		// process all messages
      var wrongMessage = false
		for ( var i = 0; i<lastMsg; i++ )
		{
			var message = substrings[i];

			// process message!
			uDbg.Log("message:",uMisc.AddVisibleSpaces(message))
			//var msg = uMisc.DeSerialize(message)
			if (message === null)
			{
				wrongMessage = true
				continue
			}
			uDbg.Log("Got message: ",message)
         uMessage.CheckAndGetMessage(message,function(obj)
         {
            if (!obj)
               return
            
            if (obj.n === uMessage.Names.kTime)
            {
               OnTimeMessage(socket,obj)
            }
            else if (obj.n === uMessage.Names.kGameMessage)
            {
               uDbg.Log("Game message received: ",obj)
               OnGameMessage(socket,obj)
            }
            else if (obj.n === uMessage.Names.kFindMatch)
            {
               OnFindMatchMessage(socket,obj)
            }
            else if (obj.n === uMessage.Names.kHunt)
            {
               OnHuntMessage(socket,obj)
            }
            else if (obj.n === uMessage.Names.kServerInfo)
            {
               uDbg.Log("Server info requested")
               OnServerInfo(socket,obj)
            }
            else if (obj.n === uMessage.Names.kListPlayers)
            {
               obj.m_data = []
               for (var key in uPlayer._players)
               {
                  var curPl = new uPlayer.ListPlayerData(uPlayer._players[key])
                  obj.m_data.push(curPl)
               }
               uMessage.SendDataToSocket(socket,obj)
            }
            else if (obj.n === uMessage.Names.kServerMessage)
            {
               OnServerMessage(socket,obj)
            }
            else if (obj.n === uMessage.Names.kLobbyMessage)
            {
               uDbg.Log("_i","This is lobby message, passing in to process function")
               uLobby.ProcessLobbyMessage(socket,obj)
            }
            else if (obj.n === uMessage.Names.kFirstConnect)
            {
               OnConnect(socket,obj.m_data)
            }
            else if (obj.n === uMessage.Names.kLeaderboard)
            {
               OnLeaderboardRequested(socket,obj)
            }
            else if (obj.n === uMessage.Names.kBuy)
            {
               OnBuyMessage(socket,obj)
            }
            else if (obj.n === uMessage.Names.kEquip)
            {
               OnEquipMessage(socket,obj)
            }
            else if (obj.n === uMessage.Names.kNameEntered)
            {
               OnNameEntered(socket,obj)
            }
            else if (obj.n === uMessage.Names.kCancelLook)
            {
               var pl = uPlayer._players[socket.str]
               if (pl)
               { 
                  uLogic.RemoveFromLookingForGame(pl)
                  uLogic.AddToLobby(pl)
                  uMessage.SendDataToSocket(socket,obj)
               }
            }
            else if (obj.n === uMessage.Names.kCheckMonsterLimit)
            {
               var pl = uPlayer._players[socket.str]
               if (pl)
               {     
                  pl.CheckMonsterLimit()
                  obj.m_data = { mkl : pl.profile.mkl, tm : pl.profile.lmft }
                  uMessage.SendDataToSocket(socket,obj)
               }
            }
            else if (obj.n === uMessage.Names.kStartTutorial)
            {
               uDbg.Log("_i","Got start tutorial message from player on socket id",socket.str)
               var pl = uPlayer._players[socket.str]
               if (pl)
               {
                  uDbg.Log("_i","Player found, starting tutorial")                  
                  uLogic.StartTutorial(pl)
               }
            }
            else if (obj.n === uMessage.Names.kMainTutorialDone)
            {
               var pl = uPlayer._players[socket.str]
               if (pl)
               {                
                  pl.profile.tut = false
               }
            }
            else if (obj.n === uMessage.Names.kAuthorize)
            {
               var pl = uPlayer._players[socket.str]
               if (pl)
               {
                  var msgData = obj.m_data
                  uAuth.Authorize(msgData, function(authData)
                  {
                     if (authData)
                     {
                        msgData.id = pl.profile.id
                        uGameProfile.AttachSocialProfile(msgData, function()
                        {
                           pl.profile.socialProfile.id = msgData.sp.id
                           pl.profile.socialProfile.name = msgData.sp.name
                           pl.profile.socialProfile.lastName = msgData.sp.lastName
                           pl.SaveProfile()
                           obj.m_data = { p : pl.profile }
                           uMessage.SendDataToSocket(pl.socket, obj)
                        })
                     }
                     else
                     {
                        obj.m_data = { }
                        uMessage.SendDataToSocket(pl.socket, obj)                       
                     }
                  })
                  // uGameProfile.GetProfile(obj.m_data, function(p)
                  // {
                     // if (p)
                     // {
                        // p.qu = pl.profile.qu
                        // p.inv = pl.profile.inv
                        // p.tut = false
                        // p.mk = 1
                        // pl.profile = p  
                        // obj.m_data = { p : p }
                     // }
                     // else
                     // {
                        // obj.m_data = { }
                     // }
                     // uMessage.SendDataToSocket(pl.socket, obj)
                  // })
               }
            }
         })
		}
	});
	
	//socket.on('connect', OnConnect)
 
	socket.on('close', function() 
   {
      if (!socket.done)
      {
         var pl = uPlayer._players[socket.str]
         if (pl)
            pl = pl.id
         else
            pl = "unknown"
         
         uDbg.Log("_ii","Player",pl,"closed connection");
         
         OnClose(socket)
      }
   })
 
	socket.on('timeout', function(data)
	{
		uDbg.Log("_ii","timeout received", data);
	});
 
	socket.on('error', function(data)
	{
		uDbg.Log("_ii","error received: ",data);
	});
});


// var root = uFs.readFileSync('/home/aikei/.ssl/root.pem')
// var inter = uFs.readFileSync('/home/aikei/.ssl/inter.pem')

// var options = 
// { 
   // key : uFs.readFileSync('/home/aikei/.ssl/key.pem'),
   // cert : uFs.readFileSync('/home/aikei/.ssl/cert.pem'),
   // ca : [ root, inter ]
// }

// var gameHttpsServer = https.createServer(options, function(request, response)
// {
	// uDbg.Log("got http request: ",request.url);
	// if (request.url === '/crossdomain.xml')
	// {
		// uDbg.Log("crossdomain.xml requested");
		// var body = GetPolicyResponse(port);
		// response.writeHead(200, {
		  // 'Content-Length': body.length,
		  // 'Content-Type': 'text/xml' });		
		// response.end(body);
	// }
// });

process.on('exit', function(code) 
{
   uDbg.LogSync("_ii","Exiting with code", code)
});

process.on('uncaughtException', function(err) 
{
   uDbg.Log("_ii","Uncaught exception: ", err)
   uDbg.Log("_ii","Exception stack: ", err.stack)
});

uDbg.Init()

for (var i = 2; i < process.argv.length; i++)
{
   if (process.argv[i] === "clear_log")
   {
      uDbg.ClearLog()
   }
}

function Refresh()
{
   uPlayer.RemoveInactivePlayers()
   setTimeout(Refresh,3000)
}

function Update()
{
   uLogic.FindMatchForAll()
   setTimeout(Update,1000)
}

var warn30min = false
var warn20min = false
var warn10min = false
var warn5min = false
var warn1min = false

function Update60()
{
   var jsDate = new Date()
   var weekDay = jsDate.getUTCDay()
   var prevWeekDay = uMongo.prevDate.getUTCDay()
   var hours = jsDate.getUTCHours()
   if (hours === uPrefs.maintenanceHours-1)
   {
      var minutes = jsDate.getUTCMinutes()    
      if (minutes >= 30 && !warn30min)
      {
         warn30min = true
         uPlayer.SendMessageToAll("Maintenance30Mins")
      }
      
      if (minutes >= 40 && !warn20min)
      {
         warn20min = true
         uPlayer.SendMessageToAll("Maintenance20Mins")
      }        
      
      if (minutes >= 50 && !warn10min)
      {
         warn10min = true
         uPlayer.SendMessageToAll("Maintenance10Mins")
      }          
      
      if (minutes >= 55 && !warn5min)
      {
         warn5min = true
         uPlayer.SendMessageToAll("Maintenance5Mins")
      }
      
      if (minutes >= 59 && !warn1min)
      {
         warn1min = true
         uPlayer.SendMessageToAll("Maintenance1Min")
      }
   }
   uDbg.Log("Update60 - current week day:",weekDay,"; prevWeekDay:",prevWeekDay)
   if (weekDay > prevWeekDay || (weekDay === 0 && prevWeekDay === 6))
   {
      uMongo.OnNewDay()
   }
   setTimeout(Update60,60000)
}

uMongo.Init(function()
{
   uStdIn.Init()
   uMongo.InitLastId(function()
   {
      server.listen(port)
      uMoneyStore.Init()
      Update()
      Refresh()
      Update60()
   })
})

