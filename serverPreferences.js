var serverPreferences = {}

serverPreferences.xpPerWin = 20
serverPreferences.xpPerLoss = 5
serverPreferences.xpPerLevel = [ 5, 10, 20, 100, 300, 500 ]

module.exports = serverPreferences