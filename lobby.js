//lobby.js
var uPlayer = require('./player')
var uMsg = require('./message')
//var uCreateAbility = require('./abilities/createAbility')
var uDbg = require('./debug')

var uLobby = module.exports

uLobby.ProcessLobbyMessage = function(socket,msg)
{
   uDbg.Log("Got lobby message:",msg)
   var str = uPlayer.GetString(socket)   
   var pl = uPlayer._players[str]
   //uDbg.Log("_ii","This socket player: ", pl)
   if (msg.s === uMsg.Names.ksPickAbility)
   {
      uDbg.Log("_ii","Add ability",msg.m_data.abilityName)
      //pl.AddAbility(uCreateAbility.Create(msg.m_data.abilityName))
   }
   else if (msg.s === uMsg.Names.ksUnpickAbility)
   {
      uDbg.Log("Remove ability",msg.m_data.abilityName)
      pl.RemoveAbility(msg.m_data.abilityName);
   }
}