//logic.js

var uGame = require('./game')
var uDbg = require('./debug')
var uMisc = require('./misc')
var uPlayer = require('./player')

var ex = module.exports

//test
var startAiGameAfter = 180
var start2PGameAfter = 20

ex._lookingForGame = {}
ex._lookingForGameSize = 0

ex._inLobby = {}
ex._inLobbySize = 0

ex._games = {}

ex._games.size = 0

ex.StartGame = function()
{
   uDbg.Log("Start game")
   var game = new uGame.Game(arguments)
   ex._games[game.id] = game
   ex._games.size++
}

ex.StartTutorial = function(player)
{
   var ai = new uPlayer.TutorialMonster()
   ex.StartGame(player,ai)
}

ex.EndGame = function(id)
{
   for (var i = 0; i < ex._games[id].players.length; i++)
   {
      var pl = ex._games[id].players[i]
      if (pl.IsConnected())
      {
         uDbg.Log("Adding player",pl.id,"to looking for game")
         pl.inGame = undefined
         ex._inLobby[pl.id] = pl
      }
   }
   delete ex._games[id]
   ex._games.size--
}

ex.ProcessGameMessage = function(player,message)
{
   if (player.inGame == undefined)
   {
      uDbg.Log("Player sent game message while not in game!")
      return
   }
   game = ex._games[player.inGame]
   game.OnGameMessage(player,message)
}

ex.AddToLobby = function(pl)
{
   ex._inLobby[pl.id] = pl
}

ex.AddToLookingForGame = function(pl)
{
   if (!ex._inLobby[pl.id])
   {
      uDbg.Log("Can't add player",pl.id,"to looking for game, because he is not in lobby!")
      return
   }
   delete ex._inLobby[pl.id]
   if (ex._lookingForGame[pl.id])
   {
      uDbg.Log("Error! Player",pl.id,"is already looking for game!")
      return
   }
   pl.matchmakingStartTime = uMisc.GetServerTime()
   ex._lookingForGame[pl.id] = pl
   ex._lookingForGameSize++
}

ex.RemoveFromLookingForGame = function(pl)
{
   if (ex._lookingForGame[pl.id])
   {
      delete ex._lookingForGame[pl.id]
      ex._lookingForGameSize--
   }
}

ex.FindMonster = function(pl)
{
   if (pl.inGame == undefined)
   {
      ex.StartGame(pl,new uPlayer.Monster(undefined,pl))
   }
   // if (pl.profile.mkl > 0)
   // {
      // pl.profile.mkl--
      // if (pl.profile.lmft === 0)
         // pl.profile.lmft = uMisc.GetServerTime()
      // ex.StartGame(pl,new uPlayer.Monster(undefined,pl))
   // }
}

function GetEloDifference(p1,p2)
{
   return Math.abs(p1.profile.elo - p2.profile.elo)
}

function FindMatchCycle()
{
   for (var key in ex._lookingForGame)
   {
      FindMatch(ex._lookingForGame[key])
   }
}

function FindMatch(pl)
{
   var timeDifference = uMisc.GetServerTime() - pl.matchmakingStartTime 
   if (ex._lookingForGameSize === 1)
   {     
      if (timeDifference > startAiGameAfter)
      {
         ex.StartGame(pl,new uPlayer.Ai())
         ex.RemoveFromLookingForGame(pl)
      }
   }
   else if (ex._lookingForGameSize === 2)
   {
      if (timeDifference > start2PGameAfter)
      {
         for (var key in ex._lookingForGame)
         {
            var pl2 = ex._lookingForGame[key]
            if (pl2.id != pl.id)
            {
               ex.StartGame(pl,pl2)
               ex.RemoveFromLookingForGame(pl)
               ex.RemoveFromLookingForGame(pl2)
               break
            }
         }
      }
   }
   else if (ex._lookingForGame.length >= 2)
   {
      var mult = 1
      if (timeDifference >= 5 && timeDifference < 10)
      {
         mult += Math.floor(timeDifference/10)     
      }
      else if (timeDifference >= 10 && timeDifference < 20)
      {
         mult = 5
      }
      else if (timeDifference >= 20)
      {
         mult += 5+(Math.floor((timeDifference-10)/10))
      }
      else if (timeDifference >= 60)
      {
         mult = -1
      }
      
      for (var key in ex._lookingForGame)
      {
         var pl2 = ex._lookingForGame[key]
         if (pl.id !== pl2.id && pl.socket.remoteAddress != pl2.socket.remoteAddress && (mult === -1 || GetEloDifference(pl,pl2) <= 100*mult))
         {
            ex.StartGame(pl,pl2)
            ex.RemoveFromLookingForGame(pl)
            ex.RemoveFromLookingForGame(pl2)
         }
      }
   }
}

ex.FindMatchForAll = function()
{
   FindMatchCycle()
}

/*
ex.FindMatch = function(n)
{
   uDbg.Log("Finding Match")
   var r = uMisc.Random(0,ex._lookingForGame.length)
   if (r != n && ex._lookingForGame[r].IsConnected() && ex._lookingForGame[n].IsConnected())
   {
      ex.StartGame(ex._lookingForGame[r],ex._lookingForGame[n])
      ex._lookingForGame.splice(r,1)
      n = ex._lookingForGame.indexOf(ex._lookingForGame[n])
      ex._lookingForGame.splice(n,1)
   }
   uDbg.Log("Looking for game length now: ", ex._lookingForGame.length)
}
*/