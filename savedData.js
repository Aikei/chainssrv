var uFs = require('fs')

var ex = module.exports

ex.data = null

ex.prevDate = null

ex.cheaters = []

ex.IsCheater = function(id)
{
   for (var i = 0; i < ex.cheaters.length; i++)
   {
      if (id === ex.cheaters[i])
         return true
   }
   return false
}

ex.Init = function()
{
   try
   {
      var f = uFs.readFileSync('data/savedData', { encoding : 'utf8' } )
      ex.data = JSON.parse(f)
   }
   catch(err)
   {
      ex.data = 
      {
         lastid: 100,
         prevDateTime: new Date().getTime(),
         startHours: 0
      }
      ex.Save()
   }
   finally
   {
      ex.prevDate = new Date(ex.data.prevDateTime)
   }
}

ex.OnNewDay = function()
{
   ex.prevDate = new Date()
   ex.data.prevDateTime = ex.prevDate.getTime()
   ex.Save()
}

ex.Save = function()
{
   uFs.writeFile('data/savedData', JSON.stringify(ex.data), function(err)
   {      
      if (err)
         throw err
   })     
}