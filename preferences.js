//preferences.js
//var uItem = require('./abilities/item.js')

var ex = module.exports

ex.fieldWidth = 6
ex.fieldHeight = 6
ex.maxHP = 100
ex.healthStoneChance = 0
ex.attackStoneChance = 2
ex.ballsPerOneDamage = 1
ex.healthStoneHeal = 3
ex.attackStoneDamage = 4
ex.maxCharge = 8
ex.ballsPerOneCharge = 1
ex.maxMana = 8
ex.firstEffectChainLength = 5
ex.secondEffectChainLength = 9
ex.minChainLength = 3
ex.damageIfNoTurns = 15
ex.damageOnTurnTimeout = 15
ex.numberOfManas = 4
ex.blackStoneDamageMultiplier = 1.25
ex.pickTimeout = 45
ex.turnTimeout = 45
ex.winXp = 20
ex.loseXp = 10
ex.monsterXP = 2
ex.useAllBalls = false
ex.playersInLeaderboard = 25
ex.baseBreakChance = 50
ex.monsterLimitRefreshTime = 10
ex.enemyAbilityRerollCost = 2
ex.uz = true   //use zip
ex.uh = true   //use hash
ex.noSS = -1
ex.allSS = -2
ex.noSSLevel = 6
ex.allSSLevel = 11
ex.stoneOfAttackLevel = 4
ex.stoneOfAttackPresenceChance = 35
ex.maintenanceHours = 9
ex.v = "1.02"
ex.AllEffectTypes = []

ex.moneyShop =
[
   { name : "cr_1", title : "crystals", val : 50, c : 50, desc : "In-game currency" },
   { name : "cr_2", title : "crystals", val : 100, c : 100, desc : "In-game currency" },
   { name : "cr_3", title : "crystals", val : 300, c : 250, desc : "In-game currency" },
   { name : "cr_4", title : "crystals", val : 500, c : 400, desc : "In-game currency" }
]

ex.tnl = [ 10, 20, 40, 80, 100, 200, 200, 200, 200, 300, 300, 300, 300, 400 ]
ex.rewards = 
{
   p1repickLv : [ 3, 5, 10, 15 ],
   itemRepick : 7,
   enemyAbilityReroll: 13
}

//test
// ex.pickTimeout = 6
// ex.monsterLimitRefreshTime = 1
// ex.pickTimeout = 30
// ex.monsterLimitRefreshTime = 5
// ex.stoneOfAttackLevel = 1
// ex.stoneOfAttackPresenceChance = 100

// ex.Store = 
// [
   // { t : "it", st : uItem.ITEM_HEALTH_VIAL, c : 50, cr : 5 },
   // { t : "it", st : uItem.ITEM_POTION_OF_DARKNESS, c : 80, cr : 10 },   
   // { t : "it", st : uItem.ITEM_BOW, c : 500 },
   // { t : "it", st : uItem.ITEM_AXE, c : 100, cr : 15 },
   // { t : "it", st : uItem.ITEM_SWORD, c : 250, cr : 35 },
   // { t : "it", st : uItem.ITEM_SHIELD, c : 400, cr : 50 }
// ]

// ex.pickTimeout = 30
// ex.turnTimeout = 45