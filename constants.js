//constants.js
//
//lists some constants used by other units

var consts = module.exports

consts.BALL_BLUE = 0
consts.BALL_PURPLE = 1
consts.BALL_RED = 2
consts.BALL_GREEN = 3
consts.BALL_YELLOW = 4

consts.SIXTH_STONE = 5

consts.BALL_BLACK = 5
consts.BALL_PINK = 6
consts.BALL_FUCHSIA = 7

consts.NUMBER_OF_USED_BALL_TYPES = 6
consts.NUMBER_OF_BALL_TYPES = 8
consts.NO_SS_DICE = 8
consts.ALL_SS_DICE = 9
consts.NUMBER_OF_MANAS = 4

consts.TYPES_NUMBER = 6
consts.BALLS_PER_ONE_CHARGE = 1

consts.BALL_SPECIAL_TYPE_HEALTH = 1
consts.BALL_SPECIAL_TYPE_ATTACK = 2

consts.RESULT_WON = 0
consts.RESULT_LOST = 1
consts.RESULT_DISCONNECT = 2