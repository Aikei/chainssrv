var uMongo = require('mongodb')
var uMongoClient = uMongo.MongoClient
var uDbg = require('./debug')
var uPrefs = require('./preferences')
var uMisc = require('./misc')
var uMyDate = require('./myDate')

var url = 'mongodb://localhost:27017/chains'

module.exports.prevDate = null

var db

var lastId = 100

module.exports.Init = function(cb)
{
   //this.SaveProfile({ id: -1 })
   this.ClearDatabase(
   function()
   {
      uMongoClient.connect(url, function(err, database)
      {
         if (err)
         {
            console.log('Couldnt connect to database!')
            uDbg.Log('_ii',"Couldnt connect to database!")
            return
         }
         db = database
         // players.indexExists('leaderboard', function(err,result)
         // {
            // if (err)
            // {
               // console.log('error checking for leaderboard index in collection "players"!')
               // throw err
            // }
            // if (!result)
            // {
               // uDbg.Log('_ii','index "leaderboard" not found in db "players"')
               // players.createIndex({ elo : -1 }, { name : 'leaderboard' }, function(err,indexName)
               // {
                  // uDbg.Log('_ii','created index, indexName:',indexName)
               // })
            // }
            // else
            // {
               // uDbg.Log('_ii','index "leaderboard" found')
            // }
         // })
         
         var players = db.collection('players')
         players.indexExists('playerId', function(err,result)
         {
            if (err)
            {
               throw err
            }
            if (!result)
            {
               uDbg.Log('_ii','index "playerId" not found in db "players"')
               players.createIndex({ id : 1 }, { name : 'playerId' }, function(err,indexName)
               {
                  uDbg.Log('_ii','created index, indexName:',indexName)
               })
            }
            else
            {
               uDbg.Log('_ii','index "playerId" found')
            }
         })

         players.indexExists('playerName', function(err,result)
         {
            if (err)
            {
               throw err
            }
            if (!result)
            {
               uDbg.Log('_ii','index "playerName" not found in db "players"')
               players.createIndex({ name : 1 }, { name : 'playerName' }, function(err,indexName)
               {
                  uDbg.Log('_ii','created index, indexName:',indexName)
               })
            }
            else
            {
               uDbg.Log('_ii','index "playerName" found')
            }
         })          
        
         var lb = db.collection('leaderboard')
         lb.indexExists('elo', function(err,result)
         {
            if (err)
            {
               throw err
               uDbg.Log('_ii','error when checking existance of index "elo"')
            }
            if (!result)
            {
               uDbg.Log('_ii','index "elo" not found in db "lb"')
               lb.createIndex({ elo : -1 }, { name : 'elo' }, function(err,indexName)
               {
                  uDbg.Log('_ii','created index, indexName:',indexName)
               })
            }
            else
            {
               uDbg.Log('_ii','index "elo" found')
            }		
         })
         
         lb.indexExists('playerId', function(err,result)
         {
            if (err)
            {
               throw err
            }
            if (!result)
            {
               uDbg.Log('_ii','index "playerId" not found in db "lb"')
               lb.createIndex({ id : 1 }, { name : 'playerId' }, function(err,indexName)
               {
                  uDbg.Log('_ii','created index, indexName:',indexName)
               })
            }
            else
            {
               uDbg.Log('_ii','index "playerId" found')
            }
         })
         
         var savedData = db.collection("savedData")
         savedData.find({ "data" : true }).next(function(err,data)
         {
            if (!data)            
            {
               module.exports.prevDate = new Date()
               savedData.insertOne({ "data" : true, "prevDateTime" : module.exports.prevDate.getTime(), "startHours" : 0 }, function(err, result)
               {
                  cb()
               })     
            }
            else
            {
               module.exports.prevDate = new Date(data.prevDateTime)
               cb()
            }
         })
         
         var idData = db.collection('idData')
         idData.indexExists('kong', function(err,result)
         {            
            if (err)
            {
               throw err
            }
            if (!result)
            {
               uDbg.Log('_ii','index "kong" not found in collection idData')
               idData.createIndex({ kong : 1 }, { name : 'kong' }, function(err, indexName)
               {
                  uDbg.Log('_ii','created index, indexName:',indexName)
               })
            }
            else
            {
               uDbg.Log('_ii','index "kong" found')
            }
         })
              
      })
   })
}

module.exports.NameExists = function(name,cb)
{
   db.collection('players').find({ name : name }).limit(1).next(function(err,result)
   {
      if (err)
         throw err
      if (result)
      {
         cb(true)
      }
      else
      {
         cb(false)
      }
   })
}

module.exports.LogToCollection = function(col,dataObj,cb)
{
   if (!(typeof dataObj === "object") || dataObj === null)
      dataObj = { dataObj : dataObj }   
   if (!dataObj.logData)
      dataObj.logData = {}
   dataObj.logData.time = uMisc.GetServerTime()
   dataObj.logData.date = new uMyDate.MyDate(dataObj.logData.time,true)
   
   db.collection(col).insertOne(dataObj, function(err)
   {
      if (err)
      {
         //console.log('dataObj:',dataObj)
         uDbg.Log("_ii","Insertion error:", err)
      }
      if (cb)
         cb(dataObj)
   })  
}

module.exports.LogObj = function(dataObj,cb)
{
   module.exports.LogToCollection('logs',dataObj,cb)
}

module.exports.LogWithType = function(dataObj,type,subType,cb)
{
   dataObj.type = type
   if (subType)
      dataObj.subType = subType
   module.exports.LogObj(dataObj,cb)
   // if (!(typeof dataObj === "object") || dataObj === null)
      // dataObj = { dataObj : dataObj }   
   // if (!dataObj.logData)
      // dataObj.logData = {}
   // dataObj.logData.time = uMisc.GetServerTime()
   // dataObj.logData.date = new uMyDate.MyDate(dataObj.logData.time,true)
   // dataObj.type = type
   // if (subType)
      // dataObj.subType = subType
   
   // uMongoClient.connect(url, function(err, db)
   // {
      // if (err)
      // {
         // uDbg.Log("_ii","Couldn't connect to database when logging to collection")
      // }
      
      // db.collection('logs').insertOne(dataObj, function()
      // {
         // if (err)
         // {
            // uDbg.Log('_ii','error logging',dataObj)
            // throw err
         // }
         // else
         // {
            // if (cb)
               // cb(dataObj)
         // }
      // })
   // })   
}

module.exports.LogError = function(dataObj,subType,cb)
{
   if (!(typeof dataObj === "object") || dataObj === null)
      dataObj = { dataObj : dataObj }
   if (!dataObj.logData)
      dataObj.logData = {}
   dataObj.logData.time = uMisc.GetServerTime()
   dataObj.logData.date = new uMyDate.MyDate(dataObj.logData.time,true)
   dataObj.type = "error"
   if (subType)
      dataObj.subType = subType  
   
   db.collection('logs').insertOne(dataObj, function(err)
   {
      if (err)
      {
         uDbg.Log('_ii','error logging error',dataObj)
         throw err
      }
      else
      {
         if (cb)
            cb(dataObj)
      }
   })   
}

module.exports.InitLastId = function(cb)
{
   db.collection('players').find().sort({ id : -1 }).limit(1).next(function(err,gameProfile)
   {
      if (err)
      {       
         uDbg.Log("_ii","Error getting profile of max id player")        
      }
      if (gameProfile)
      {
         lastId = gameProfile.id
      }
      console.log('lastId = ',lastId)
      cb()
   })
}

module.exports.UpdateIdData = function(playerId,newPlatformType,newPlatformId, cb)
{
   var setObj = {}
   setObj[newPlatformType] = newPlatformId
   db.collection("idData").updateOne({ id : playerId }, { "$set" : setObj }, function(err,result)
   {
      if (err)
         throw err
      cb()
   })
}

module.exports.GetIdDataByGameId = function(id,cb)
{
   db.collection('idData').find({ "id" : id }).limit(1).next(function(err,result)
   {
      if (err)
      {
         throw err
      }
      else
      {
         if (result)
         {
            uDbg.Log("_i","Found profile for game id",id)
            cb(result)
         }
         else
         {
            uDbg.Log("_i","Couldnt find profile for game id",id)
            cb()
         }
      }         
   })
}

module.exports.GetIdDataBySocialData = function(socialProfile,cb)
{
   var obj = {}
   obj[socialProfile.type] = socialProfile.id
   db.collection('idData').find(obj).limit(1).next(function(err,result)
   {
      if (err)
      {
         uDbg.Log('_ii','error getting id data for',socialProfile)
      }
      else
      {
         if (result)
         {
            cb(result)
         }
         else
         {
            cb()
         }
      }
   })      
}

module.exports.GetIdData = function(messageData,cb)
{
   uDbg.Log("_i","Entered GetIdData")
   var socialProfile = messageData.sp
   var idData = db.collection('idData')
   if (messageData.sp.type === "web")
   {
      uDbg.Log("_i","Getting iddata for gameid",messageData.gid)
      module.exports.GetIdDataByGameId(messageData.gid, function(result)
      {
         if (result)
         {
            uDbg.Log("_i","Found iddata",result)
            cb(result)
         }
         else
         {
            lastId++
            var idObj = { id : lastId, "web" : true }
            idData.insertOne(idObj,function(err)
            {
               if (err)
               {
                  uDbg.Log('_ii','error inserting idObj')
                  throw err
               }
               else
               {
                  cb(idObj)
               }
            }) 
         }
      })
   }
   else if (socialProfile.id === "guest")
   {
      lastId++
      var idObj = { id : lastId }
      idData.insertOne(idObj,function(err)
      {
         if (err)
         {
            uDbg.Log('_ii','error inserting idObj')
            throw err
         }
         else
         {
            cb(idObj)
         }
      })         
   }
   else
   {
      var obj = {}
      obj[socialProfile.type] = socialProfile.id
      idData.find(obj).limit(1).next(function(err,result)
      {
         if (err)
         {
            uDbg.Log('_ii','error getting id data for',socialProfile)
         }
         else
         {
            if (result)
            {
               cb(result)
            }
            else
            {
               lastId++
               var idObj = { id : lastId }
               idObj[socialProfile.type] = socialProfile.id
               idData.insertOne(idObj,function(err)
               {
                  if (err)
                  {
                     uDbg.Log('_ii','error inserting idObj')
                     throw err
                  }
                  else
                  {
                     cb(idObj)
                  }
               })
            }
         }
      })
   }
}

// module.exports.SaveIdData = function(socialProfile,gameId)
// {
   // if (socialProfile.type != "web")
   // {
      // var idData = db.collection('idData')
      // idData.replaceOne( { id : gameId }, { id : gameId, socialProfile.type : socialProfile.id }, { upsert : true }, function(err,results)
      // {
         // if (err)
         // {
            // uDbg.Log("_ii","Error saving iddata",socialProfile.id)
            // throw err
         // }         
      // })
   // }
// }

module.exports.GetSavedData = function(cb)
{
   if (err)
   {
      console.log('Couldnt connect to database!')
      uDbg.Log('_ii',"Couldnt connect to database!")
      return
   }   
   
   db.collection("savedData").find({ "data" : true }).limit(1).next(function(err,data)
   {
      if (err)
      {
         uDbg.Log("_ii","Error getting savedData")
      }
      else
      {
         cb(data)
      }
   })
}

module.exports.UpdateSavedData = function(upd,cb)
{
   if (err)
   {
      console.log('Couldnt connect to database!')
      uDbg.Log('_ii',"Couldnt connect to database!")
      return
   }
   
   db.collection("savedData").updateOne({ "data" : true }, upd, function(err, result)
   {
      if (err)
         uDbg.Log("_ii","Error updating savedData")
      if (cb)
         cb()
   })
}

module.exports.OnNewDay = function()
{
   uDbg.Log("_i","On new day")
   module.exports.prevDate = new Date()
   db.collection("savedData").updateOne({ "data" : true }, { "$set" : { "prevDateTime" : module.exports.prevDate.getTime() } })
}

module.exports.SaveProfile = function(gameProfile)
{
   var players = db.collection('players')
   players.replaceOne({ "id" : gameProfile.id }, gameProfile, { "upsert" : true }, function(err,results)
   {
      if (err)
      {
         uDbg.Log("_ii","Error saving player",gameProfile.id,"profile")
            throw err
      }
   })

   var leaderboard = db.collection('leaderboard')
   var lbObj = { "n" : gameProfile.name, "elo" : gameProfile.elo, "id" : gameProfile.id }
   leaderboard.replaceOne({ "id" : gameProfile.id }, lbObj, { "upsert" : true }, function(err,results)
   {
      if (err)
      {
         uDbg.Log("_ii","Error saving player",gameProfile.id,"leaderboard profle")
            throw err
      }
   })	   
}

module.exports.GetLeaderboard = function(cb)
{
   db.collection('leaderboard').find({}, { _id : 0 }).sort({ elo : -1}).limit(uPrefs.playersInLeaderboard).toArray(function(err,leaderboard)
   {
      cb(leaderboard)
   })
}

module.exports.GetPlayerLbEntry = function(cb,id)
{
   db.collection('leaderboard').find({ "id" : id }).limit(1).next(function(err,result)
   {
      db.collection('leaderboard').find({"elo" : { "$gt" : result.elo } }).count(function(err,count)
      {
         if (err)
         {
            uDbg.Log("_ii","Error in GetPlayerLbEntry:",err)
         }
         else
         {
            db.collection('leaderboard').find({"elo" : result.elo, "_id" : { "$lt" : uMongo.ObjectID(result._id) } }, { _id : 0}).count(function(err2,count2)
            {
               if (err2)
               {
                  uDbg.Log("_ii","Error in GetPlayerLbEntry:",err2)                    
               }
               else
               {
                  var pos = count+count2
                  result.p = pos
                  delete result._id
                  cb(result)
               }
            })
         }
      })
   })
}

module.exports.LoadProfile = function(id,cb)
{
   var players = db.collection('players')
   var findCursor = players.find({ "id" : id }).limit(1)
   findCursor.next(function(err,doc)
   {
      if (err)
         throw err
      cb(doc)
   })
}

var clear = 0

module.exports.ClearDatabase = function(cb)
{
   
   cb()
   
   //test
   // uMongoClient.connect(url, function(err, db) 
   // {
      // db.dropDatabase()
      // uMongoClient.connect(url, function(err, db2) 
      // {
         // db2.createCollection('players', function(err, collection) { clear++; if (clear === 4) cb(); })
         // db2.createCollection('leaderboard', function(err, collection) { clear++; if (clear === 4) cb(); })
         // db2.createCollection('idData', function(err, collection) { clear++; if (clear === 4) cb(); })
         // db2.createCollection('logs', function(err, collection) { clear++; if (clear === 4) cb(); })
      // })  
   // })
   
}