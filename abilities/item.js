var uRange = require('./abilityRange')
var uAffect = require('./abilityAffect')
var uAbility = require('./ability')
var uMisc = require('../misc')
var uDbg = require('../debug')
var uEffects = require('./effects')

var ex = module.exports

ex.ITEM_HEALTH_VIAL = "0"
ex.ITEM_BOW = "1"
ex.ITEM_POTION_OF_DARKNESS = "2"
ex.ITEM_FALSE_PLANS = "3"
ex.ITEM_AXE = "4"
ex.ITEM_SWORD = "5"
ex.ITEM_SHIELD = "6"
ex.ITEM_MINOR_HEALTH_POTION = "7"
ex.ITEM_MINOR_MANA_POTION = "8"
ex.ITEM_MANA_POTION = "9"
ex.ITEM_DYNAMITE = "10"
ex.ITEM_DYNAMITE_BUNDLE = "11"
ex.ITEM_MOLOTOV_COCKTAIL = "12"
ex.ITEM_DYE = "13"
ex.ITEM_POTION_OF_SHIELD = "14"
ex.ITEM_SYMBOL_OF_MANA = "15"
ex.ITEM_THROWING_AXE = "16"
ex.ITEM_LOCKET_OF_HEALTH = "17"
ex.ITEM_SYMBOL_OF_MANA_DRAIN = "18"
ex.ITEM_MINOR_SYMBOL_OF_MANA_DRAIN = "19"
ex.ITEM_MINOR_LOCKET_OF_HEALTH = "20"
ex.ITEM_FALLING_STAR = "21"
ex.ITEM_MINOR_FALLING_STAR = "22"
ex.ITEM_SYMBOL_OF_WARLOCK = "23"
ex.ITEM_SYMBOL_OF_SNOW = "24"
ex.ITEM_MINOR_SYMBOL_OF_SNOW = "25"
ex.ITEM_GREATER_SYMBOL_OF_MANA = "26"
ex.ITEM_KNIFE = "27"
ex.ITEM_MINOR_DYNAMITE = "28"
ex.ITEM_MINOR_SHIELD = "29"
ex.ITEM_LONGBOW = "30"
ex.ITEM_MEDIUM_SYMBOL_OF_SNOW = "31"
ex.ITEM_MEDIUM_FALLING_STAR = "32"

ex.aiItemTypes = []

ex.OwnedItemInfo = function(type)
{
   this.type = type
   this.num = 0
   this.eq = false
}

ex._itemAbilityDatas = []

ex._itemAbilityDatas[ex.ITEM_HEALTH_VIAL] =
{
   type : ex.ITEM_HEALTH_VIAL,
   cons: true, //consumable
   useable: true,  //useable
   item: true,
   actions : 
   [
      {                                                                      //create a new action,
         pickRangeType : uRange.RANGE_NONE,                                         //pick range for this action - automatic, no need to pick a target,
         affectRangeType : uRange.RANGE_YOUR_HERO,                                  //range for this action - your hero,
         showRangeType : uRange.RANGE_NONE,
         rangeArgs : [],                                                               //range args. empty this time.,
         affectType : uAffect.AFFECT_HEAL,                                               //affect type - heal
         affectArgs : [ 10 ],                                                           //affect args, specifying how affect should behave. empty              
         maxSelected : 0,
         minSelected : 0,
         minTargets : 1,
         maxTargets : 1
      }
   ]
}

ex._itemAbilityDatas[ex.ITEM_BOW] =
{
   type : ex.ITEM_BOW,
   cons : false,
   useable: true,
   item: true,
   chargeable: true,
   charge: 2,
   nmElim : true, //can negate no moves
   actions : 
   [
      {                                                                      //create a new action,
         pickRangeType : uRange.RANGE_SELECTED_STONE_ONLY,                         //pick range for this action - single stone,
         affectRangeType : uRange.RANGE_SELECTED_STONE_ONLY,       //range for this action - selected stone only
         showRangeType: uRange.RANGE_SELECTED_STONE_ONLY,            //show range for this action - selected stone only
         rangeArgs : [],  
         affectType : uAffect.AFFECT_DESTROY_BALLS,                                //affect - destroy balls,
         affectArgs : [],                                                           //affect args, specifying how affect should behave. empty,
         maxSelected : 1,
         minSelected : 1,
         minTargets : 1,
         maxTargets : 1
      }
   ]
}

ex._itemAbilityDatas[ex.ITEM_LONGBOW] =
{
   type : ex.ITEM_LONGBOW,
   cons : false,
   useable: true,
   item: true,
   nmElim : true, //can negate no moves
   actions : 
   [
      {                                                                      //create a new action,
         pickRangeType : uRange.RANGE_SELECTED_STONE_ONLY,                         //pick range for this action - single stone,
         affectRangeType : uRange.RANGE_SELECTED_STONE_ONLY,       //range for this action - selected stone only
         showRangeType: uRange.RANGE_SELECTED_STONE_ONLY,            //show range for this action - selected stone only
         rangeArgs : [],  
         affectType : uAffect.AFFECT_DESTROY_BALLS,                                //affect - destroy balls,
         affectArgs : [],                                                           //affect args, specifying how affect should behave. empty,
         maxSelected : 1,
         minSelected : 1,
         minTargets : 1,
         maxTargets : 1
      }
   ]
}

ex._itemAbilityDatas[ex.ITEM_POTION_OF_DARKNESS] =
{
   type : ex.ITEM_POTION_OF_DARKNESS,
   cons : true, //consumable
   useable : true,  //useable
   item : true,
   actions : 
   [
      {                                                                      //create a new action,
         pickRangeType : uRange.RANGE_NONE,                                         //pick range for this action - automatic, no need to pick a target,
         affectRangeType : uRange.RANGE_YOUR_HERO,                                  //range for this action - your hero,
         showRangeType : uRange.RANGE_NONE,
         rangeArgs : [],                                                               //range args. empty this time.,
         affectType : uAffect.AFFECT_DAMAGE,                                               //affect type - heal
         affectArgs : [ 15 ],                                                           //affect args, specifying how affect should behave. empty              
         maxSelected : 0,
         minSelected : 0,
         minTargets : 1,
         maxTargets : 1
      },
      {                                                                      //create a new action,
         pickRangeType : uRange.RANGE_NONE,                                         //pick range for this action - automatic, no need to pick a target,
         affectRangeType : uRange.RANGE_ENEMY_HERO,                                  //range for this action - your hero,
         showRangeType : uRange.RANGE_NONE,
         rangeArgs : [],                                                               //range args. empty this time.,
         affectType : uAffect.AFFECT_DAMAGE,                                               //affect type - damage
         affectArgs : [ 15 ],                                                           //affect args, specifying how affect should behave. empty              
         maxSelected : 0,
         minSelected : 0,
         minTargets : 1,
         maxTargets : 1
      }           
   ]
}

ex._itemAbilityDatas[ex.ITEM_FALSE_PLANS] =
{
   type : ex.ITEM_FALSE_PLANS,
   cons: false,
   useable: false,
   actType: uAbility.ACTIVATION_TYPE_PASSIVE_BEFORE_PICK,
   item: true,
   actions : 
   [
      {                                                             //create new action
         pickRangeType : uRange.RANGE_NONE,                //pick range for this action - single stone
         affectRangeType : uRange.RANGE_YOUR_HERO,    //range for this action - your hero
         showRangeType : uRange.RANGE_NONE,
         affectType : uAffect.AFFECT_APPLY_EFFECT,                         //affect apply effect
         rangeArgs : [],                                                //range args. empty this time.
         affectArgs : 
         [                                                 //affect args, specifying how affect should behave
            uEffects.EFFECT_FALSE_PLANS,                            //args[0] - what effect to apply to the targets
            []
         ],
         maxSelected : 0,
         minSelected : 0,
         minTargets : 1,
         maxTargets : 1
      }
   ]
}

ex._itemAbilityDatas[ex.ITEM_AXE] =
{
   type : ex.ITEM_AXE,
   cons: false, //consumable
   useable: true,  //useable
   item: true,
   chargeable: true,
   charge: 2,
   actions : 
   [
      {                                                                      //create a new action,
         pickRangeType : uRange.RANGE_NONE,                                         //pick range for this action - automatic, no need to pick a target,
         affectRangeType : uRange.RANGE_ENEMY_HERO,                                  //range for this action - your hero,
         showRangeType : uRange.RANGE_NONE,
         rangeArgs : [],                                                               //range args. empty this time.,
         affectType : uAffect.AFFECT_DAMAGE,                                               //affect type - damage
         affectArgs : [ 5 ],                                                           //affect args, specifying how affect should behave. empty              
         maxSelected : 0,
         minSelected : 0,
         minTargets : 1,
         maxTargets : 1
      }       
   ]
}

ex._itemAbilityDatas[ex.ITEM_SWORD] =
{
   type : ex.ITEM_SWORD,
   cons: false, //consumable
   useable: true,  //useable
   item: true,
   actions : 
   [
      {                                                                      //create a new action,
         pickRangeType : uRange.RANGE_NONE,                                         //pick range for this action - automatic, no need to pick a target,
         affectRangeType : uRange.RANGE_ENEMY_HERO,                                  //range for this action - your hero,
         showRangeType : uRange.RANGE_NONE,
         rangeArgs : [],                                                               //range args. empty this time.,
         affectType : uAffect.AFFECT_DAMAGE,                                               //affect type - damage
         affectArgs : [ 5 ],                                                           //affect args, specifying how affect should behave. empty              
         maxSelected : 0,
         minSelected : 0,
         minTargets : 1,
         maxTargets : 1
      },
      {                                                                      //create a new action,
         pickRangeType : uRange.RANGE_NONE,                                         //pick range for this action - automatic, no need to pick a target,
         affectRangeType : uRange.RANGE_ALL_YOUR_ABILITIES,                                  //range for this action - your hero,
         showRangeType : uRange.RANGE_NONE,
         rangeArgs : [],                                                               //range args. empty this time.,
         affectType : uAffect.AFFECT_CHARGE_ABILITY,                                               //affect type - heal
         affectArgs : [ 1 ],                                                           //affect args, specifying how affect should behave. empty              
         maxSelected : 0,
         minSelected : 0,
         minTargets : 1,
         maxTargets : 4
      },      
      {                                                             //create new action
         pickRangeType : uRange.RANGE_NONE,                //pick range for this action - single stone
         affectRangeType : uRange.RANGE_NONE,    //range for this action - selected stone and its neighbours
         showRangeType : uRange.RANGE_NONE,
         affectType : uAffect.AFFECT_PASS_TURN,                         //affect apply effect
         rangeArgs : [],                                                //range args. empty this time.
         affectArgs : [],
         maxSelected : 0,
         minSelected : 0,
         minTargets : 0,
         maxTargets : 0
      }            
   ]
}

ex._itemAbilityDatas[ex.ITEM_SHIELD] =
{
   type : ex.ITEM_SHIELD,
   cons: false, //consumable
   useable: false,  //useable
   actType: uAbility.ACTIVATION_TYPE_PASSIVE_ON_YOUR_TURN,
   item: true,
   actions : 
   [
      {                                                                             //create a new action,
         pickRangeType : uRange.RANGE_NONE,                                         //pick range for this action - automatic, no need to pick a target,
         affectRangeType : uRange.RANGE_YOUR_HERO,                                  //range for this action - your hero,
         showRangeType : uRange.RANGE_NONE, 
         affectType : uAffect.AFFECT_ADD_SHIELD,                                    //affect - heal,
         rangeArgs : [],                                                            //range args. empty this time.,
         affectArgs : [ 3 ],                                                        //affect args, specifying how affect should behave. empty
         maxSelected : 0,
         minSelected : 0,
         minTargets : 1,
         maxTargets : 1
      }
   ]
}

ex._itemAbilityDatas[ex.ITEM_MINOR_HEALTH_POTION] =
{
   type : ex.ITEM_MINOR_HEALTH_POTION,
   cons: true, //consumable
   useable: true,  //useable
   item: true,
   actions : 
   [
      {                                                                      //create a new action,
         pickRangeType : uRange.RANGE_NONE,                                         //pick range for this action - automatic, no need to pick a target,
         affectRangeType : uRange.RANGE_YOUR_HERO,                                  //range for this action - your hero,
         showRangeType : uRange.RANGE_NONE,
         rangeArgs : [],                                                               //range args. empty this time.,
         affectType : uAffect.AFFECT_HEAL,                                               //affect type - heal
         affectArgs : [ 4 ],                                                           //affect args, specifying how affect should behave. empty              
         maxSelected : 0,
         minSelected : 0,
         minTargets : 1,
         maxTargets : 1
      }
   ]
}

ex._itemAbilityDatas[ex.ITEM_MINOR_MANA_POTION] =
{
   type : ex.ITEM_MINOR_MANA_POTION,
   cons: true, //consumable
   useable: true,  //useable
   item: true,
   actions : 
   [
      {                                                                      //create a new action,
         pickRangeType : uRange.RANGE_NONE,                                         //pick range for this action - automatic, no need to pick a target,
         affectRangeType : uRange.RANGE_ALL_YOUR_ABILITIES,                                  //range for this action - your hero,
         showRangeType : uRange.RANGE_NONE,
         rangeArgs : [],                                                               //range args. empty this time.,
         affectType : uAffect.AFFECT_CHARGE_ABILITY,                                               //affect type - heal
         affectArgs : [ 1 ],                                                           //affect args, specifying how affect should behave. empty              
         maxSelected : 0,
         minSelected : 0,
         minTargets : 1,
         maxTargets : 4
      }
   ]
}

ex._itemAbilityDatas[ex.ITEM_MANA_POTION] =
{
   type : ex.ITEM_MANA_POTION,
   cons: true, //consumable
   useable: true,  //useable
   item: true,
   actions : 
   [
      {                                                                      //create a new action,
         pickRangeType : uRange.RANGE_NONE,                                         //pick range for this action - automatic, no need to pick a target,
         affectRangeType : uRange.RANGE_ALL_YOUR_ABILITIES,                                  //range for this action - your hero,
         showRangeType : uRange.RANGE_NONE,
         rangeArgs : [],                                                               //range args. empty this time.,
         affectType : uAffect.AFFECT_CHARGE_ABILITY,                                               //affect type - heal
         affectArgs : [ 3 ],                                                           //affect args, specifying how affect should behave. empty              
         maxSelected : 0,
         minSelected : 0,
         minTargets : 1,
         maxTargets : 4
      }
   ]
}

ex._itemAbilityDatas[ex.ITEM_MINOR_DYNAMITE] =
{
   type : ex.ITEM_MINOR_DYNAMITE,
   cons: true,
   useable: true,
   nmElim : true, //can negate no moves
   item: true,
   actions : 
   [
      {                                                                      //create a new action,
         pickRangeType : uRange.RANGE_NONE,                         //pick range for this action - single stone,
         affectRangeType : uRange.RANGE_RANDOM_STONES,               //range for this action - selected stone and its cross neighbours,
         showRangeType: uRange.RANGE_NONE,
         affectType : uAffect.AFFECT_DESTROY_BALLS,                                //affect - destroy balls,
         rangeArgs : [ 5 ],                                                            //range args. empty this time.,
         affectArgs : [ ],                                                           //affect args, specifying how affect should behave. empty,
         maxSelected : 0,
         minSelected : 0,
         minTargets : 5,
         maxTargets : 5
      }
   ]
}

ex._itemAbilityDatas[ex.ITEM_DYNAMITE] =
{
   type : ex.ITEM_DYNAMITE,
   cons: true,
   useable: true,
   nmElim : true, //can negate no moves
   item: true,
   actions : 
   [
      {                                                                      //create a new action,
         pickRangeType : uRange.RANGE_NONE,                         //pick range for this action - single stone,
         affectRangeType : uRange.RANGE_RANDOM_STONES,               //range for this action - selected stone and its cross neighbours,
         showRangeType: uRange.RANGE_NONE,
         affectType : uAffect.AFFECT_DESTROY_BALLS,                                //affect - destroy balls,
         rangeArgs : [ 10 ],                                                            //range args. empty this time.,
         affectArgs : [ ],                                                           //affect args, specifying how affect should behave. empty,
         maxSelected : 0,
         minSelected : 0,
         minTargets : 10,
         maxTargets : 10
      }
   ]
}

ex._itemAbilityDatas[ex.ITEM_DYNAMITE_BUNDLE] =
{
   type : ex.ITEM_DYNAMITE_BUNDLE,
   cons: false,
   useable: true,
   nmElim : true, //can negate no moves
   chargeable: true,
   charge: 3,
   item: true,
   actions : 
   [
      {                                                                      //create a new action,
         pickRangeType : uRange.RANGE_NONE,                         //pick range for this action - single stone,
         affectRangeType : uRange.RANGE_RANDOM_STONES,               //range for this action - selected stone and its cross neighbours,
         showRangeType: uRange.RANGE_NONE,
         affectType : uAffect.AFFECT_DESTROY_BALLS,                                //affect - destroy balls,
         rangeArgs : [ 6 ],                                                            //range args. empty this time.,
         affectArgs : [ ],                                                           //affect args, specifying how affect should behave. empty,
         maxSelected : 0,
         minSelected : 0,
         minTargets : 6,
         maxTargets : 6
      }
   ]
}

ex._itemAbilityDatas[ex.ITEM_MOLOTOV_COCKTAIL] =
{
   type : ex.ITEM_MOLOTOV_COCKTAIL,
   cons: true,
   useable: true,
   item: true,
   nmElim : true, //can negate no moves
   actions : 
   [
      {                                                                      //create a new action,
         pickRangeType : uRange.RANGE_SELECTED_STONE_ONLY,                         //pick range for this action - single stone,
         affectRangeType : uRange.RANGE_SELECTED_STONE_WITH_NEIGHBOURS,       //range for this action - selected stone and its cross neighbours,
         showRangeType: uRange.RANGE_SELECTED_STONE_WITH_NEIGHBOURS,
         affectType : uAffect.AFFECT_DESTROY_BALLS,                                //affect - destroy balls,
         visualType : uAbility.visual.VISUALIZE_BALL_PARTICLE,
         visualArgs : [ "fire", 0.6, 0.25 ],
         rangeArgs : [],                                                            //range args. empty this time.,
         affectArgs : [],                                                           //affect args, specifying how affect should behave. empty,
         maxSelected : 1,
         minSelected : 1,
         minTargets : 4,
         maxTargets : 9
      }
   ]
}

ex._itemAbilityDatas[ex.ITEM_DYE] =
{
   type : ex.ITEM_DYE,
   cons: false,
   useable: true,
   item: true,   
   nmElim : true, //can negate no moves
   chargeable : true,
   charge : 3,
   actions : 
   [
      {                                                                             //create a new action,
         pickRangeType : uRange.RANGE_SELECTED_STONE_ONLY,                          //pick range for this action - single stone,
         affectRangeType : uRange.RANGE_STONES_IN_SELECTED_CHAIN,                        //range for this action - selected stone only,
         showRangeType : uRange.RANGE_STONES_IN_SELECTED_CHAIN,
         affectType : uAffect.AFFECT_TURN_BALL_COLOR,                               //affect - change ball color,
         visualType : uAbility.visual.VISUALIZE_BALL_PARTICLE,
         visualArgs : [ "purpleM", 0.5 ],            
         rangeArgs : [],                                                            //range args. empty this time.,
         affectArgs : [],                                                           //affect args, specifying how affect should behave. empty,
         maxSelected : 1,
         minSelected : 1,
         minTargets : 1,
         maxTargets : 36
      }
   ]
}

ex._itemAbilityDatas[ex.ITEM_POTION_OF_SHIELD] =
{
   type : ex.ITEM_POTION_OF_SHIELD,
   cons: true,
   useable: true,
   item: true,   
   nmElim : false, //can negate no moves
   actions : 
   [
      {                                                                             //create a new action,
         pickRangeType : uRange.RANGE_NONE,                                         //pick range for this action - automatic, no need to pick a target,
         affectRangeType : uRange.RANGE_YOUR_HERO,                                  //range for this action - your hero,
         showRangeType : uRange.RANGE_NONE, 
         affectType : uAffect.AFFECT_ADD_SHIELD,                                    //affect - heal,
         rangeArgs : [],                                                            //range args. empty this time.,
         affectArgs : [ 15 ],                                                        //affect args, specifying how affect should behave. empty
         maxSelected : 0,
         minSelected : 0,
         minTargets : 1,
         maxTargets : 1
      }
   ]
}

ex._itemAbilityDatas[ex.ITEM_SYMBOL_OF_MANA] =
{
   type : ex.ITEM_SYMBOL_OF_MANA,
   cons: false, //consumable
   useable: false,  //useable
   actType: uAbility.ACTIVATION_TYPE_PASSIVE_ON_YOUR_TURN,
   item: true,
   actions : 
   [
      {                                                                             //create a new action,
         pickRangeType : uRange.RANGE_NONE,                                         //pick range for this action - automatic, no need to pick a target,
         affectRangeType : uRange.RANGE_YOUR_HERO,                                  //range for this action - your hero,
         showRangeType : uRange.RANGE_NONE, 
         affectType : uAffect.AFFECT_CHARGE_RANDOM_ABILITY,                                    //affect - heal,
         rangeArgs : [],                                                            //range args. empty this time.,
         affectArgs : [ 1 ],                                                        //affect args, specifying how affect should behave. empty
         maxSelected : 0,
         minSelected : 0,
         minTargets : 1,
         maxTargets : 1
      }
   ]
}

ex._itemAbilityDatas[ex.ITEM_THROWING_AXE] =
{
   type : ex.ITEM_THROWING_AXE,
   cons : false,
   useable: true,
   item: true,
   nmElim : true, //can negate no moves
   chargeable : true,
   charge : 3,
   actions : 
   [
      {                                                                      //create a new action,
         pickRangeType : uRange.RANGE_SELECTED_STONE_ONLY,                         //pick range for this action - single stone,
         affectRangeType : uRange.RANGE_SELECTED_STONE_ONLY,       //range for this action - selected stone only
         showRangeType: uRange.RANGE_SELECTED_STONE_ONLY,            //show range for this action - selected stone only
         rangeArgs : [],  
         affectType : uAffect.AFFECT_DESTROY_BALLS,                                //affect - destroy balls,
         affectArgs : [],                                                           //affect args, specifying how affect should behave. empty,
         maxSelected : 1,
         minSelected : 1,
         minTargets : 1,
         maxTargets : 1
      }
   ]
}

ex._itemAbilityDatas[ex.ITEM_LOCKET_OF_HEALTH] =
{
   type : ex.ITEM_LOCKET_OF_HEALTH,
   cons : false,
   useable: true,
   item: true,
   chargeable : true,
   charge : 3,   
   actions : 
   [
      {                                                                      //create a new action,
         pickRangeType : uRange.RANGE_NONE,                                         //pick range for this action - automatic, no need to pick a target,
         affectRangeType : uRange.RANGE_ALL_YOUR_ABILITIES,                                  //range for this action - your hero,
         showRangeType : uRange.RANGE_NONE, 
         affectType : uAffect.AFFECT_DRAIN_MANA_TO_HEALTH,                                          //affect - heal,
         rangeArgs : [],                                                            //range args. empty this time.,
         affectArgs : [ ],                                                           //affect args, specifying how affect should behave. empty              
         maxSelected : 0,
         minSelected : 0,
         minTargets : 1,
         maxTargets : 4
      }
   ]
}

ex._itemAbilityDatas[ex.ITEM_MINOR_LOCKET_OF_HEALTH] =
{
   type : ex.ITEM_MINOR_LOCKET_OF_HEALTH,
   cons : false,
   useable: true,
   item: true,
   chargeable : true,
   charge : 2,   
   actions : 
   [
      {                                                                      //create a new action,
         pickRangeType : uRange.RANGE_YOUR_SELECTED_ABILTIY,                                         //pick range for this action - automatic, no need to pick a target,
         affectRangeType : uRange.RANGE_YOUR_SELECTED_ABILTIY,                                  //range for this action - your hero,
         showRangeType : uRange.RANGE_NONE, 
         affectType : uAffect.AFFECT_DRAIN_MANA_TO_HEALTH,                                          //affect - heal,
         sChk : uAbility.CHECK_ABILITY_HAS_CHARGE,
         sChkArgs : [ 1 ],
         rangeArgs : [],                                                            //range args. empty this time.,
         affectArgs : [],                                                           //affect args, specifying how affect should behave. empty              
         maxSelected : 1,
         minSelected : 1,
         minTargets : 1,
         maxTargets : 1
      }
   ]
}

ex._itemAbilityDatas[ex.ITEM_SYMBOL_OF_MANA_DRAIN] =
{
   type : ex.ITEM_SYMBOL_OF_MANA_DRAIN,
   cons: false, //consumable
   useable: true,  //useable
   item: true,
   chargeable : true,
   charge : 2,
   actions : 
   [
      {                                                                             //create a new action,
         pickRangeType : uRange.RANGE_ENEMY_SELECTED_ABILITY,                                         //pick range for this action - automatic, no need to pick a target,
         affectRangeType : uRange.RANGE_ENEMY_SELECTED_ABILITY,                                  //range for this action - your hero,
         showRangeType : uRange.RANGE_NONE, 
         affectType : uAffect.AFFECT_DRAIN_MANA,                                    //affect - heal,
         rangeArgs : [],                                                            //range args. empty this time.,
         affectArgs : [],                                                        //affect args, specifying how affect should behave. empty
         maxSelected : 1,
         minSelected : 1,
         minTargets : 1,
         maxTargets : 1
      }
   ]
}

ex._itemAbilityDatas[ex.ITEM_MINOR_SYMBOL_OF_MANA_DRAIN] =
{
   type : ex.ITEM_MINOR_SYMBOL_OF_MANA_DRAIN,
   cons: false, //consumable
   useable: true,  //useable
   item: true,
   chargeable : true,
   charge : 2,
   actions : 
   [
      {                                                                             //create a new action,
         pickRangeType : uRange.RANGE_ENEMY_SELECTED_ABILITY,                                         //pick range for this action - automatic, no need to pick a target,
         affectRangeType : uRange.RANGE_ENEMY_SELECTED_ABILITY,                                  //range for this action - your hero,
         showRangeType : uRange.RANGE_NONE, 
         affectType : uAffect.AFFECT_DRAIN_MANA,                                    //affect - heal,
         rangeArgs : [],                                                            //range args. empty this time.,
         affectArgs : [ 2 ],                                                        //affect args, specifying how affect should behave. empty
         maxSelected : 1,
         minSelected : 1,
         minTargets : 1,
         maxTargets : 1
      }
   ]
}

ex._itemAbilityDatas[ex.ITEM_FALLING_STAR] =
{
   type : ex.ITEM_FALLING_STAR,
   cons: false, //consumable
   useable: false,  //useable
   actType: uAbility.ACTIVATION_TYPE_PASSIVE_ON_ENEMY_TURN,
   item: true,
   actions : 
   [
      {                                                                             //create a new action,
         pickRangeType : uRange.RANGE_NONE,                                         //pick range for this action - automatic, no need to pick a target,
         affectRangeType : uRange.RANGE_RANDOM_STONES,                                  //range for this action - your hero,
         showRangeType : uRange.RANGE_NONE, 
         affectType : uAffect.AFFECT_DESTROY_BALLS,                                    //affect - heal,
         visualType : uAbility.visual.VISUALIZE_DROP_BALL_VISUAL,
         visualArgs : [ "fallingStar", 1, "rot" ],         
         rangeArgs : [ 3 ],                                                            //range args. empty this time.,
         affectArgs : [ ],                                                        //affect args, specifying how affect should behave. empty
         maxSelected : 0,
         minSelected : 0,
         minTargets : 3,
         maxTargets : 3
      }
   ]
}

ex._itemAbilityDatas[ex.ITEM_MINOR_FALLING_STAR] =
{
   type : ex.ITEM_MINOR_FALLING_STAR,
   cons: false, //consumable
   useable: false,  //useable
   actType: uAbility.ACTIVATION_TYPE_PASSIVE_ON_ENEMY_TURN,
   item: true,
   actions : 
   [
      {                                                                             //create a new action,
         pickRangeType : uRange.RANGE_NONE,                                         //pick range for this action - automatic, no need to pick a target,
         affectRangeType : uRange.RANGE_RANDOM_STONES,                                  //range for this action - your hero,
         showRangeType : uRange.RANGE_NONE, 
         affectType : uAffect.AFFECT_DESTROY_BALLS,                                    //affect - heal,
         visualType : uAbility.visual.VISUALIZE_DROP_BALL_VISUAL,
         visualArgs : [ "fallingStar", 1, "rot" ],         
         rangeArgs : [ 1 ],                                                            //range args. empty this time.,
         affectArgs : [ ],                                                        //affect args, specifying how affect should behave. empty
         maxSelected : 0,
         minSelected : 0,
         minTargets : 1,
         maxTargets : 1
      }
   ]
}

ex._itemAbilityDatas[ex.ITEM_MEDIUM_FALLING_STAR] =
{
   type : ex.ITEM_MEDIUM_FALLING_STAR,
   cons: false, //consumable
   useable: false,  //useable
   actType: uAbility.ACTIVATION_TYPE_PASSIVE_ON_ENEMY_TURN,
   item: true,
   actions : 
   [
      {                                                                             //create a new action,
         pickRangeType : uRange.RANGE_NONE,                                         //pick range for this action - automatic, no need to pick a target,
         affectRangeType : uRange.RANGE_RANDOM_STONES,                                  //range for this action - your hero,
         showRangeType : uRange.RANGE_NONE, 
         affectType : uAffect.AFFECT_DESTROY_BALLS,                                    //affect - heal,
         visualType : uAbility.visual.VISUALIZE_DROP_BALL_VISUAL,
         visualArgs : [ "fallingStar", 1, "rot" ],         
         rangeArgs : [ 2 ],                                                            //range args. empty this time.,
         affectArgs : [ ],                                                        //affect args, specifying how affect should behave. empty
         maxSelected : 0,
         minSelected : 0,
         minTargets : 2,
         maxTargets : 2
      }
   ]
}

ex._itemAbilityDatas[ex.ITEM_SYMBOL_OF_WARLOCK] =
{
   type : ex.ITEM_SYMBOL_OF_WARLOCK,
   cons: false, //consumable
   useable: true,  //useable
   item: true,  
   actions : 
   [
      {                                                             //create new action
         pickRangeType : uRange.RANGE_NONE,                //pick range for this action - single stone
         affectRangeType : uRange.RANGE_ALL_YOUR_ABILITIES,    //range for this action - selected stone and its neighbours
         showRangeType : uRange.RANGE_NONE,
         affectType : uAffect.AFFECT_CHARGE_ABILITY,                         //affect change ability - add mana to the player
         rangeArgs : [],                                                //range args. empty this time.
         affectArgs : [ 8 ],
         maxSelected : 0,
         minSelected : 0,
         minTargets : 1,
         maxTargets : 4
      },
      {                                                             //create new action
         pickRangeType : uRange.RANGE_NONE,                //pick range for this action - single stone
         affectRangeType : uRange.RANGE_YOUR_HERO,    //range for this action - selected stone and its neighbours
         showRangeType : uRange.RANGE_NONE,
         affectType : uAffect.AFFECT_DAMAGE,                         //affect change ability - add mana to the player
         rangeArgs : [],                                                //range args. empty this time.
         affectArgs : [ 10 ],
         maxSelected : 0,
         minSelected : 0,
         minTargets : 1,
         maxTargets : 1
      }
   ]
}

ex._itemAbilityDatas[ex.ITEM_SYMBOL_OF_SNOW] =
{
   type : ex.ITEM_SYMBOL_OF_SNOW,
   cons: false, //consumable
   useable: false,  //useable
   actType: uAbility.ACTIVATION_TYPE_PASSIVE_ON_ENEMY_TURN,
   item: true,
   actions : 
   [
      {                                                                             //create a new action,
         pickRangeType : uRange.RANGE_NONE,                                         //pick range for this action - automatic, no need to pick a target,
         affectRangeType : uRange.RANGE_RANDOM_STONES,                                  //range for this action - your hero,
         showRangeType : uRange.RANGE_NONE, 
         affectType : uAffect.AFFECT_APPLY_EFFECT,                                    //affect - apply effect,
         affectArgs :                                                                  //affect args, specifying how affect should behave. empty
         [ 
            uEffects.EFFECT_FROZEN,
            [ 1 ]
         ], 
         visualType : uAbility.visual.VISUALIZE_DROP_BALL_VISUAL,
         visualArgs : [ "snowflake", 1, "rot" ],         
         rangeArgs : [ 6 ],                                                            //range args. empty this time.,        
         maxSelected : 0,
         minSelected : 0,
         minTargets : 6,
         maxTargets : 6
      }
   ]
}

ex._itemAbilityDatas[ex.ITEM_MINOR_SYMBOL_OF_SNOW] =
{
   type : ex.ITEM_MINOR_SYMBOL_OF_SNOW,
   cons: false,      //consumable
   useable: false,   //useable
   actType: uAbility.ACTIVATION_TYPE_PASSIVE_ON_ENEMY_TURN,
   item: true,
   actions : 
   [
      {                                                                             //create a new action,
         pickRangeType : uRange.RANGE_NONE,                                         //pick range for this action - automatic, no need to pick a target,
         affectRangeType : uRange.RANGE_RANDOM_STONES,                                  //range for this action - your hero,
         showRangeType : uRange.RANGE_NONE, 
         affectType : uAffect.AFFECT_APPLY_EFFECT,                                    //affect - apply effect,
         affectArgs :                                                                  //affect args, specifying how affect should behave. empty
         [ 
            uEffects.EFFECT_FROZEN,
            [ 1 ]
         ], 
         visualType : uAbility.visual.VISUALIZE_DROP_BALL_VISUAL,
         visualArgs : [ "snowflake", 1, "rot" ],         
         rangeArgs : [ 3 ],                                                            //range args. empty this time.,        
         maxSelected : 0,
         minSelected : 0,
         minTargets : 3,
         maxTargets : 3
      }
   ]
}

ex._itemAbilityDatas[ex.ITEM_MEDIUM_SYMBOL_OF_SNOW] =
{
   type : ex.ITEM_MEDIUM_SYMBOL_OF_SNOW,
   cons: false,      //consumable
   useable: false,   //useable
   actType: uAbility.ACTIVATION_TYPE_PASSIVE_ON_ENEMY_TURN,
   item: true,
   actions : 
   [
      {                                                                             //create a new action,
         pickRangeType : uRange.RANGE_NONE,                                         //pick range for this action - automatic, no need to pick a target,
         affectRangeType : uRange.RANGE_RANDOM_STONES,                                  //range for this action - your hero,
         showRangeType : uRange.RANGE_NONE, 
         affectType : uAffect.AFFECT_APPLY_EFFECT,                                    //affect - apply effect,
         affectArgs :                                                                  //affect args, specifying how affect should behave. empty
         [ 
            uEffects.EFFECT_FROZEN,
            [ 1 ]
         ], 
         visualType : uAbility.visual.VISUALIZE_DROP_BALL_VISUAL,
         visualArgs : [ "snowflake", 1, "rot" ],         
         rangeArgs : [ 4 ],                                                            //range args. empty this time.,        
         maxSelected : 0,
         minSelected : 0,
         minTargets : 4,
         maxTargets : 4
      }
   ]
}

ex._itemAbilityDatas[ex.ITEM_GREATER_SYMBOL_OF_MANA] =
{
   type : ex.ITEM_GREATER_SYMBOL_OF_MANA,
   cons: false, //consumable
   useable: false,  //useable
   actType: uAbility.ACTIVATION_TYPE_PASSIVE_ON_YOUR_TURN,
   item: true,
   actions : 
   [
      {                                                                      //create a new action,
         pickRangeType : uRange.RANGE_NONE,                                         //pick range for this action - automatic, no need to pick a target,
         affectRangeType : uRange.RANGE_ALL_YOUR_ABILITIES,                                  //range for this action - your hero,
         showRangeType : uRange.RANGE_NONE,
         rangeArgs : [],                                                               //range args. empty this time.,
         affectType : uAffect.AFFECT_CHARGE_ABILITY,                                               //affect type - heal
         affectArgs : [ 1 ],                                                           //affect args, specifying how affect should behave. empty              
         maxSelected : 0,
         minSelected : 0,
         minTargets : 1,
         maxTargets : 4
      }
   ]
}

ex._itemAbilityDatas[ex.ITEM_KNIFE] =
{
   type : ex.ITEM_KNIFE,
   cons: false, //consumable
   useable: true,  //useable
   item: true,
   actions : 
   [
      {                                                                      //create a new action,
         pickRangeType : uRange.RANGE_NONE,                                         //pick range for this action - automatic, no need to pick a target,
         affectRangeType : uRange.RANGE_ENEMY_HERO,                                  //range for this action - your hero,
         showRangeType : uRange.RANGE_NONE,
         rangeArgs : [],                                                               //range args. empty this time.,
         affectType : uAffect.AFFECT_DAMAGE,                                               //affect type - damage
         affectArgs : [ 1 ],                                                           //affect args, specifying how affect should behave. empty              
         maxSelected : 0,
         minSelected : 0,
         minTargets : 1,
         maxTargets : 1
      }       
   ]
}

ex._itemAbilityDatas[ex.ITEM_MINOR_SHIELD] =
{
   type : ex.ITEM_MINOR_SHIELD,
   cons: false, //consumable
   useable: false,  //useable
   actType: uAbility.ACTIVATION_TYPE_PASSIVE_ON_YOUR_TURN,
   item: true,
   actions : 
   [
      {                                                                             //create a new action,
         pickRangeType : uRange.RANGE_NONE,                                         //pick range for this action - automatic, no need to pick a target,
         affectRangeType : uRange.RANGE_YOUR_HERO,                                  //range for this action - your hero,
         showRangeType : uRange.RANGE_NONE, 
         affectType : uAffect.AFFECT_ADD_SHIELD,                                    //affect - heal,
         rangeArgs : [],                                                            //range args. empty this time.,
         affectArgs : [ 1 ],                                                        //affect args, specifying how affect should behave. empty
         maxSelected : 0,
         minSelected : 0,
         minTargets : 1,
         maxTargets : 1
      }
   ]
}

ex.Item = function(type)
{
   this.ability = new uAbility.Ability(ex._itemAbilityDatas[type])
}

ex.aiItemTypes = [ ex.ITEM_KNIFE, ex.ITEM_AXE, ex.ITEM_THROWING_AXE, ex.ITEM_SYMBOL_OF_MANA, ex.ITEM_MINOR_SYMBOL_OF_SNOW, ex.ITEM_DYNAMITE, ex.ITEM_MINOR_DYNAMITE, ex.ITEM_BOW ]

ex.GetRandomAiItem = function()
{
   var t = uMisc.Random(0, ex.aiItemTypes.length)
   return new ex.Item(ex.aiItemTypes[t])
}

// for (var key in _itemAbilityDatas)
// {
   // var abData = _itemAbilityDatas[key]
   // if (abData.actions.length === 1)
   // {
      // if (abData.actType || uAffect.aiSupportedTypes.has(abData.actions[0].affectType))
         // ex.aiItemTypes.push(key)
   // }   
// }
