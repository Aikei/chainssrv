var uRange = require('./abilityRange')
var uAction = require('./abilityAction')
var uAffect = require('./abilityAffect')
var uAbility = require('./ability')
var uPrefs = require('../preferences')
var uDbg = require('../debug')
var uConsts = require('../constants')
var uEffect = require('../abilities/effects')
var uMisc = require('../misc')

var abilities = {}

abilities[uAbility.ABILITY_FREEZE] =
{
   type : uAbility.ABILITY_FREEZE,
   color : uConsts.BALL_BLUE,
   cost : 3,
   actions : 
   [
      {                                                             //create new action
         pickRangeType : uRange.RANGE_SELECTED_STONE_ONLY,                //pick range for this action - single stone
         affectRangeType : uRange.RANGE_SELECTED_STONE_WITH_NEIGHBOURS,    //range for this action - selected stone and its neighbours
         showRangeType : uRange.RANGE_SELECTED_STONE_WITH_NEIGHBOURS,
         affectType : uAffect.AFFECT_APPLY_EFFECT,                         //affect apply effect
         rangeArgs : [],                                                //range args. empty this time.
         affectArgs : 
         [                                                 //affect args, specifying how affect should behave
            uEffect.EFFECT_FROZEN,                            //args[0] - what effect to apply to the targets
            [ 2 ],                                               //args[1] - effect args
         ],
         maxSelected : 1,
         minSelected : 1,
         minTargets : 4,
         maxTargets : 9
      }
   ]
}

abilities[uAbility.ABILITY_FIRE] =
{
   type : uAbility.ABILITY_FIRE,
   color : uConsts.BALL_RED,
   cost : 6,
   actions : 
   [
      {                                                                      //create a new action,
         pickRangeType : uRange.RANGE_SELECTED_STONE_ONLY,                         //pick range for this action - single stone,
         affectRangeType : uRange.RANGE_SELECTED_STONE_WITH_CROSS_NEIGHBOURS,       //range for this action - selected stone and its cross neighbours,
         showRangeType: uRange.RANGE_SELECTED_STONE_WITH_CROSS_NEIGHBOURS,
         affectType : uAffect.AFFECT_DESTROY_BALLS,                                //affect - destroy balls,
         rangeArgs : [],                                                            //range args. empty this time.,
         affectArgs : [],                                                           //affect args, specifying how affect should behave. empty,
         maxSelected : 1,
         minSelected : 1,
         minTargets : 3,
         maxTargets : 5
      }
   ]
}

abilities[uAbility.ABILITY_TURN] =
{
   type : uAbility.ABILITY_TURN,
   color : uConsts.BALL_PURPLE,
   cost : 4,
   actions : 
   [
      {                                                                             //create a new action,
         pickRangeType : uRange.RANGE_SELECTED_STONE_ONLY,                          //pick range for this action - single stone,
         affectRangeType : uRange.RANGE_SELECTED_STONE_ONLY,                        //range for this action - selected stone only,
         showRangeType : uRange.RANGE_SELECTED_STONE_ONLY,
         affectType : uAffect.AFFECT_TURN_BALL_COLOR,                               //affect - change ball color,
         rangeArgs : [],                                                            //range args. empty this time.,
         affectArgs : [],                                                           //affect args, specifying how affect should behave. empty,
         maxSelected : 1,
         minSelected : 1,
         minTargets : 1,
         maxTargets : 1,
         pickArgs : uAbility.PICK_ARGS_STONE_COLOR
      }
   ]
}

abilities[uAbility.ABILITY_HEAL] =
{
   type : uAbility.ABILITY_HEAL,
   color : uConsts.BALL_GREEN,
   cost : 6,
   actions : 
   [
      {                                                                      //create a new action,
         pickRangeType : uRange.RANGE_NONE,                                         //pick range for this action - automatic, no need to pick a target,
         affectRangeType : uRange.RANGE_YOUR_HERO,                                  //range for this action - your hero,
         showRangeType : uRange.RANGE_NONE, 
         affectType : uAffect.AFFECT_HEAL,                                          //affect - heal,
         rangeArgs : [],                                                            //range args. empty this time.,
         affectArgs : [ 7 ],                                                           //affect args, specifying how affect should behave. empty,
         maxSelected : 0,
         minSelected : 0,
         minTargets : 1,
         maxTargets : 1
      }
   ]
}
console.log('abilities',abilities)

module.exports = abilities