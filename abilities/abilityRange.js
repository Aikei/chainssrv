var ex = module.exports

ex.RANGE_NONE = 0

ex.RANGE_SELECTED_STONE_ONLY = 0x1
ex.RANGE_SELECTED_STONE_WITH_NEIGHBOURS = 0x2
ex.RANGE_SELECTED_STONE_WITH_CROSS_NEIGHBOURS = 0x4
ex.RANGE_SELECTED_STONE_ROW_AND_COLUMN = 0x8
ex.RANGE_ALL_STONES_OF_SPECIFIC_COLOR = 0x10
ex.RANGE_STONES_IN_SELECTED_CHAIN = 0x20

ex.SELECT_STONE = 0x7F

ex.RANGE_YOUR_HERO = 0x100
ex.RANGE_ENEMY_HERO = 0x200
ex.RANGE_RANDOM_STONES = 0x400
ex.RANGE_ALL_STONES = 0x800
ex.RANGE_ALL_STONES_IN_ROWS_AND_COLUMNS_OF_THE_SAME_COLOR = 0x1000
ex.RANGE_YOUR_SELECTED_ABILTIY = 0x2000
ex.RANGE_STONES_WITH_OWN_EFFECTS = 0x4000
ex.RANGE_ALL_YOUR_ABILITIES = 0x8000
ex.RANGE_ALL_GEMS_IN_CHAINS_OF_SPECIFIC_LENGTH = 0x10000
ex.RANGE_ENEMY_SELECTED_ABILITY = 0x20000

ex.Range = function(type,thisAction)
{
   this.rangeType = type
   this.args = []
   this.targets = []
   this.action = thisAction
}

ex.Range.prototype.GetTargets = function(game,selected,rArgs)
{
   this.targets = []
   switch (this.rangeType)
   {
      case ex.RANGE_SELECTED_STONE_ONLY:
         for (var i = 0; i < selected.length; i++)
            this.targets.push(game.field[selected[i].x][selected[i].y])
         break
      
      case ex.RANGE_SELECTED_STONE_WITH_NEIGHBOURS:
         for (var i = 0; i < selected.length; i++)
         {
            var ball = game.field[selected[i].x][selected[i].y]
            this.targets = this.targets.concat(game.GetNeighbours(ball))
            if (this.args.length > 0)
            {
               if (this.args[0] !== "no")
                  this.targets.push(ball)
            }
            else
               this.targets.push(ball)
         }
         break
      
      case ex.RANGE_SELECTED_STONE_WITH_CROSS_NEIGHBOURS:
         for (var i = 0; i < selected.length; i++)
         {
            var ball = game.field[selected[i].x][selected[i].y]
            this.targets = this.targets.concat(game.GetCrossNeighbours(ball))
            if (this.args.length > 0)
            {
               if (this.args[0] !== "no")
                  this.targets.push(ball)
            }
            else
               this.targets.push(ball)
         }
         break
      
      case ex.RANGE_YOUR_HERO:
         this.targets.push(this.action.ability.owner)
         //this.targets.push(game.GetMovingPlayer())
         break
      
      case ex.RANGE_ENEMY_HERO:
         this.targets.push(game.GetEnemyOfPlayer(this.action.ability.owner))        
         //this.targets.push(game.GetEnemyOfMovingPlayer())
         break
         
      case ex.RANGE_RANDOM_STONES:
         for (var i = 0; i < selected.length; i++)
            this.targets.push(game.field[selected[i].x][selected[i].y])
         this.targets = game.GetRandomBalls(this.args[0],this.targets)
         break
         
      case ex.RANGE_ALL_STONES:
         for (var i = 0; i < game.field.length; i++)
         {
            this.targets = this.targets.concat(game.field[i])
         }      
         break
         
      case ex.RANGE_ALL_STONES_OF_SPECIFIC_COLOR:
         for (var i = 0; i < game.field.length; i++)
         {
            for (var j = 0; j < game.field[i].length; j++)
            {
               if (game.field[i][j].type === this.args[0])
                  this.targets.push(game.field[i][j])
            }
         }      
         break         
         
      case ex.RANGE_SELECTED_STONE_ROW_AND_COLUMN:
         for (var i = 0; i < selected.length; i++)
         {
            var thisBall = game.field[selected[i].x][selected[i].y]
            if (this.args.length === 0)
            {
               var v = game.GetBallsForEliminateRows(thisBall,true)              
            }
            else if (this.args[0] === "r")
            {
               v = game.GetBallsInRow(thisBall,true)
            }
            else if (this.args[0] === "c")
            {
               v = game.GetBallsInColumn(thisBall,true)
            }
            v.push(thisBall)
            this.targets = this.targets.concat(v)
         }
         break
      
      case ex.RANGE_ALL_STONES_IN_ROWS_AND_COLUMNS_OF_THE_SAME_COLOR:
         var result = []
         var tempBallsX = []
         var tempBallsY = []
         
         for (var i = 0; i < game.field.length; i++)
         {
            tempBallsX.push(game.field[i].concat())
            tempBallsY.push(game.field[i].concat())
         }
         
         var balls = game.field
         
         for (var i = 0; i < balls.length; i++)
         {
            for (var j = 0; j < balls[i].length; j++)
            {
               for (var axis = 0; axis < 2; axis++) //axis: 0 = x, 1 = y
               {
                  var nFound = 1 //number of found matching balls of the same color
                  var checkx = i
                  var checky = j
                  for (var k = 1; k < 6; k++)
                  {
                     if (axis === 0)
                        checkx++
                     else
                        checky++
                     if (checkx < balls.length && checky < balls[0].length && balls[i][j].type === balls[checkx][checky].type
                        && ((axis === 0 && tempBallsX[i][j] !== null) ||  (axis === 1 && tempBallsY[i][j] !== null)) )
                     {
                        nFound++
                     }
                     else if (nFound < this.args[0]) //args[0] - min line length
                     {
                        break
                     }
                     else
                     {
                        var v = []
                        for (var a = 0; a < nFound; a++)
                        {
                           if (axis === 0)
                           {
                              tempBallsX[i + a][j] = null
                              v.push(balls[i + a][j])
                           }
                           else
                           {
                              tempBallsY[i][j + a] = null
                              v.push(balls[i][j + a])
                           }
                        }
                        result.push(v)
                        break
                     }
                  }
               }
            }
         }
         for (var i = 0; i < result.length; i++)
         {
            this.targets = this.targets.concat(result[i])
         }
         this.targets = game.RemoveSameBalls(this.targets)      
         break
         
      case ex.RANGE_YOUR_SELECTED_ABILTIY:
         var abs = this.action.ability.owner.abilities
         for (var i = 0; i < selected.length; i++)
         {
            for (var key in abs)
            {
               if (selected[i].aT === abs[key].type)
               {
                  this.targets.push(abs[key])
                  break
               }    
            }
         }
         break
         
      case ex.RANGE_ENEMY_SELECTED_ABILITY:
         var abs = game.GetEnemyOfPlayer(this.action.ability.owner).abilities
         for (var i = 0; i < selected.length; i++)
         {
            for (var key in abs)
            {
               if (selected[i].aT === abs[key].type)
               {
                  this.targets.push(abs[key])
                  break
               }    
            }
         }
         break      
      
      case ex.RANGE_ALL_YOUR_ABILITIES:
         var abs = game.GetMovingPlayer().abilities
         for (var key in abs)
         {
            this.targets.push(abs[key])
         }
         break
      
      case ex.RANGE_STONES_IN_SELECTED_CHAIN:
         for (var i = 0; i < selected.length; i++)
         {
            var thisBall = game.field[selected[i].x][selected[i].y]            
            var v = game.GetChain(thisBall)      
            this.targets = this.targets.concat(v)
         }   
         break
         
      case ex.RANGE_STONES_WITH_OWN_EFFECTS:
         var playerNumber = game.GetMovingPlayer().number
         for (var i = 0; i < game.field.length; i++)
         {
            for (var j = 0; j < game.field.length; j++)
            {
               if (game.field[i][j].HasOwningEffect(playerNumber))
               {
                  this.targets.push(game.field[i][j])
               }
            }
         }
         break
         
      case ex.RANGE_ALL_GEMS_IN_CHAINS_OF_SPECIFIC_LENGTH:
         for (var i = 0; i < game.field.length; i++)
         {
            for (var j = 0; j < game.field.length; j++)
            {       
               var chain = game.GetChain(game.field[i][j],false)
               var l = chain.length
               if (l >= this.args[0] && (this.args.length === 1 || l <= this.args[1]))
                  this.targets = this.targets.concat(chain)
            }
         }
         this.targets = game.RemoveSameBalls(this.targets)
         break
   }
   return this.targets
}

ex.Range.prototype.SetArgs = function(args)
{
   this.args = args
}