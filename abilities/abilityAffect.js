var uMisc = require('../misc.js')
var uConsts = require('../constants.js')
var uEffects = require('./effects.js')

var ex = module.exports

ex.AFFECT_NONE = 0 //no args
ex.AFFECT_REMOVE_BALLS = 1 //no args
ex.AFFECT_DESTROY_BALLS = 2 //no args
ex.AFFECT_DESTROY_BALLS_BUT_DONT_GIVE_MANA = 3 //no args
ex.AFFECT_APPLY_EFFECT = 4 //arg[0] - effect type, arg[1] - array of effect arguments
ex.AFFECT_DAMAGE = 5  //arg[0] - damage value
ex.AFFECT_HEAL = 6   //arg[0] - heal value
ex.AFFECT_TURN_BALL_COLOR = 7 //tArgs[0] - new ball type
ex.AFFECT_ADD_SHIELD = 8
ex.AFFECT_APPLY_GLOBAL_EFFECT = 9
ex.AFFECT_CHARGE_RANDOM_ABILITY = 10
ex.AFFECT_PASS_TURN = 11
ex.AFFECT_CHARGE_ABILITY = 12
ex.AFFECT_SPREAD_OWN_EFFECT = 13
ex.AFFECT_DRAIN_MANA_TO_HEALTH = 14
ex.AFFECT_DRAIN_MANA = 15

ex.aiSupportedTypes = new Set(
[ 
   ex.AFFECT_ADD_SHIELD, ex.AFFECT_DAMAGE, ex.AFFECT_REMOVE_BALLS, ex.AFFECT_HEAL, ex.AFFECT_TURN_BALL_COLOR,
	ex.AFFECT_DESTROY_BALLS, ex.AFFECT_DESTROY_BALLS_BUT_DONT_GIVE_MANA, ex.AFFECT_APPLY_EFFECT
])

var uGlobalEffects = require('./globalEffects')
var uAbility = require('./ability.js')
var uPrefs = require('../preferences')
var uDbg = require('../debug')



ex.Affect = function(affectType,thisAction)
{
   this.affectType = affectType
   this.args = []
   this.action = thisAction
}

ex.Affect.prototype.Affect = function(game,targets,tArgs)
{

   switch(this.affectType)
   {
      case ex.AFFECT_REMOVE_BALLS:         
         game.JustRemoveBalls(targets)
         game.MoveBallsAfterRemoving("ab_rem")
         //game.SendRefreshPlayerData()
         break
      
      case ex.AFFECT_DESTROY_BALLS:
         game.DamageEnemy(targets,this.action.ability.owner)
         game.ChargeAbilities(targets,this.action.ability.owner)
         game.JustRemoveBalls(targets)
         game.MoveBallsAfterRemoving("ab_rem")         
         break
      
      case ex.AFFECT_DESTROY_BALLS_BUT_DONT_GIVE_MANA:
         game.DamageEnemy(targets,this.action.ability.owner)
         game.JustRemoveBalls(targets)
         game.MoveBallsAfterRemoving("ab_rem")         
         break         

      
      case ex.AFFECT_APPLY_EFFECT:
         var args1 = this.args[1].concat()
         args1.push(game)
         for (var i = 0; i < targets.length; i++)
         {
            var effect = uMisc.Construct(uEffects["Effect"+this.args[0]],args1)
            targets[i].AddEffect(effect)
         }
         break
      
      case ex.AFFECT_DAMAGE:
         for (var i = 0; i < targets.length; i++)
         {           
            targets[i].Damage(this.args[0])
         }
         break
         
      case ex.AFFECT_HEAL:
         for (var i = 0; i < targets.length; i++)
         {           
            targets[i].Heal(this.args[0])
         }
         break
         
      case ex.AFFECT_TURN_BALL_COLOR:         
         if (tArgs && tArgs.length > 0)
         {
            var toClr = tArgs[0]
         }
         else if (this.args.length > 0)
         {
            toClr = this.args[0]
         }
         else
         {
            do
            {
               toClr = game.MtRandom(0,uConsts.NUMBER_OF_USED_BALL_TYPES)
               if (toClr === uConsts.SIXTH_STONE)
                  toClr = game.GetSixthGem()
            } while (toClr === targets[0].type)
         }         
         for (var i = 0; i < targets.length; i++)
         {
            targets[i].specialType = 0
            targets[i].type = toClr
         }         
         break
      
      case ex.AFFECT_ADD_SHIELD:
         for (var i = 0; i < targets.length; i++)
         {    
            if (targets[i].shield === undefined)
               throw "trying to add shield to a non-player object!"
            targets[i].shield += this.args[0]
         }
         break
      
      case ex.AFFECT_APPLY_GLOBAL_EFFECT:
         var args1 = this.args[1].concat()
         args1.push(game)
         var effect = uMisc.Construct(uGlobalEffects["Effect"+this.args[0]],args1)
         game.AddGlobalEffect(effect)
         break
         
      case ex.AFFECT_CHARGE_RANDOM_ABILITY:
         //args[0] - how much mana to give
         //targets[0] - player to give mana to
         //console.log('in AFFECT_CHARGE_RANDOM_ABILITY')
         var pl = targets[0]
         var abs = pl.abilities
         var actives = []
         var presentNotCharged = false
         for (var key in abs)
         {
            var ab = abs[key]
            if (!ab.passive && ab.abilityData.cost > 0)
            {
               actives.push(ab)
            }
            if (!presentNotCharged && !ab.EnoughMana())
            {
               presentNotCharged = true
            }
         }
         // console.log('actives length:',actives.length)
         // console.log('presentNotCharged:',presentNotCharged)
         ab = null
         if (actives.length > 0 && presentNotCharged)
         {
            do
            {
               ab = actives[uMisc.Random(0,actives.length)]
            } while (ab.EnoughMana())
         }
         //console.log('ab =',ab)
         if (ab)
         {
            ab.Charge(this.args[0])           
         }
         break
         
      case ex.AFFECT_CHARGE_ABILITY:
         for (var i = 0; i < targets.length; i++)
            targets[i].Charge(this.args[0])
         break
         
      case ex.AFFECT_PASS_TURN:
         game.NewTurn()
         break
      
      case ex.AFFECT_SPREAD_OWN_EFFECT:
         for (var i = 0; i < targets.length; i++)
         {
            var neighbours = game.GetNeighbours(targets[i])            
            for (var j = 0; j < neighbours.length; j++)
            {
               if (neighbours[j].HasOwningEffect())
               {
                  neighbours.splice(j,1)
                  j--
               }
            }
            if (neighbours.length > 0)
            {
               var n = 0
               if (neighbours.length > 1)
               {
                  n = game.MtRandom(0,100) % neighbours.length
               }
               for (var e = 0; e < targets[i].effects.length; e++)
               {
                  var originalEffect = targets[i].effects[e]
                  if (originalEffect.owner != null && originalEffect.owner.number === game.GetMovingPlayer().number)
                  {
                     var newEffect = originalEffect.CreateIdenticalEffect()
                     uDbg.Log("_ii","Correct owner, created effect",newEffect.type)
                     neighbours[n].AddEffect(newEffect)
                  }                        
               }         
            }               
         }
         break
      
      case ex.AFFECT_DRAIN_MANA_TO_HEALTH:
         var totalMana = 0
         for (var i = 0; i < targets.length; i++)
         {
            totalMana += this.action.ability.owner.mana[targets[i].clr]
            this.action.ability.owner.mana[targets[i].clr] = 0
         }
         this.action.ability.owner.hp += totalMana
         break
         
      case ex.AFFECT_DRAIN_MANA:
         for (var i = 0; i < targets.length; i++)
         {
            if (this.args.length === 0)
               targets[i].Drain()
            else
               targets[i].Drain(this.args[0])
         }
         break
   }
}

ex.Affect.prototype.SetArgs = function(args)
{
   this.args = args
}

ex.Affect.prototype.SetDamage = function(damage)
{
   if (this.args.length > 0)
      this.args[0] = damage
   else
      this.args.push(damage)
}

ex.Affect.prototype.SetHeal = function(heal)
{
   if (this.args.length > 1)
      this.args[1] = heal
   else if (this.args.length > 0)
      this.args[0] = heal
   else
      this.args.push(heal)
}