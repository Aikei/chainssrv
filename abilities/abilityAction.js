var uRange = require('./abilityRange.js')
var uAffect = require('./abilityAffect.js')

var ex = module.exports

ex.Action = function(pickRangeType,affectRangeType,affectType,thisAbility)
{
   this.ability = thisAbility
   this.pickRangeType = pickRangeType
   this.affectRange = new uRange.Range(affectRangeType,this)
   this.affect = new uAffect.Affect(affectType,this)
}

ex.Action.prototype.SetRangeArgs = function(args)
{
   this.affectRange.SetArgs(args)
}

ex.Action.prototype.SetAffectArgs = function(args)
{
   this.affect.SetArgs(args)
}

ex.Action.prototype.SetAffectDamage = function(damage)
{
   this.affect.SetDamage(damage)
}

ex.Action.prototype.SetAffectHeal = function(heal)
{
   this.affect.SetHeal(heal)
}