//ability.js

var uPrefs = require('../preferences')
var uDbg = require('../debug')
var uConsts = require('../constants')
var uRange = require('./abilityRange')
var uAction = require('./abilityAction')
var uAffect = require('./abilityAffect')
var uEffect = require('../abilities/effects')
var uMisc = require('../misc')
var uGlobalEffects = require('./globalEffects')

var ex = module.exports

ex.ABILITY_GREEN_RAGE = "GreenRage"
ex.ABILITY_RED_RAGE = "RedRage"
ex.ABILITY_BLUE_RAGE = "BlueRage"
ex.ABILITY_PURPLE_RAGE = "PurpleRage"
ex.ABILITY_FROST = "Frost"
ex.ABILITY_FLAME = "Flame"
ex.ABILITY_SEE_NEXT = "SeeNext"

ex.ABILITY_NONE = "None"
ex.ABILITY_FREEZE = "Freeze"
ex.ABILITY_FIRE = "Fire"
ex.ABILITY_FIRE_IMMEDIATE = "FireImmediate"
ex.ABILITY_TURN = "Turn"
ex.ABILITY_HEAL = "Heal"
ex.ABILITY_POISON = "Poison"
ex.ABILITY_BOMB = "Bomb"
ex.ABILITY_METEORITE_RAIN = "MeteoriteRain"
ex.ABILITY_STONE_OF_HEALTH = "StoneOfHealth"
ex.ABILITY_GOBLIN_MINE = "GoblinMine"
ex.ABILITY_SHATTER = "Shatter"
ex.ABILITY_EARTHQUAKE = "Earthquake"
ex.ABILITY_SHIELD_OF_ICE = "ShieldOfIce"
ex.ABILITY_DARK_MADNESS = "PurpleMadness"
ex.ABILITY_RED_MADNESS = "RedMadness"
ex.ABILITY_GREEN_MADNESS = "GreenMadness"
ex.ABILITY_BLUE_MADNESS = "BlueMadness"
ex.ABILITY_YELLOW_MADNESS = "YellowMadness"
ex.ABILITY_REMOVE_STONE = "RemoveStone"
ex.ABILITY_CHAIN_LIGHTNING = "ChainLightning"
ex.ABILITY_FREEZING_RAIN = "FreezingRain"
ex.ABILITY_MATCH_THREE = "MatchThree"
ex.ABILITY_POWER_OF_ABYSS = "PowerOfAbyss"
ex.ABILITY_ICE_BLOCK = "IceBlock"
ex.ABILITY_POWER_DRAIN = "PowerDrain"
ex.ABILITY_INFECTION = "Infection"
ex.ABILITY_WALL_OF_FIRE = "WallOfFire"
ex.ABILITY_PILLAR_OF_FIRE = "PillarOfFire"
ex.ABILITY_DESTROY_ISOLATED = "DestroyIsolated"
ex.ABILITY_FREEZE_CHAIN = "FreezeChain"
ex.ABILITY_SHATTER_ROW = "ShatterRow"
ex.ABILITY_SHATTER_COLUMN = "ShatterColumn"

//effect abilities
ex.ABILITY_FIRE_BOMB_APPLY_EFFECT = "FireBombApplyEffect"
ex.ABILITY_FIRE_BOMB = "FireBomb"

ex.ABILITY_FIRE_MINE_APPLY_EFFECT = "FireMineApplyEffect"

ex.ABILITY_FROST_NOVA_APPLY_EFFECT = "FrostNovaApplyEffect"
ex.ABILITY_FROST_NOVA = "FrostNova"

ex.ABILITY_FROST_CUBE = "FrostCube"
ex.ABILITY_SINGLE_FREEZE = "SingleFreeze"

ex.CHECK_NONE = 0
ex.CHECK_LENGTH_MINIMUM = 1
ex.CHECK_LENGTH_EQUAL = 2
ex.CHECK_LENGTH_MAXIMUM = 3
ex.CHECK_NO_OWNING_EFFECT = 4
ex.CHECK_NO_OWNING_EFFECT_EXCLUDING_HIDDEN_EFFECTS = 5
ex.CHECK_ABILITY_HAS_CHARGE = 6

var visual = {}
visual.VISUALIZE_NONE = 0
visual.VISUALIZE_SHAKE = 1
visual.VISUALIZE_CREATE_BALL_VISUAL = 2
visual.VISUALIZE_DROP_BALL_VISUAL = 3
visual.VISUALUZE_SHATTER = 4
visual.VISUALIZE_BALL_PARTICLE = 5
     
//targeting
ex.ABILITY_TARGETING_BALL = 0
ex.ABILITY_TARGETING_SELF = 1
ex.ABILITY_TARGETING_ENEMY = 2

// ex.VAR_ABILITY_OWNER = "oWn"
// ex.VAR_ABILITY_OWNERS_ENEMY = "eNe"

//pick args
ex.PICK_ARGS_NONE = 0
ex.PICK_ARGS_STONE_COLOR = 1

ex._allAbilityNames = []
ex._abilityDatas = {}
var _abilityTypes = []


ex._aiAbilityTypes = []

ex.visual = visual

ex.ACTIVATION_TYPE_ACTIVE = 0
ex.ACTIVATION_TYPE_PASSIVE_BEFORE_PICK = 1
ex.ACTIVATION_TYPE_PASSIVE_ON_YOUR_TURN = 2
ex.ACTIVATION_TYPE_PASSIVE_ON_ENEMY_TURN = 3
ex.ACTIVATION_TYPE_AFTER_PICK = 4

ex.CreateRandomAbility = function(ofColor,butNotTheeseAbs,ai,possAbs)
{
   if (!possAbs)
   {
      if (!ai)
         possAbs = _abilityTypes
      else
         possAbs = ex._aiAbilityTypes
   }
   while (1)
   {
      // if (ai)
         // var ab = new ex.Ability(ex._abilityDatas[ex._aiAbilityTypes[uMisc.Random(0,ex._aiAbilityTypes.length)]], null, ofColor)
      // else
         // ab = new ex.Ability(ex._abilityDatas[_abilityTypes[uMisc.Random(0,_abilityTypes.length)]], null, ofColor)
      var n = uMisc.Random(0,possAbs.length)
      var ab = new ex.Ability(ex._abilityDatas[possAbs[n]], null, ofColor)
      if (butNotTheeseAbs != undefined)
      {
         var found = false
         for (var key in butNotTheeseAbs)
         {
            if (butNotTheeseAbs[key].type === ab.type)
            {
               found = true
               break
            }
         }
         if (!found)
            return ab
      }
      else
      {
         return ab
      }
   }
}

ex.Ability = function(abilityDataObj,owner,clr)
{
   this.abilityData = abilityDataObj
   this.type = this.abilityData.type
   this.used = false
   this.consumed = false
   this.actions = []
   this.owner = owner
   this.clr = clr
   this.free = false
   this.actType = this.abilityData.actType || ex.ACTIVATION_TYPE_ACTIVE
   this.removal = (this.HasAffectType(uAffect.AFFECT_REMOVE_BALLS) || this.HasAffectType(uAffect.AFFECT_DESTROY_BALLS) || this.HasAffectType(uAffect.AFFECT_DESTROY_BALLS_BUT_DONT_GIVE_MANA))
   this.chargeNow = 0
   for (var i = 0; i < this.abilityData.actions.length; i++)
   {
      this.actions.push(new uAction.Action(this.abilityData.actions[i].pickRangeType,this.abilityData.actions[i].affectRangeType,this.abilityData.actions[i].affectType, this))
      this.actions[i].SetRangeArgs(this.abilityData.actions[i].rangeArgs)
      this.actions[i].SetAffectArgs(this.abilityData.actions[i].affectArgs)
   }
}

ex.Ability.prototype.Use = function(game,finalTargets,acs,srvPickTargs)
{
   uDbg.Log("Using ability",this.type)
   this.DeductMana()
   if (this.actType === ex.ACTIVATION_TYPE_ACTIVE)
      this.used = true
   if (this.abilityData.cons)
      this.consumed = true
   for (var i = 0; i < this.actions.length; i++)
   {
      if (finalTargets)
      {
         var targets = finalTargets[i]
      }
      else
      {
         srvPickTargs = srvPickTargs || []
         targets = this.actions[i].affectRange.GetTargets(game,srvPickTargs,[])
      }

      if (acs)
         var tA = acs[i].tA
      else
         tA = []
      uDbg.Log("Ability",this.type,"targets use targets:",targets)
      this.actions[i].affect.Affect(game,targets,tA)
   }
   game.SendRefreshPlayerData()
}

ex.Ability.prototype.HasAffectType = function(type)
{
   for (var i = 0; i < this.actions.length; i++)
   {
      if (this.actions[i].affect.affectType === type)
         return true
   }
   return false
}

ex.Ability.prototype.CanBeUsedWithTheseTargetsAndArguments = function(game,acs)
{
   if (!this.CanBeUsed())
   {
      uDbg.Log("_ii","CanBeUsedWithTheseTargetsAndArguments first conditional returned undefined - can't use")
      return undefined
   }
   var allServerTargets = []
   for (var i = 0; i < acs.length; i++)
   {
      var ballsSelectedByClient = acs[i].sl
      var clientTargets = acs[i].tg
      var rArgs = acs[i].rA
      var tArgs = acs[i].tA
      if (ballsSelectedByClient.length > this.abilityData.actions[i].maxSelected || ballsSelectedByClient.length < this.abilityData.actions[i].minSelected || 
            clientTargets.length < this.abilityData.actions[i].minTargets || clientTargets.length > this.abilityData.actions[i].maxTargets)
      {
         uDbg.Log("_ii","CanBeUsedWithTheseTargetsAndArguments second conditional returned undefined - wrong targets number")
         uDbg.Log("_ii","selected balls length: ",ballsSelectedByClient.length)
         uDbg.Log("_ii","max selected: ",this.abilityData.actions[i].maxSelected)
         uDbg.Log("_ii","min selected: ",this.abilityData.actions[i].minSelected)
         uDbg.Log("_ii","clientTargetsArray[i] length:",clientTargets.length)
         uDbg.Log("_ii","min targets: ",this.abilityData.actions[i].minTargets)
         uDbg.Log("_ii","max targets: ",this.abilityData.actions[i].maxTargets)
         return undefined
      }
      uDbg.Log("ability type:",this.type)
      uDbg.Log("before GetAllTargets, ballsSelectedByClientArray[i] length: ",ballsSelectedByClient.length)
      uDbg.Log("before GetAllTargets, clientTargetsArray[i] length: ",clientTargets.length)
      var serverTargets = this.actions[i].affectRange.GetTargets(game,ballsSelectedByClient,rArgs)  
      uDbg.Log("server targets: ",serverTargets)
      if (!uMisc.CheckTargetsForEquality(clientTargets,serverTargets))
      {
         uDbg.Log("_ii","CanBeUsedWithTheseTargetsAndArguments third conditional returned undefined")
         //console.log('client targets:\n',clientTargets)
         //console.log('server targets:\n',serverTargets)
         return undefined
      }
      allServerTargets.push(serverTargets)
   }
   return allServerTargets
}

ex.Ability.prototype.CanBeUsed = function()
{
   if (this.free)
      return true
   
   if (this.abilityData.chargeable && this.abilityData.charge > this.chargeNow)
   {
      uDbg.Log("_ii","Ability",this.type,"is chargeable and has not enough charge!")
      return false
   }
   
   if (this.used)
   {
      uDbg.Log("_ii","Ability",this.type,"has already been used this turn, CanBeUsed() returning false!")
      return false
   }
   
   if (this.consumed)
   {
      uDbg.Log("_ii","Ability",this.type,"has already been consumed, CanBeUsed() returning false!")
      return false      
   }
   
   if (!this.EnoughMana())
   {
      uDbg.Log("_ii","Not enough mana for ability",this.type,"returning false!")
      return false
   }
   
   return true
}

ex.Ability.prototype.CanEliminateNoMoves = function()
{
   if (this.abilityData.nmElim)
      return true
   return false
}

ex.Ability.prototype.DeductMana = function()
{
   if (this.abilityData.cost > 0)
      this.owner.mana[this.clr] = 0
   if (this.abilityData.chargeable)
      this.chargeNow = 0
}

ex.Ability.prototype.EnoughMana = function()
{
   // console.log('Enough mana:')
   // console.log('this.abilityData.cost[this.clr]:',this.abilityData.cost)
   // console.log('this.owner.mana[this.clr]',this.owner.mana[this.clr])
   if (this.abilityData.item)
      return true
   if (this.abilityData.cost > this.owner.mana[this.clr])
   {
      uDbg.Log("Not enough mana for ability",this.type,"returning false!")
      return false
   }
   return true      
}

ex.Ability.prototype.Charge = function(by)
{
   uDbg.Log("Charging ability",this.type)
   uDbg.Log("Ability clr is",this.clr)
   this.owner.mana[this.clr] += by
   if (this.owner.mana[this.clr] > this.abilityData.cost)
   {
      this.owner.mana[this.clr] = this.abilityData.cost
   }
   uDbg.Log("this.owner.mana[this.clr] = ",this.owner.mana[this.clr])
}

ex.Ability.prototype.Drain = function(by)
{
   if (by === undefined)
   {
      this.owner.mana[this.clr] = 0
   }
   else
   {
      if (this.owner.mana[this.clr] >= this.abilityData.cost)
         this.owner.mana[this.clr] = this.abilityData.cost
      this.owner.mana[this.clr] -= by
      if (this.owner.mana[this.clr] < 0)
         this.owner.mana[this.clr] = 0
   }
}

ex._abilityDatas[ex.ABILITY_FREEZE] =
{
   type : ex.ABILITY_FREEZE,
   cost : 3,
   actions : 
   [
      {                                                             //create new action
         pickRangeType : uRange.RANGE_SELECTED_STONE_ONLY,                //pick range for this action - single stone
         affectRangeType : uRange.RANGE_SELECTED_STONE_WITH_NEIGHBOURS,    //range for this action - selected stone and its neighbours
         showRangeType : uRange.RANGE_SELECTED_STONE_WITH_NEIGHBOURS,
         affectType : uAffect.AFFECT_APPLY_EFFECT,                         //affect apply effect
         rangeArgs : [],                                                //range args. empty this time.
         affectArgs : 
         [                                                 //affect args, specifying how affect should behave
            uEffect.EFFECT_FROZEN,                            //args[0] - what effect to apply to the targets
            [ 2 ],                                               //args[1] - effect args
         ],
         visualType : visual.VISUALIZE_BALL_PARTICLE,
         visualArgs : [ "blueM", 0.5 ],         
         maxSelected : 1,
         minSelected : 1,
         minTargets : 4,
         maxTargets : 9
      }
   ]
}
_abilityTypes.push(ex.ABILITY_FREEZE)

ex._abilityDatas[ex.ABILITY_FREEZE_CHAIN] =
{
   type : ex.ABILITY_FREEZE_CHAIN,
   cost : 4,
   actions : 
   [
      {                                                             //create new action
         pickRangeType : uRange.RANGE_SELECTED_STONE_ONLY,                //pick range for this action - single stone
         affectRangeType : uRange.RANGE_STONES_IN_SELECTED_CHAIN,    //range for this action - selected stone and its neighbours
         showRangeType : uRange.RANGE_STONES_IN_SELECTED_CHAIN,
         affectType : uAffect.AFFECT_APPLY_EFFECT,                         //affect apply effect
         rangeArgs : [],                                                //range args. empty this time.
         affectArgs : 
         [                                                 //affect args, specifying how affect should behave
            uEffect.EFFECT_FROZEN,                            //args[0] - what effect to apply to the targets
            [ 2 ],                                               //args[1] - effect args
         ],
         visualType : visual.VISUALIZE_BALL_PARTICLE,
         visualArgs : [ "blueM", 0.5 ],         
         maxSelected : 1,
         minSelected : 1,
         minTargets : 1,
         maxTargets : 36
      }
   ]
}
_abilityTypes.push(ex.ABILITY_FREEZE_CHAIN)

ex._abilityDatas[ex.ABILITY_FROST_NOVA] =
{
   type : ex.ABILITY_FROST_NOVA,
   cost : 3,
   actions : 
   [
      {                                                             //create new action
         pickRangeType : uRange.RANGE_SELECTED_STONE_ONLY,                //pick range for this action - single stone
         affectRangeType : uRange.RANGE_SELECTED_STONE_WITH_NEIGHBOURS,    //range for this action - selected stone and its neighbours
         showRangeType : uRange.RANGE_SELECTED_STONE_WITH_NEIGHBOURS,
         affectType : uAffect.AFFECT_APPLY_EFFECT,                         //affect apply effect
         rangeArgs : [ "no" ],                                                //range args. empty this time.
         affectArgs : 
         [                                                 //affect args, specifying how affect should behave
            uEffect.EFFECT_FROZEN,                            //args[0] - what effect to apply to the targets
            [ 1 ],                                               //args[1] - effect args
         ],
         visualType : visual.VISUALIZE_BALL_PARTICLE,
         visualArgs : [ "blueM", 0 ],         
         maxSelected : 1,
         minSelected : 1,
         minTargets : 3,
         maxTargets : 8
      }
   ]
}

ex._abilityDatas[ex.ABILITY_FROST_NOVA_APPLY_EFFECT] =
{
   type : ex.ABILITY_FROST_NOVA_APPLY_EFFECT,
   cost : 3,
   actions : 
   [
      {                                                             //create new action
         pickRangeType : uRange.RANGE_SELECTED_STONE_ONLY,                //pick range for this action - single stone
         affectRangeType : uRange.RANGE_SELECTED_STONE_ONLY,    //range for this action - selected stone and its neighbours
         showRangeType : uRange.RANGE_SELECTED_STONE_ONLY,
         affectType : uAffect.AFFECT_APPLY_EFFECT,                         //affect apply effect
         rangeArgs : [],                                                //range args. empty this time.
         affectArgs : 
         [                                                 //affect args, specifying how affect should behave
            uEffect.EFFECT_FROST_NOVA,                            //args[0] - what effect to apply to the targets
            [ ],                                               //args[1] - effect args
         ],
         visualType : visual.VISUALIZE_BALL_PARTICLE,
         visualArgs : [ "blueM", 0.5 ],         
         maxSelected : 1,
         minSelected : 1,
         minTargets : 1,
         maxTargets : 1
      }
   ]
}
_abilityTypes.push(ex.ABILITY_FROST_NOVA_APPLY_EFFECT)

ex._abilityDatas[ex.ABILITY_SINGLE_FREEZE] =
{
   type : ex.ABILITY_SINGLE_FREEZE,
   cost : 1,
   actions : 
   [
      {                                                             //create new action
         pickRangeType : uRange.RANGE_SELECTED_STONE_ONLY,                //pick range for this action - single stone
         affectRangeType : uRange.RANGE_SELECTED_STONE_ONLY,    //range for this action - selected stone and its neighbours
         showRangeType : uRange.RANGE_SELECTED_STONE_ONLY,
         affectType : uAffect.AFFECT_APPLY_EFFECT,                         //affect apply effect
         rangeArgs : [],                                                //range args. empty this time.
         affectArgs : 
         [                                                 //affect args, specifying how affect should behave
            uEffect.EFFECT_FROZEN,                            //args[0] - what effect to apply to the targets
            [ 1 ],                                               //args[1] - effect args
         ],
         visualType : visual.VISUALIZE_BALL_PARTICLE,
         visualArgs : [ "blueM", 0 ],         
         maxSelected : 1,
         minSelected : 1,
         minTargets : 1,
         maxTargets : 1
      }
   ]
}

ex._abilityDatas[ex.ABILITY_FROST_CUBE] =
{
   type : ex.ABILITY_FROST_CUBE,
   cost : 4,
   actions : 
   [
      {                                                             //create new action
         pickRangeType : uRange.RANGE_SELECTED_STONE_ONLY,                //pick range for this action - single stone
         affectRangeType : uRange.RANGE_SELECTED_STONE_WITH_NEIGHBOURS,    //range for this action - selected stone and its neighbours
         showRangeType : uRange.RANGE_SELECTED_STONE_WITH_NEIGHBOURS,
         affectType : uAffect.AFFECT_APPLY_EFFECT,                         //affect apply effect
         rangeArgs : [ ],                                                //range args. empty this time.
         affectArgs : 
         [                                                 //affect args, specifying how affect should behave
            uEffect.EFFECT_FROST,                            //args[0] - what effect to apply to the targets
            [ ],                                               //args[1] - effect args
         ],
         visualType : visual.VISUALIZE_BALL_PARTICLE,
         visualArgs : [ "blueM", 0.5 ],         
         maxSelected : 1,
         minSelected : 1,
         minTargets : 4,
         maxTargets : 9
      }
   ]
}
_abilityTypes.push(ex.ABILITY_FROST_CUBE)

ex._abilityDatas[ex.ABILITY_POISON] =
{
   type : ex.ABILITY_POISON,
   cost : 3,
   actions : 
   [
      {                                                             //create new action
         pickRangeType : uRange.RANGE_SELECTED_STONE_ONLY,                //pick range for this action - single stone
         affectRangeType : uRange.RANGE_SELECTED_STONE_ONLY,    //range for this action - selected stone and its neighbours
         showRangeType : uRange.RANGE_SELECTED_STONE_ONLY,
         affectType : uAffect.AFFECT_APPLY_EFFECT,                         //affect apply effect
         sChk : ex.CHECK_NO_OWNING_EFFECT_EXCLUDING_HIDDEN_EFFECTS,
         rangeArgs : [],                                                //range args. empty this time.
         affectArgs : 
         [                                                 //affect args, specifying how affect should behave
            uEffect.EFFECT_POISON,                            //args[0] - what effect to apply to the targets
            [ 2 ],                                               //args[1] - effect args - damage per turn
         ],
         visualType : visual.VISUALIZE_BALL_PARTICLE,
         visualArgs : [ "greenM", 0.5 ],             
         maxSelected : 1,
         minSelected : 1,
         minTargets : 1,
         maxTargets : 1
      }
   ]
}
_abilityTypes.push(ex.ABILITY_POISON)

ex._abilityDatas[ex.ABILITY_STONE_OF_HEALTH] =
{
   type : ex.ABILITY_STONE_OF_HEALTH,
   cost : 3,
   actions : 
   [
      {                                                             //create new action
         pickRangeType : uRange.RANGE_SELECTED_STONE_ONLY,                //pick range for this action - single stone
         affectRangeType : uRange.RANGE_SELECTED_STONE_ONLY,    //range for this action - selected stone and its neighbours
         showRangeType : uRange.RANGE_SELECTED_STONE_ONLY,
         affectType : uAffect.AFFECT_APPLY_EFFECT,                         //affect apply effect
         sChk : ex.CHECK_NO_OWNING_EFFECT_EXCLUDING_HIDDEN_EFFECTS,
         rangeArgs : [],                                                //range args. empty this time.
         affectArgs : 
         [                                                 //affect args, specifying how affect should behave
            uEffect.EFFECT_STONE_OF_HEALTH,                            //args[0] - what effect to apply to the targets
            [ 2 ],                                               //args[1] - effect args - damage per turn
         ],
         visualType : visual.VISUALIZE_BALL_PARTICLE,
         visualArgs : [ "greenM", 0.5 ],             
         maxSelected : 1,
         minSelected : 1,
         minTargets : 1,
         maxTargets : 1
      }
   ]
}
_abilityTypes.push(ex.ABILITY_STONE_OF_HEALTH)

ex._abilityDatas[ex.ABILITY_BOMB] =
{
   type : ex.ABILITY_BOMB,
   cost : 4,
   actions : 
   [
      {                                                             //create new action
         pickRangeType : uRange.RANGE_SELECTED_STONE_ONLY,                //pick range for this action - single stone
         affectRangeType : uRange.RANGE_SELECTED_STONE_ONLY,    //range for this action - selected stone and its neighbours
         showRangeType : uRange.RANGE_SELECTED_STONE_ONLY,
         affectType : uAffect.AFFECT_APPLY_EFFECT,                         //affect apply effect
         sChk : ex.CHECK_NO_OWNING_EFFECT_EXCLUDING_HIDDEN_EFFECTS,
         rangeArgs : [],                                                //range args. empty this time.
         affectArgs : 
         [                                                 //affect args, specifying how affect should behave
            uEffect.EFFECT_BOMB,                            //args[0] - what effect to apply to the targets
            [ 10, 5 ],                                               //args[1] - effect args: 1) explosion damage, 2) time to explode (turns)
         ],
         maxSelected : 1,
         minSelected : 1,
         minTargets : 1,
         maxTargets : 1
      }
   ]
}
_abilityTypes.push(ex.ABILITY_BOMB)

ex._abilityDatas[ex.ABILITY_FIRE_BOMB_APPLY_EFFECT] =
{
   type : ex.ABILITY_FIRE_BOMB_APPLY_EFFECT,
   cost : 8,
   actions : 
   [
      {                                                             //create new action
         pickRangeType : uRange.RANGE_SELECTED_STONE_ONLY,                //pick range for this action - single stone
         affectRangeType : uRange.RANGE_SELECTED_STONE_ONLY,    //range for this action - selected stone and its neighbours
         showRangeType : uRange.RANGE_SELECTED_STONE_ONLY,
         affectType : uAffect.AFFECT_APPLY_EFFECT,                         //affect apply effect
         sChk : ex.CHECK_NO_OWNING_EFFECT_EXCLUDING_HIDDEN_EFFECTS,
         rangeArgs : [],                                                //range args. empty this time.
         affectArgs : 
         [                                                 //affect args, specifying how affect should behave
            uEffect.EFFECT_FIRE_BOMB,                            //args[0] - what effect to apply to the targets
            [ ],                                               //args[1] - effect args: 1) explosion damage, 2) time to explode (turns)
         ],
         maxSelected : 1,
         minSelected : 1,
         minTargets : 1,
         maxTargets : 1
      }
   ]
}
_abilityTypes.push(ex.ABILITY_FIRE_BOMB_APPLY_EFFECT)

ex._abilityDatas[ex.ABILITY_FIRE_MINE_APPLY_EFFECT] =
{
   type : ex.ABILITY_FIRE_MINE_APPLY_EFFECT,
   cost : 5,
   actions : 
   [
      {                                                             //create new action
         pickRangeType : uRange.RANGE_SELECTED_STONE_ONLY,                //pick range for this action - single stone
         affectRangeType : uRange.RANGE_SELECTED_STONE_ONLY,    //range for this action - selected stone and its neighbours
         showRangeType : uRange.RANGE_SELECTED_STONE_ONLY,
         affectType : uAffect.AFFECT_APPLY_EFFECT,                         //affect apply effect
         sChk : ex.CHECK_NO_OWNING_EFFECT_EXCLUDING_HIDDEN_EFFECTS,
         rangeArgs : [],                                                //range args. empty this time.
         affectArgs : 
         [                                                 //affect args, specifying how affect should behave
            uEffect.EFFECT_FIRE_MINE,                            //args[0] - what effect to apply to the targets
            [ ],                                               //args[1] - effect args: 1) explosion damage, 2) time to explode (turns)
         ],
         maxSelected : 1,
         minSelected : 1,
         minTargets : 1,
         maxTargets : 1
      }
   ]
}
_abilityTypes.push(ex.ABILITY_FIRE_MINE_APPLY_EFFECT)

ex._abilityDatas[ex.ABILITY_BOMB] =
{
   type : ex.ABILITY_BOMB,
   cost : 4,
   actions : 
   [
      {                                                             //create new action
         pickRangeType : uRange.RANGE_SELECTED_STONE_ONLY,                //pick range for this action - single stone
         affectRangeType : uRange.RANGE_SELECTED_STONE_ONLY,    //range for this action - selected stone and its neighbours
         showRangeType : uRange.RANGE_SELECTED_STONE_ONLY,
         affectType : uAffect.AFFECT_APPLY_EFFECT,                         //affect apply effect
         sChk : ex.CHECK_NO_OWNING_EFFECT_EXCLUDING_HIDDEN_EFFECTS,
         rangeArgs : [],                                                //range args. empty this time.
         affectArgs : 
         [                                                 //affect args, specifying how affect should behave
            uEffect.EFFECT_BOMB,                            //args[0] - what effect to apply to the targets
            [ 10, 5 ],                                               //args[1] - effect args: 1) explosion damage, 2) time to explode (turns)
         ],
         maxSelected : 1,
         minSelected : 1,
         minTargets : 1,
         maxTargets : 1
      }
   ]
}
_abilityTypes.push(ex.ABILITY_BOMB)

ex._abilityDatas[ex.ABILITY_GOBLIN_MINE] =
{
   type : ex.ABILITY_GOBLIN_MINE,
   cost : 4,
   hiddenSelect : true,
   actions : 
   [
      {                                                             //create new action
         pickRangeType : uRange.RANGE_SELECTED_STONE_ONLY,                //pick range for this action - single stone
         affectRangeType : uRange.RANGE_SELECTED_STONE_ONLY,    //range for this action - selected stone and its neighbours
         showRangeType : uRange.RANGE_SELECTED_STONE_ONLY,
         affectType : uAffect.AFFECT_APPLY_EFFECT,                         //affect apply effect
         sChk : ex.CHECK_NO_OWNING_EFFECT,
         rangeArgs : [],                                                //range args. empty this time.
         affectArgs : 
         [                                                 //affect args, specifying how affect should behave
            uEffect.EFFECT_GOBLIN_MINE,                            //args[0] - what effect to apply to the targets
            [ 10 ],                                               //args[1] - effect args: 1) mine damage
         ],
         hiddenSelect: true,                                      //enemy can't see selection and effect
         maxSelected : 1,
         minSelected : 1,
         minTargets : 1,
         maxTargets : 1
      }
   ]
}
_abilityTypes.push(ex.ABILITY_GOBLIN_MINE)

ex._abilityDatas[ex.ABILITY_FIRE] =
{
   type : ex.ABILITY_FIRE,
   cost : 6,
   nmElim : true, //can negate no moves
   actions : 
   [
      {                                                                      //create a new action,
         pickRangeType : uRange.RANGE_SELECTED_STONE_ONLY,                         //pick range for this action - single stone,
         affectRangeType : uRange.RANGE_SELECTED_STONE_WITH_CROSS_NEIGHBOURS,       //range for this action - selected stone and its cross neighbours,
         showRangeType: uRange.RANGE_SELECTED_STONE_WITH_CROSS_NEIGHBOURS,
         affectType : uAffect.AFFECT_DESTROY_BALLS,                                //affect - destroy balls,
         visualType : visual.VISUALIZE_BALL_PARTICLE,
         visualArgs : [ "fire", 0.6, 0.25 ],
         rangeArgs : [],                                                            //range args. empty this time.,
         affectArgs : [],                                                           //affect args, specifying how affect should behave. empty,
         maxSelected : 1,
         minSelected : 1,
         minTargets : 3,
         maxTargets : 5
      }
   ]
}
_abilityTypes.push(ex.ABILITY_FIRE)

ex._abilityDatas[ex.ABILITY_FIRE_IMMEDIATE] =
{
   type : ex.ABILITY_FIRE_IMMEDIATE,
   cost : 6,
   nmElim : true, //can negate no moves
   actions : 
   [
      {                                                                      //create a new action,
         pickRangeType : uRange.RANGE_SELECTED_STONE_ONLY,                         //pick range for this action - single stone,
         affectRangeType : uRange.RANGE_SELECTED_STONE_WITH_CROSS_NEIGHBOURS,       //range for this action - selected stone and its cross neighbours,
         showRangeType: uRange.RANGE_SELECTED_STONE_WITH_CROSS_NEIGHBOURS,
         affectType : uAffect.AFFECT_DESTROY_BALLS,                                //affect - destroy balls,
         visualType : visual.VISUALIZE_BALL_PARTICLE,
         visualArgs : [ "fire", 0.6, 0 ],
         rangeArgs : [],                                                            //range args. empty this time.,
         affectArgs : [],                                                           //affect args, specifying how affect should behave. empty,
         maxSelected : 1,
         minSelected : 1,
         minTargets : 3,
         maxTargets : 5
      }
   ]
}

ex._abilityDatas[ex.ABILITY_WALL_OF_FIRE] =
{
   type : ex.ABILITY_WALL_OF_FIRE,
   cost : 6,
   nmElim : true, //can negate no moves
   actions : 
   [
      {                                                                      //create a new action,
         pickRangeType : uRange.RANGE_SELECTED_STONE_ONLY,                         //pick range for this action - single stone,
         affectRangeType : uRange.RANGE_SELECTED_STONE_ROW_AND_COLUMN,       //range for this action - selected stone and its cross neighbours,
         showRangeType: uRange.RANGE_SELECTED_STONE_ROW_AND_COLUMN,
         affectType : uAffect.AFFECT_DESTROY_BALLS,                                //affect - destroy balls,
         visualType : visual.VISUALIZE_BALL_PARTICLE,
         visualArgs : [ "fire", 0.6, 0.25 ],
         rangeArgs : [ "r" ],                                                            //range args. empty this time.,
         affectArgs : [],                                                           //affect args, specifying how affect should behave. empty,
         maxSelected : 1,
         minSelected : 1,
         minTargets : 6,
         maxTargets : 6
      }
   ]
}
_abilityTypes.push(ex.ABILITY_WALL_OF_FIRE)

ex._abilityDatas[ex.ABILITY_PILLAR_OF_FIRE] =
{
   type : ex.ABILITY_PILLAR_OF_FIRE,
   cost : 6,
   nmElim : true, //can negate no moves
   actions : 
   [
      {                                                                      //create a new action,
         pickRangeType : uRange.RANGE_SELECTED_STONE_ONLY,                         //pick range for this action - single stone,
         affectRangeType : uRange.RANGE_SELECTED_STONE_ROW_AND_COLUMN,       //range for this action - selected stone and its cross neighbours,
         showRangeType: uRange.RANGE_SELECTED_STONE_ROW_AND_COLUMN,
         affectType : uAffect.AFFECT_DESTROY_BALLS,                                //affect - destroy balls,
         visualType : visual.VISUALIZE_BALL_PARTICLE,
         visualArgs : [ "fire", 0.6, 0.25 ],
         rangeArgs : [ "c" ],                                                            //range args. empty this time.,
         affectArgs : [],                                                           //affect args, specifying how affect should behave. empty,
         maxSelected : 1,
         minSelected : 1,
         minTargets : 6,
         maxTargets : 6
      }
   ]
}
_abilityTypes.push(ex.ABILITY_PILLAR_OF_FIRE)

ex._abilityDatas[ex.ABILITY_FIRE_BOMB] =
{
   type : ex.ABILITY_FIRE_BOMB,
   cost : 0,
   nmElim : true, //can negate no moves
   actions : 
   [
      {                                                                      //create a new action,
         pickRangeType : uRange.RANGE_SELECTED_STONE_ONLY,                         //pick range for this action - single stone,
         affectRangeType : uRange.RANGE_SELECTED_STONE_WITH_NEIGHBOURS,       //range for this action - selected stone and its cross neighbours,
         showRangeType: uRange.RANGE_SELECTED_STONE_WITH_NEIGHBOURS,
         affectType : uAffect.AFFECT_DESTROY_BALLS,                                //affect - destroy balls,
         visualType : visual.VISUALIZE_BALL_PARTICLE,
         visualArgs : [ "fire", 0.6, 0 ],
         rangeArgs : [],                                                            //range args. empty this time.,
         affectArgs : [],                                                           //affect args, specifying how affect should behave. empty,
         maxSelected : 1,
         minSelected : 1,
         minTargets : 4,
         maxTargets : 9
      }
   ]
}

ex._abilityDatas[ex.ABILITY_RING_OF_FIRE] =
{
   type : ex.ABILITY_RING_OF_FIRE,
   cost : 0,
   nmElim : true, //can negate no moves
   actions : 
   [
      {                                                                      //create a new action,
         pickRangeType : uRange.RANGE_SELECTED_STONE_ONLY,                         //pick range for this action - single stone,
         affectRangeType : uRange.RANGE_SELECTED_STONE_WITH_NEIGHBOURS,       //range for this action - selected stone and its cross neighbours,
         showRangeType: uRange.RANGE_SELECTED_STONE_WITH_NEIGHBOURS,
         affectType : uAffect.AFFECT_DESTROY_BALLS,                                //affect - destroy balls,
         visualType : visual.VISUALIZE_BALL_PARTICLE,
         visualArgs : [ "fire", 0.6, 0.25 ],
         rangeArgs : [ "no" ],                                                            //range args. empty this time.,
         affectArgs : [],                                                           //affect args, specifying how affect should behave. empty,
         maxSelected : 1,
         minSelected : 1,
         minTargets : 8,
         maxTargets : 8
      }
   ]
}

ex._abilityDatas[ex.ABILITY_METEORITE_RAIN] =
{
   type : ex.ABILITY_METEORITE_RAIN,
   cost : 8,
   nmElim : true, //can negate no moves
   actions : 
   [
      {                                                                      //create a new action,
         pickRangeType : uRange.RANGE_NONE,                         //pick range for this action - single stone,
         affectRangeType : uRange.RANGE_RANDOM_STONES,               //range for this action - selected stone and its cross neighbours,
         showRangeType: uRange.RANGE_NONE,
         affectType : uAffect.AFFECT_DESTROY_BALLS,                                //affect - destroy balls,
         visualType : visual.VISUALIZE_DROP_BALL_VISUAL,
         visualArgs : [ "fireball", 1, "rot" ],
         rangeArgs : [ 10 ],                                                            //range args. empty this time.,
         affectArgs : [ ],                                                           //affect args, specifying how affect should behave. empty,
         maxSelected : 0,
         minSelected : 0,
         minTargets : 10,
         maxTargets : 10
      }
   ]
}
_abilityTypes.push(ex.ABILITY_METEORITE_RAIN)

ex._abilityDatas[ex.ABILITY_MATCH_THREE] =
{
   type : ex.ABILITY_MATCH_THREE,
   cost : 4,
   nmElim : true, //can negate no moves
   actions : 
   [
      {                                                                                                  //create a new action,
         pickRangeType : uRange.RANGE_NONE,                                                              //pick range for this action - none, target automatic
         affectRangeType : uRange.RANGE_ALL_STONES_IN_ROWS_AND_COLUMNS_OF_THE_SAME_COLOR,                //range for this action - balls of the same color in the same vertical or horizontal lines
         showRangeType: uRange.RANGE_NONE,
         affectType : uAffect.AFFECT_DESTROY_BALLS,                                                      //affect - destroy balls,
         rangeArgs : [ 3 ],                                                                              //range args: number of balls in a line to match
         affectArgs : [ ],                                                                               //affect args: empty
         maxSelected : 0,
         minSelected : 0,
         minTargets : 0,
         maxTargets : 36
      }
   ]
}
_abilityTypes.push(ex.ABILITY_MATCH_THREE)

ex._abilityDatas[ex.ABILITY_DESTROY_ISOLATED] =
{
   type : ex.ABILITY_DESTROY_ISOLATED,
   cost : 8,
   nmElim : true, //can negate no moves
   actions : 
   [
      {                                                                                                  //create a new action,
         pickRangeType : uRange.RANGE_NONE,                                                              //pick range for this action - none, target automatic
         affectRangeType : uRange.RANGE_ALL_GEMS_IN_CHAINS_OF_SPECIFIC_LENGTH,                //range for this action - balls of the same color in the same vertical or horizontal lines
         showRangeType: uRange.RANGE_NONE,
         affectType : uAffect.AFFECT_DESTROY_BALLS,                                                      //affect - destroy balls,
         rangeArgs : [ 1, 1 ],                                                                              //range args: number of balls in a line to match
         affectArgs : [ ],                                                                               //affect args: empty
         maxSelected : 0,
         minSelected : 0,
         minTargets : 0,
         maxTargets : 36
      }
   ]
}
_abilityTypes.push(ex.ABILITY_DESTROY_ISOLATED)

ex._abilityDatas[ex.ABILITY_CHAIN_LIGHTNING] =
{
   type : ex.ABILITY_CHAIN_LIGHTNING,
   cost : 4,
   nmElim : true, //can negate no moves
   actions : 
   [
      {                                                                      //create a new action,
         pickRangeType : uRange.RANGE_SELECTED_STONE_ONLY,                         //pick range for this action - single stone,
         affectRangeType : uRange.RANGE_RANDOM_STONES,               //range for this action - selected stone and its cross neighbours,
         showRangeType: uRange.RANGE_SELECTED_STONE_ONLY,
         affectType : uAffect.AFFECT_DESTROY_BALLS,                                //affect - destroy balls,
         rangeArgs : [ 3 ],                                                            //range args. empty this time.,
         affectArgs : [ ],                                                           //affect args, specifying how affect should behave. empty,
         maxSelected : 1,
         minSelected : 1,
         minTargets : 4,
         maxTargets : 4
      }
   ]
}
_abilityTypes.push(ex.ABILITY_CHAIN_LIGHTNING)

ex._abilityDatas[ex.ABILITY_DARK_MADNESS] =
{
   type : ex.ABILITY_DARK_MADNESS,
   cost : 8,
   nmElim : true, //can negate no moves
   actions : 
   [
      {                                                                      //create a new action,
         pickRangeType : uRange.RANGE_NONE,                         //pick range for this action - single stone,
         affectRangeType : uRange.RANGE_ALL_STONES_OF_SPECIFIC_COLOR,               //range for this action - selected stone and its cross neighbours,
         showRangeType: uRange.RANGE_NONE,
         affectType : uAffect.AFFECT_DESTROY_BALLS,                                //affect - destroy balls,
         rangeArgs : [ uConsts.BALL_PURPLE ],                                                            //range args. empty this time.,
         affectArgs : [ ],                                                           //affect args, specifying how affect should behave. empty,
         maxSelected : 0,
         minSelected : 0,
         minTargets : 0,
         maxTargets : 36
      }
   ]
}
//_abilityTypes.push(ex.ABILITY_DARK_MADNESS)

ex._abilityDatas[ex.ABILITY_RED_MADNESS] =
{
   type : ex.ABILITY_RED_MADNESS,
   cost : 8,
   nmElim : true, //can negate no moves
   actions : 
   [
      {                                                                      //create a new action,
         pickRangeType : uRange.RANGE_NONE,                         //pick range for this action - single stone,
         affectRangeType : uRange.RANGE_ALL_STONES_OF_SPECIFIC_COLOR,               //range for this action - selected stone and its cross neighbours,
         showRangeType: uRange.RANGE_NONE,
         affectType : uAffect.AFFECT_DESTROY_BALLS,                                //affect - destroy balls,
         rangeArgs : [ uConsts.BALL_RED ],                                                            //range args. empty this time.,
         affectArgs : [ ],                                                           //affect args, specifying how affect should behave. empty,
         maxSelected : 0,
         minSelected : 0,
         minTargets : 0,
         maxTargets : 36
      }
   ]
}
//_abilityTypes.push(ex.ABILITY_RED_MADNESS)

ex._abilityDatas[ex.ABILITY_GREEN_MADNESS] =
{
   type : ex.ABILITY_GREEN_MADNESS,
   cost : 8,
   nmElim : true, //can negate no moves
   actions : 
   [
      {                                                                      //create a new action,
         pickRangeType : uRange.RANGE_NONE,                         //pick range for this action - single stone,
         affectRangeType : uRange.RANGE_ALL_STONES_OF_SPECIFIC_COLOR,               //range for this action - selected stone and its cross neighbours,
         showRangeType: uRange.RANGE_NONE,
         affectType : uAffect.AFFECT_DESTROY_BALLS,                                //affect - destroy balls,
         rangeArgs : [ uConsts.BALL_GREEN ],                                                            //range args. empty this time.,
         affectArgs : [ ],                                                           //affect args, specifying how affect should behave. empty,
         maxSelected : 0,
         minSelected : 0,
         minTargets : 0,
         maxTargets : 36
      }
   ]
}
//_abilityTypes.push(ex.ABILITY_GREEN_MADNESS)

ex._abilityDatas[ex.ABILITY_BLUE_MADNESS] =
{
   type : ex.ABILITY_BLUE_MADNESS,
   cost : 8,
   nmElim : true, //can negate no moves
   actions : 
   [
      {                                                                      //create a new action,
         pickRangeType : uRange.RANGE_NONE,                         //pick range for this action - single stone,
         affectRangeType : uRange.RANGE_ALL_STONES_OF_SPECIFIC_COLOR,               //range for this action - selected stone and its cross neighbours,
         showRangeType: uRange.RANGE_NONE,
         affectType : uAffect.AFFECT_DESTROY_BALLS,                                //affect - destroy balls,
         rangeArgs : [ uConsts.BALL_BLUE ],                                                            //range args. empty this time.,
         affectArgs : [ ],                                                           //affect args, specifying how affect should behave. empty,
         maxSelected : 0,
         minSelected : 0,
         minTargets : 0,
         maxTargets : 36
      }
   ]
}
//_abilityTypes.push(ex.ABILITY_BLUE_MADNESS)

ex._abilityDatas[ex.ABILITY_YELLOW_MADNESS] =
{
   type : ex.ABILITY_YELLOW_MADNESS,
   cost : 8,
   nmElim : true, //can negate no moves
   actions : 
   [
      {                                                                      //create a new action,
         pickRangeType : uRange.RANGE_NONE,                         //pick range for this action - single stone,
         affectRangeType : uRange.RANGE_ALL_STONES_OF_SPECIFIC_COLOR,               //range for this action - selected stone and its cross neighbours,
         showRangeType: uRange.RANGE_NONE,
         affectType : uAffect.AFFECT_DESTROY_BALLS,                                //affect - destroy balls,
         rangeArgs : [ uConsts.BALL_YELLOW ],                                                            //range args. empty this time.,
         affectArgs : [ ],                                                           //affect args, specifying how affect should behave. empty,
         maxSelected : 0,
         minSelected : 0,
         minTargets : 0,
         maxTargets : 36
      }
   ]
}
_abilityTypes.push(ex.ABILITY_YELLOW_MADNESS)

ex._abilityDatas[ex.ABILITY_TURN] =
{
   type : ex.ABILITY_TURN,
   cost : 4,
   nmElim : true, //can negate no moves
   actions : 
   [
      {                                                                             //create a new action,
         pickRangeType : uRange.RANGE_SELECTED_STONE_ONLY,                          //pick range for this action - single stone,
         affectRangeType : uRange.RANGE_SELECTED_STONE_ONLY,                        //range for this action - selected stone only,
         showRangeType : uRange.RANGE_SELECTED_STONE_ONLY,
         affectType : uAffect.AFFECT_TURN_BALL_COLOR,                               //affect - change ball color,
         visualType : visual.VISUALIZE_BALL_PARTICLE,
         visualArgs : [ "purpleM", 0.5 ],            
         rangeArgs : [],                                                            //range args. empty this time.,
         affectArgs : [],                                                           //affect args, specifying how affect should behave. empty,
         maxSelected : 1,
         minSelected : 1,
         minTargets : 1,
         maxTargets : 1,
         pickArgs : ex.PICK_ARGS_STONE_COLOR
      }
   ]
}
_abilityTypes.push(ex.ABILITY_TURN)

ex._abilityDatas[ex.ABILITY_HEAL] =
{
   type : ex.ABILITY_HEAL,
   cost : 6,
   actions : 
   [
      {                                                                      //create a new action,
         pickRangeType : uRange.RANGE_NONE,                                         //pick range for this action - automatic, no need to pick a target,
         affectRangeType : uRange.RANGE_YOUR_HERO,                                  //range for this action - your hero,
         showRangeType : uRange.RANGE_NONE, 
         affectType : uAffect.AFFECT_HEAL,                                          //affect - heal,
         rangeArgs : [],                                                            //range args. empty this time.,
         affectArgs : [ 7 ],                                                           //affect args, specifying how affect should behave. empty              
         maxSelected : 0,
         minSelected : 0,
         minTargets : 1,
         maxTargets : 1
      }
   ]
}
_abilityTypes.push(ex.ABILITY_HEAL)

ex._abilityDatas[ex.ABILITY_SHATTER] =
{
   type : ex.ABILITY_SHATTER,
   cost : 4,
   nmElim : true, //can negate no moves
   actions : 
   [
      {                                                                      //create a new action,
         pickRangeType : uRange.RANGE_SELECTED_STONE_ONLY,                         //pick range for this action - single stone
         affectRangeType : uRange.RANGE_SELECTED_STONE_ROW_AND_COLUMN,       //range for this action - selected stone and its row and column
         showRangeType: uRange.RANGE_SELECTED_STONE_ROW_AND_COLUMN,           //show range for this action - selected stone and its row and column
         affectType : uAffect.AFFECT_REMOVE_BALLS,                                //affect - remove balls
         rangeArgs : [],                                                            //range args. empty this time.
         affectArgs : [],                                                           //affect args, specifying how affect should behave. empty
         visualType : visual.VISUALUZE_SHATTER,
         maxSelected : 1,
         minSelected : 1,
         minTargets : 11,
         maxTargets : 11
      }
   ]
}
_abilityTypes.push(ex.ABILITY_SHATTER)

ex._abilityDatas[ex.ABILITY_SHATTER_ROW] =
{
   type : ex.ABILITY_SHATTER_ROW,
   cost : 3,
   nmElim : true, //can negate no moves
   actions : 
   [
      {                                                                      //create a new action,
         pickRangeType : uRange.RANGE_SELECTED_STONE_ONLY,                         //pick range for this action - single stone
         affectRangeType : uRange.RANGE_SELECTED_STONE_ROW_AND_COLUMN,       //range for this action - selected stone and its row and column
         showRangeType: uRange.RANGE_SELECTED_STONE_ROW_AND_COLUMN,           //show range for this action - selected stone and its row and column
         affectType : uAffect.AFFECT_REMOVE_BALLS,                                //affect - remove balls
         rangeArgs : [ "r" ],                                                            //range args. empty this time.
         affectArgs : [],                                                           //affect args, specifying how affect should behave. empty
         visualType : visual.VISUALUZE_SHATTER,
         maxSelected : 1,
         minSelected : 1,
         minTargets : 6,
         maxTargets : 6
      }
   ]
}
_abilityTypes.push(ex.ABILITY_SHATTER_ROW)

ex._abilityDatas[ex.ABILITY_SHATTER_COLUMN] =
{
   type : ex.ABILITY_SHATTER_COLUMN,
   cost : 3,
   nmElim : true, //can negate no moves
   actions : 
   [
      {                                                                      //create a new action,
         pickRangeType : uRange.RANGE_SELECTED_STONE_ONLY,                         //pick range for this action - single stone
         affectRangeType : uRange.RANGE_SELECTED_STONE_ROW_AND_COLUMN,       //range for this action - selected stone and its row and column
         showRangeType: uRange.RANGE_SELECTED_STONE_ROW_AND_COLUMN,           //show range for this action - selected stone and its row and column
         affectType : uAffect.AFFECT_REMOVE_BALLS,                                //affect - remove balls
         rangeArgs : [ "c" ],                                                            //range args. empty this time.
         affectArgs : [],                                                           //affect args, specifying how affect should behave. empty
         visualType : visual.VISUALUZE_SHATTER,
         maxSelected : 1,
         minSelected : 1,
         minTargets : 6,
         maxTargets : 6
      }
   ]
}
_abilityTypes.push(ex.ABILITY_SHATTER_COLUMN)

ex._abilityDatas[ex.ABILITY_EARTHQUAKE] =
{
   type : ex.ABILITY_EARTHQUAKE,
   cost : 6,
   nmElim : true, //can negate no moves
   actions : 
   [
      {                                                                             //create a new action,
         pickRangeType : uRange.RANGE_NONE,                          //pick range for this action - single stone
         affectRangeType : uRange.RANGE_ALL_STONES,              //range for this action - selected stone and its row and column
         showRangeType: uRange.RANGE_NONE,                 //show range for this action - selected stone and its row and column
         affectType : uAffect.AFFECT_REMOVE_BALLS,                                  //affect - remove balls
         rangeArgs : [],                                                            //range args. empty this time.
         affectArgs : [],                                                           //affect args, specifying how affect should behave. empty
         visualType : visual.VISUALIZE_SHAKE,
         maxSelected : 0,
         minSelected : 0,
         minTargets : 36,
         maxTargets : 36
      }
   ]
}
_abilityTypes.push(ex.ABILITY_EARTHQUAKE)

ex._abilityDatas[ex.ABILITY_REMOVE_STONE] =
{
   type : ex.ABILITY_REMOVE_STONE,
   cost : 2,
   nmElim : true, //can negate no moves
   actions : 
   [
      {                                                             //create new action
         pickRangeType : uRange.RANGE_SELECTED_STONE_ONLY,                //pick range for this action - single selected stone
         affectRangeType : uRange.RANGE_SELECTED_STONE_ONLY,    //range for this action - selected stone only
         showRangeType : uRange.RANGE_SELECTED_STONE_ONLY,        //show range - selected stone only
         affectType : uAffect.AFFECT_REMOVE_BALLS,                         //affect remove balls
         rangeArgs : [],                                                //range args. empty this time.
         affectArgs : [],
         maxSelected : 1,
         minSelected : 1,
         minTargets : 1,
         maxTargets : 1
      }
   ]
}
_abilityTypes.push(ex.ABILITY_REMOVE_STONE)

ex._abilityDatas[ex.ABILITY_SHIELD_OF_ICE] =
{
   type : ex.ABILITY_SHIELD_OF_ICE,
   cost : 2,
   actions : 
   [
      {                                                                             //create a new action,
         pickRangeType : uRange.RANGE_NONE,                                         //pick range for this action - automatic, no need to pick a target,
         affectRangeType : uRange.RANGE_YOUR_HERO,                                  //range for this action - your hero,
         showRangeType : uRange.RANGE_NONE, 
         affectType : uAffect.AFFECT_ADD_SHIELD,                                    //affect - heal,
         rangeArgs : [],                                                            //range args. empty this time.,
         affectArgs : [ 3 ],                                                        //affect args, specifying how affect should behave. empty
         maxSelected : 0,
         minSelected : 0,
         minTargets : 1,
         maxTargets : 1
      }
   ]
}
_abilityTypes.push(ex.ABILITY_SHIELD_OF_ICE)

ex._abilityDatas[ex.ABILITY_FREEZING_RAIN] =
{
   type : ex.ABILITY_FREEZING_RAIN,
   cost : 3,
   actions : 
   [
      {                                                             //create new action
         pickRangeType : uRange.RANGE_NONE,                //pick range for this action - single stone
         affectRangeType : uRange.RANGE_NONE,    //range for this action - selected stone and its neighbours
         showRangeType : uRange.RANGE_NONE,
         affectType : uAffect.AFFECT_APPLY_GLOBAL_EFFECT,                         //affect apply effect
         rangeArgs : [],                                                //range args. empty this time.
         affectArgs : 
         [                                                 //affect args, specifying how affect should behave
            uGlobalEffects.EFFECT_FREEZING_RAIN,                            //args[0] - what effect to apply to the targets
            [ 1 ],                                               //args[1] - effect args - empty
         ],
         maxSelected : 0,
         minSelected : 0,
         minTargets : 0,
         maxTargets : 0
      }
   ]
}
_abilityTypes.push(ex.ABILITY_FREEZING_RAIN)

ex._abilityDatas[ex.ABILITY_POWER_OF_ABYSS] =
{
   type : ex.ABILITY_POWER_OF_ABYSS,
   cost : 0,
   actions : 
   [
      {                                                             //create new action
         pickRangeType : uRange.RANGE_NONE,                //pick range for this action - single stone
         affectRangeType : uRange.RANGE_YOUR_HERO,    //range for this action - selected stone and its neighbours
         showRangeType : uRange.RANGE_NONE,
         affectType : uAffect.AFFECT_CHARGE_RANDOM_ABILITY,                         //affect change ability - add mana to the player
         rangeArgs : [],                                                //range args. empty this time.
         affectArgs : [ 2 ],
         maxSelected : 0,
         minSelected : 0,
         minTargets : 1,
         maxTargets : 1
      },
      {                                                             //create new action
         pickRangeType : uRange.RANGE_NONE,                //pick range for this action - single stone
         affectRangeType : uRange.RANGE_YOUR_HERO,    //range for this action - selected stone and its neighbours
         showRangeType : uRange.RANGE_NONE,
         affectType : uAffect.AFFECT_DAMAGE,                         //affect change ability - add mana to the player
         rangeArgs : [],                                                //range args. empty this time.
         affectArgs : [ 2 ],
         maxSelected : 0,
         minSelected : 0,
         minTargets : 1,
         maxTargets : 1
      }
   ]
}
_abilityTypes.push(ex.ABILITY_POWER_OF_ABYSS)

ex._abilityDatas[ex.ABILITY_ICE_BLOCK] =
{
   type : ex.ABILITY_ICE_BLOCK,
   cost : 3,
   nmElim : true, //can negate no moves
   actions : 
   [
      {                                                             //create new action
         pickRangeType : uRange.RANGE_NONE,                //pick range for this action - single stone
         affectRangeType : uRange.RANGE_YOUR_HERO,    //range for this action - selected stone and its neighbours
         showRangeType : uRange.RANGE_NONE,
         affectType : uAffect.AFFECT_APPLY_EFFECT,                         //affect apply effect
         rangeArgs : [],                                                //range args. empty this time.
         affectArgs : 
         [                                                 //affect args, specifying how affect should behave
            uEffect.EFFECT_ICE_BLOCK,                            //args[0] - what effect to apply to the targets
            [ ],                                               //args[1] - effect args: 1) mine damage
         ],
         maxSelected : 0,
         minSelected : 0,
         minTargets : 1,
         maxTargets : 1
      },
      {                                                             //create new action
         pickRangeType : uRange.RANGE_NONE,                //pick range for this action - single stone
         affectRangeType : uRange.RANGE_NONE,    //range for this action - selected stone and its neighbours
         showRangeType : uRange.RANGE_NONE,
         affectType : uAffect.AFFECT_PASS_TURN,                         //affect apply effect
         rangeArgs : [],                                                //range args. empty this time.
         affectArgs : [],
         maxSelected : 0,
         minSelected : 0,
         minTargets : 0,
         maxTargets : 0
      }      
   ]
}
//_abilityTypes.push(ex.ABILITY_ICE_BLOCK)

ex._abilityDatas[ex.ABILITY_POWER_DRAIN] =
{
   type : ex.ABILITY_POWER_DRAIN,
   cost : 1,
   actions : 
   [
      {                                                             //create new action
         pickRangeType : uRange.RANGE_SELECTED_STONE_ONLY,                //pick range for this action - single stone
         affectRangeType : uRange.RANGE_STONES_IN_SELECTED_CHAIN,    //range for this action - selected stone and its neighbours
         showRangeType : uRange.RANGE_STONES_IN_SELECTED_CHAIN,
         affectType : uAffect.AFFECT_APPLY_EFFECT,                         //affect apply effect
         rangeArgs : [],                                                //range args. empty this time.
         affectArgs : 
         [                                                 //affect args, specifying how affect should behave
            uEffect.EFFECT_FROZEN,                            //args[0] - what effect to apply to the targets
            [ 1 ],                                               //args[1] - effect args
         ],
         aChk : ex.CHECK_LENGTH_MINIMUM,
         aChkArgs : [ 5 ],
         maxSelected : 1,
         minSelected : 1,
         minTargets : 5,
         maxTargets : 36
      },
      {                                                                       //create new action
         pickRangeType : uRange.RANGE_YOUR_SELECTED_ABILTIY,                  //pick range for this action - your selected ability
         affectRangeType : uRange.RANGE_YOUR_SELECTED_ABILTIY,                //range for this action - your selected ability
         showRangeType : uRange.RANGE_NONE,                                   //show range - none      
         affectType : uAffect.AFFECT_CHARGE_ABILITY,                          //affect apply effect
         rangeArgs : [],                                                      //range args - empty
         affectArgs : [ 4 ],                                                  //affect args - how much to charge the ability by
         maxSelected : 1,
         minSelected : 1,
         minTargets : 1,
         maxTargets : 1
      }      
   ]
}
_abilityTypes.push(ex.ABILITY_POWER_DRAIN)

ex._abilityDatas[ex.ABILITY_INFECTION] =
{
   type : ex.ABILITY_INFECTION,
   //color : uConsts.BALL_RED,
   cost : 3,
   actions : 
   [
      {                                                             //create new action
         pickRangeType : uRange.RANGE_NONE,                //pick range for this action - single stone
         affectRangeType : uRange.RANGE_STONES_WITH_OWN_EFFECTS,    //range for this action - selected stone and its neighbours
         showRangeType : uRange.RANGE_NONE,
         affectType : uAffect.AFFECT_SPREAD_OWN_EFFECT,                         //affect apply effect
         rangeArgs : [],                                                //range args. empty this time.
         affectArgs : [],
         visualType : visual.VISUALIZE_BALL_PARTICLE,
         visualArgs : [ "greenM", 0.5 ],                 
         maxSelected : 0,
         minSelected : 0,
         minTargets : 0,
         maxTargets : 36
      }
   ]
}
_abilityTypes.push(ex.ABILITY_INFECTION)

for (var i = 0; i < _abilityTypes.length; i++)
{
   var abData = ex._abilityDatas[_abilityTypes[i]]
   //_abilityTypes.push(abData.type)
   if (abData.actions.length === 1)
   {
      var act = abData.actions[0]
      if (uAffect.aiSupportedTypes.has(act.affectType))
      {         
         if (act.affectType === uAffect.AFFECT_APPLY_EFFECT)
         {            
            if (act.affectArgs[0] === uEffect.EFFECT_POISON || act.affectArgs[0] === uEffect.EFFECT_BOMB || act.affectArgs[0] === uEffect.EFFECT_STONE_OF_HEALTH)
               ex._aiAbilityTypes.push(abData.type)
         }
         else
         {
            ex._aiAbilityTypes.push(abData.type)
         }
      }
   }   
}

console.log("_abilityTypes",_abilityTypes)
console.log("number:",_abilityTypes.length)