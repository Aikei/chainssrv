var globalEffects = module.exports

var uDbg = require('../debug')
var uConsts = require('../constants')
var uPrefs = require('../preferences')
var uMisc = require('../misc')

globalEffects.EFFECT_FREEZING_RAIN = "FreezingRain"
globalEffects.EFFECT_RAGE = "Rage"

globalEffects.GlobalEffect = function(type,owner,time)
{
   this.type = type
   this.time = time || 1
   this.tickInterval = 1
   this.tickIntervalCounter = 0  
   this.owner = owner
   this.OnTick = function() { }
   
   this.Tick = function()
   {
      this.tickIntervalCounter++
      if (this.tickIntervalCounter === this.tickInterval)
      {
         this.tickIntervalCounter = 0         
         if (this.time > -1)
         {
            this.time--
            this.OnTick()
            if (this.time === 0)
            {
               uDbg.Log("global effect",this.type,"returning true")
               return true
            }
         }
         else
         {
            this.OnTick()
         }
      }
      return false
   }
   
   this.TickBeforeMoving = function()
   {
      return this.Tick()
   }
   
   this.TickAfterMoving = function()
   {
      return false
   }

   this.OnAdded = function(game)
   {
      
   }
   
   this.OnRemoved = function(game)
   {
      
   }  
}

globalEffects.EffectFreezingRain = function(time,game)
{
   globalEffects.GlobalEffect.call(this,globalEffects.EFFECT_FREEZING_RAIN,game.GetMovingPlayer(),time)
   this.time = time || 1
   
   this.TickBeforeMoving = function()
   {
      return false
   }
   
   this.TickAfterMoving = function()
   {
      return this.Tick()
   }   
}
globalEffects.EffectFreezingRain.prototype = new globalEffects.GlobalEffect(globalEffects.EFFECT_FREEZING_RAIN)
globalEffects.EffectFreezingRain.prototype.constructor = globalEffects.EffectFreezingRain

globalEffects.EffectRage = function(ballType, owner, mult1, mult2, manaMult, time)
{
   globalEffects.GlobalEffect.call(this,globalEffects.EFFECT_RAGE,owner,time)
   this.ballType = ballType
   this.time = time || -1
   this.mult1 = mult1 || 1.5
   this.mult2 = mult2 || 2.0
   this.manaMult = manaMult || 0.3
   
   this.OnAdded = function(game)
   {
      uDbg.Log("Multiplier before multiplication: ", this.owner.ballDamageMultiplier[ballType])
      
      for (var i = 0; i < uConsts.NUMBER_OF_BALL_TYPES; i++)
      {
         this.owner.ballDamageMultiplier[i] += this.mult1
         this.owner.ballManaMultiplier[i] -= this.manaMult
         if (this.owner.ballManaMultiplier[i] < 0)
            this.owner.ballManaMultiplier[i] = 0
      }      
      
      // if (ballType === -1)
      // {
         // for (var i = 0; i < uConsts.NUMBER_OF_BALL_TYPES; i++)
         // {
            // this.owner.ballDamageMultiplier[i] = this.mult2
            // this.owner.ballManaMultiplier[i] = 0
         // }
      // }
      // else
      // {
         // this.owner.ballDamageMultiplier[ballType] = this.mult1
         // this.owner.ballManaMultiplier[ballType] = 0
      // }
      uDbg.Log("Multiplier after multiplication: ", this.owner.ballDamageMultiplier[ballType])
   }
   
   this.OnRemoved = function(game)
   {
      for (var i = 0; i < uConsts.NUMBER_OF_BALL_TYPES; i++)
      {
         this.owner.ballDamageMultiplier[i] = 1
         this.owner.ballManaMultiplier[i] = 1
      }      
      // if (ballType === -1)
      // {
         // for (var i = 0; i < uConsts.NUMBER_OF_BALL_TYPES; i++)
         // {
            // this.owner.ballDamageMultiplier[i] = 1
            // this.owner.ballManaMultiplier[i] = 1
         // }
      // }
      // else
      // {
         // this.owner.ballDamageMultiplier[ballType] = 1
         // this.owner.ballManaMultiplier[ballType] = 1
      // }      
   }
}
globalEffects.EffectRage.prototype = new globalEffects.GlobalEffect(globalEffects.EFFECT_RAGE)
globalEffects.EffectRage.prototype.constructor = globalEffects.EffectRage
