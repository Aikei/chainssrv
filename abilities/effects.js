//ef._effects

var ef = module.exports

//ball effects
ef.EFFECT_FROZEN = "Frozen"
ef.EFFECT_POISON = "Poison"
ef.EFFECT_BOMB = "Bomb"
ef.EFFECT_STONE_OF_HEALTH = "StoneOfHealth"
ef.EFFECT_GOBLIN_MINE = "GoblinMine"
ef.EFFECT_FIRE_BOMB = "FireBomb"
ef.EFFECT_FROST = "Frost"
ef.EFFECT_FROST_NOVA = "NovaFrost"
ef.EFFECT_FIRE_MINE = "FireMine"

//player effects
ef.EFFECT_ICE_BLOCK = "IceBlock"
ef.EFFECT_FALSE_PLANS = "FalsePlans"

var uDbg = require('../debug')
var uConsts = require('../constants')
var uPrefs = require('../preferences')
var uMisc = require('../misc')
var uAbility = require('./ability')



ef.Effect = function(type,owner,time)
{
   this.type = type
   this.time = time || 1
   this.startingTime = time
   this.tickInterval = 1
   this.tickIntervalCounter = 0
   this.triggerOnSelect = false
   this.removeOnSelect = false
   this.triggerOnTrigger = false
   this.host = null
   this.owner = owner || null
   this.owningEffect = true   
   this.tickPriority = 1
   this.tookOff = false
   this.removal = false
}

ef.Effect.prototype.Trigger = function(turn) { }

ef.Effect.prototype.OnTick = function() { }

ef.Effect.prototype.Tick = function()
{
   this.tickIntervalCounter++
   if (this.tickIntervalCounter === this.tickInterval)
   {
      this.tickIntervalCounter = 0         
      if (this.time > -1)
      {
         this.time--
         this.OnTick()
         if (this.time === 0)
         {
            uDbg.Log("_i","effect",this.type,"tick returning true")
            return true
         }
      }
      else
      {
         this.OnTick()
      }
   }
   return false
}

ef.Effect.prototype.TickBeforeMoving = function()
{
   return this.Tick()
}

ef.Effect.prototype.TickAfterMoving = function()
{
   return false
}

//Frozen
uPrefs.AllEffectTypes.push(ef.EFFECT_FROZEN)
ef.EffectFrozen = function(time)
{
   ef.Effect.call(this,ef.EFFECT_FROZEN,undefined,time)
   this.owningEffect = false
   this.time = time || 2
}
ef.EffectFrozen.prototype = new ef.Effect(ef.EFFECT_FROZEN)
ef.EffectFrozen.prototype.constructor = ef.EffectFrozen

ef.EffectFrozen.prototype.CreateIdenticalEffect = function()
{
   return new ef.EffectFrozen(this.startingTime)
}

//Poision
uPrefs.AllEffectTypes.push(ef.EFFECT_POISON)
ef.EffectPoison = function(damage,game,owner,targetPlayer)
{
   owner = owner || game.GetMovingPlayer()
   targetPlayer = targetPlayer || game.GetEnemyOfMovingPlayer() 
   ef.Effect.call(this,ef.EFFECT_POISON,owner,-1)
   this.tickInterval = 2
   this.tickIntervalCounter = 1
   this.targetPlayer = targetPlayer
   this.damage = damage
}

ef.EffectPoison.prototype = new ef.Effect(ef.EFFECT_POISON)
ef.EffectPoison.prototype.constructor = ef.EffectPoison

ef.EffectPoison.prototype.OnTick = function()
{
   this.targetPlayer.hp -= this.damage
}

ef.EffectPoison.prototype.CreateIdenticalEffect = function()
{
   return new ef.EffectPoison(this.damage,undefined,this.owner,this.targetPlayer)
}   

//Bomb
uPrefs.AllEffectTypes.push(ef.EFFECT_BOMB)
ef.EffectBomb = function(damage,time,game,owner,targetPlayer)
{
   owner = owner || game.GetMovingPlayer()
   targetPlayer = targetPlayer || game.GetEnemyOfMovingPlayer()   
   ef.Effect.call(this,ef.EFFECT_BOMB,owner,time)
   this.damage = damage
   //this.time = time
   this.startingTime = time
   this.targetPlayer = targetPlayer
   this.removal = true
}
ef.EffectBomb.prototype = new ef.Effect(ef.EFFECT_BOMB)
ef.EffectBomb.prototype.constructor = ef.EffectBomb

ef.EffectBomb.prototype.OnTick = function()
{
   if (this.time === 0)
   {
      this.targetPlayer.hp -= this.damage
      this.host.RemoveThisBall()
   }
}

ef.EffectBomb.prototype.CreateIdenticalEffect = function()
{
   return new ef.EffectBomb(this.damage,this.startingTime,undefined,this.owner,this.targetPlayer)
}

//StoneOfHealth
uPrefs.AllEffectTypes.push(ef.EFFECT_STONE_OF_HEALTH)
ef.EffectStoneOfHealth = function(heal,game,owner)
{
   owner = owner || game.GetMovingPlayer()
   ef.Effect.call(this,ef.EFFECT_STONE_OF_HEALTH,owner,-1)
   this.tickInterval = 2
   this.tickIntervalCounter = 1
   this.targetPlayer = owner
   this.heal = heal   
}
ef.EffectStoneOfHealth.prototype = new ef.Effect(ef.EFFECT_STONE_OF_HEALTH)
ef.EffectStoneOfHealth.prototype.constructor = ef.EffectStoneOfHealth

ef.EffectStoneOfHealth.prototype.OnTick = function()
{
   this.targetPlayer.hp += this.heal
}

ef.EffectStoneOfHealth.prototype.CreateIdenticalEffect = function()
{
   return new ef.EffectStoneOfHealth(this.heal,undefined,this.owner)
}   

//GoblinMine
uPrefs.AllEffectTypes.push(ef.EFFECT_GOBLIN_MINE)
ef.EffectGoblinMine = function(damage,game,owner,targetPlayer)
{
   owner = owner || game.GetMovingPlayer()
   targetPlayer = targetPlayer || game.GetEnemyOfMovingPlayer()
   ef.Effect.call(this,ef.EFFECT_GOBLIN_MINE,owner,-1)
   this.tickInterval = 2
   this.tickIntervalCounter = 1
   this.targetPlayer = targetPlayer
   this.damage = damage
   this.triggerOnSelect = true
   this.removeOnTrigger = true     
}
ef.EffectGoblinMine.prototype = new ef.Effect(ef.EFFECT_GOBLIN_MINE)
ef.EffectGoblinMine.prototype.constructor = ef.EffectGoblinMine

ef.EffectGoblinMine.prototype.Trigger = function(turn)
{
   uDbg.Log("Triggering effect Goblin Mine")
   if (turn === this.targetPlayer.number)
   {
      uDbg.Log("Affecting effect Goblin Mine")
      this.targetPlayer.hp -= this.damage
      return true
   }
   return false
}

ef.EffectGoblinMine.prototype.CreateIdenticalEffect = function()
{
   return new ef.EffectGoblinMine(this.damage,undefined,this.owner,this.targetPlayer)
}    

//player effects

//IceBlock
ef.EffectIceBlock = function(game,owner)
{
   owner = owner || game.GetMovingPlayer()
   ef.Effect.call(this,ef.EFFECT_ICE_BLOCK,owner,2)
   this.startingTime = this.time     
}
ef.EffectIceBlock.prototype = new ef.Effect(ef.EFFECT_ICE_BLOCK)
ef.EffectIceBlock.prototype.constructor = ef.EffectIceBlock

ef.EffectIceBlock.prototype.CreateIdenticalEffect = function()
{
   return new ef.EffectIceBlock(undefined, this.owner)
}

//FalsePlans
ef.EffectFalsePlans = function(owner)
{
   ef.Effect.call(this,ef.EFFECT_FALSE_PLANS,owner,1)
   this.startingTime = this.time     
}
ef.EffectFalsePlans.prototype = new ef.Effect(ef.EFFECT_FALSE_PLANS)
ef.EffectFalsePlans.prototype.constructor = ef.EffectFalsePlans

ef.EffectFalsePlans.prototype.CreateIdenticalEffect = function()
{
   return new ef.EffectFalsePlans(this.owner)
}

//FireBomb
uPrefs.AllEffectTypes.push(ef.EFFECT_FIRE_BOMB)
ef.EffectFireBomb = function(game,owner)
{
   owner = owner || game.GetMovingPlayer()
   this.ab = new uAbility.Ability(uAbility._abilityDatas[uAbility.ABILITY_FIRE_BOMB],owner,-1)
   this.game = game
   ef.Effect.call(this,ef.EFFECT_FIRE_BOMB,owner,1)
   this.tickPriority = 2
   this.removal = true
}
ef.EffectFireBomb.prototype = new ef.Effect(ef.EFFECT_FIRE_BOMB)
ef.EffectFireBomb.prototype.constructor = ef.EffectFireBomb

ef.EffectFireBomb.prototype.OnTick = function()
{
   if (this.time === 0)
   {
      this.tookOff = true
      this.ab.Use(this.game, null, null, [ { type : "b", x : this.host.x, y : this.host.y } ])
      //game.MoveBallsAfterRemoving()
   }
}

ef.EffectFireBomb.prototype.TickBeforeMoving = function()
{
   return false
}

ef.EffectFireBomb.prototype.TickAfterMoving = function()
{
   return this.Tick()
}

ef.EffectFireBomb.prototype.CreateIdenticalEffect = function()
{
   return new ef.EffectFireBomb(this.game,this.owner)
}

//FireMine
uPrefs.AllEffectTypes.push(ef.EFFECT_FIRE_MINE)
ef.EffectFireMine = function(game,owner)
{
   owner = owner || game.GetMovingPlayer()
   this.ab = new uAbility.Ability(uAbility._abilityDatas[uAbility.ABILITY_FIRE_IMMEDIATE],owner,-1)
   this.game = game
   ef.Effect.call(this,ef.EFFECT_FIRE_MINE,owner,1)
   this.tickPriority = 2
   this.removal = true
}
ef.EffectFireMine.prototype = new ef.Effect(ef.EFFECT_FIRE_MINE)
ef.EffectFireMine.prototype.constructor = ef.EffectFireMine

ef.EffectFireMine.prototype.OnTick = function()
{
   if (this.time === 0)
   {
      this.tookOff = true
      this.ab.Use(this.game, null, null, [ { type : "b", x : this.host.x, y : this.host.y } ])
      //game.MoveBallsAfterRemoving()
   }
}

ef.EffectFireMine.prototype.TickBeforeMoving = function()
{
   return false
}

ef.EffectFireMine.prototype.TickAfterMoving = function()
{
   return this.Tick()
}

ef.EffectFireMine.prototype.CreateIdenticalEffect = function()
{
   return new ef.EffectFireMine(this.game,this.owner)
}

//Frost
uPrefs.AllEffectTypes.push(ef.EFFECT_FROST)
ef.EffectFrost = function(game,owner)
{
   owner = owner || game.GetMovingPlayer()
   this.ab = new uAbility.Ability(uAbility._abilityDatas[uAbility.ABILITY_SINGLE_FREEZE],owner,-1)
   this.game = game
   ef.Effect.call(this,ef.EFFECT_FROST,owner,1)
}
ef.EffectFrost.prototype = new ef.Effect(ef.EFFECT_FROST)
ef.EffectFrost.prototype.constructor = ef.EffectFrost

ef.EffectFrost.prototype.OnTick = function()
{
   if (this.time === 0)
   {
      this.tookOff = true
      this.ab.Use(this.game, null, null, [ { type : "b", x : this.host.x, y : this.host.y } ])
      //game.MoveBallsAfterRemoving()
   }
}

ef.EffectFrost.prototype.TickBeforeMoving = function()
{
   return false
}

ef.EffectFrost.prototype.TickAfterMoving = function()
{
   return this.Tick()
}

ef.EffectFrost.prototype.CreateIdenticalEffect = function()
{
   return new ef.EffectFrost(this.game,this.owner)
}

//FrostNova
uPrefs.AllEffectTypes.push(ef.EFFECT_FROST_NOVA)
ef.EffectFrostNova = function(game,owner)
{
   owner = owner || game.GetMovingPlayer()
   this.ab = new uAbility.Ability(uAbility._abilityDatas[uAbility.ABILITY_FROST_NOVA],owner,-1)
   this.game = game
   ef.Effect.call(this,ef.EFFECT_FROST_NOVA,owner,1)
}
ef.EffectFrostNova.prototype = new ef.Effect(ef.EFFECT_FROST_NOVA)
ef.EffectFrostNova.prototype.constructor = ef.EffectFrostNova

ef.EffectFrostNova.prototype.OnTick = function()
{
   if (this.time === 0)
   {
      this.tookOff = true
      this.ab.Use(this.game, null, null, [ { type : "b", x : this.host.x, y : this.host.y } ])
      //game.MoveBallsAfterRemoving()
   }
}

ef.EffectFrostNova.prototype.TickBeforeMoving = function()
{
   return false
}

ef.EffectFrostNova.prototype.TickAfterMoving = function()
{
   return this.Tick()
}

ef.EffectFrostNova.prototype.CreateIdenticalEffect = function()
{
   return new ef.EffectFrostNova(this.game,this.owner)
}