//debug.js

var uFs = require('fs')
var uAsync = require('./node_modules/async/lib/async')
var uWinston = require('winston')

var ex = module.exports

var logLevel = 2
var logFileName = "s_log.txt"
var lastMsgNumber = 2

var logger = new uWinston.Logger(
{
   transports: [ new (uWinston.transports.File)({ filename: logFileName, json : false }) ]
})

var MAX_OPEN_FILES = 128

function ToText(data)
{
   if (data && typeof(data) != "string")
   {
      try
      {
         return JSON.stringify(data)
      }
      catch(err)
      {
         return data
      }
   }
   else
      return data
}

ex.__Log = function(filename)
{
   var topArgs = arguments
   return function(callback)
   {
      var allData = "["+lastMsgNumber+"] "
      lastMsgNumber++
      for (var i = 1; i < topArgs.length; i++)
      {
         allData += ToText(topArgs[i])
         if (i < topArgs.length-1)
            allData += " "
         else
            allData += "\n"
      }
      uFs.open(filename,"a",function(error,file)
      {
         if (!error)
         {
            uFs.write(file,allData,null,'utf8', function(err)
            {
               if (err)
               {
                  console.log(err)
                  console.log(err.stack)
               }
               uFs.close(file, function() { callback(); });
            })
         }
         else
         {
            console.log(error)
            console.log(error.stack)
            callback()
         }
      })
   }
}


// ex.Log = function()
// {
   // if (arguments[0] !== '_i' && arguments[0] !== '_ii' && logLevel === 1)
      // return      
   // if (arguments[0] !== '_ii' && logLevel === 2)
      // return            
   
   // var allData = ""
   // for (var i = 0; i < arguments.length; i++)
   // {
      // allData += arguments[i]
      // if (i < arguments.length-1)
         // allData += " "
      // else
         // allData += "\n"
   // }  
   // uFs.appendFile(logFileName,allData)   
// }

ex.__LogToConsole = function()
{
   if (logLevel === -1)
      return   
   if (arguments[0] !== '_i' && arguments[0] !== '_ii' && logLevel === 1)
      return      
   if (arguments[0] !== '_ii' && logLevel === 2)
      return  
   console.log.apply(null,arguments)
}

ex.__LogToFile = function()
{
   //var args = Array.prototype.slice.call(arguments)
   //console.log.apply(console,args)
   if (logLevel === -1)
      return
   if (arguments[0] !== '_i' && arguments[0] !== '_ii' && logLevel === 1)
      return      
   if (arguments[0] !== '_ii' && logLevel === 2)
      return
   for (var i = 0; i < arguments.length; i++)
      arguments[i] = ToText(arguments[i])
   var args = []
   args.push(logFileName)
   args = args.concat(Array.prototype.slice.call(arguments))
   var fullText = ""
   for (var i = 1; i < args.length; i++)
      fullText += args[i] + " "
   logger.info(fullText)
   //ex.__writeQueue.push(ex.__Log.apply(null,args))
}

ex.LogSync = function()
{
   if (arguments[0] !== '_i' && arguments[0] !== '_ii' && logLevel === 1)
      return      
   if (arguments[0] !== '_ii' && logLevel === 2)
      return    
   var allData = ""
   for (var i = 0; i < arguments.length; i++)
   {
      allData += arguments[i]
      if (i < arguments.length-1)
         allData += " "
      else
         allData += "\n"
   }   
   uFs.appendFileSync(logFileName,allData)
}

ex.Log = ex.__LogToFile
//ex.Log = ex.__LogToConsole
//ex.Log = ex.LogSync

function RefreshLogger()
{
   delete uWinston
   uWinston = null
   uWinston = require('winston')
   
   if (logger)
   {
      logger.close()
   }
   
   logger = new uWinston.Logger(
   {
      transports: [ new (uWinston.transports.File)({ filename: logFileName, json : false }) ]
   })   
}

ex.Init = function()
{
   setInterval(RefreshLogger, 10000)
   uFs.appendFileSync(logFileName,"\n\n------------------------------------------------------------\n")
   uFs.appendFileSync(logFileName,"SERVER RESTARTED\n\n")
}

ex.ClearLog = function()
{
   uFs.writeFileSync(logFileName,"Log Cleared\n")
}

ex.__writeQueue = uAsync.queue(function (task, callback)
{
   task(callback)
}, MAX_OPEN_FILES)