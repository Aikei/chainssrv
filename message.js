//message.js

var uPrefs = require('./preferences')
var uMys = require('./mys.js')
var zlib = require('zlib')
var uDbg = require('./debug.js')
var uMisc = require('./misc.js')
var uMongo = require('./mongo.js')

var ex = module.exports

const kMessageTerminator = "\n";
const kNullCharacter = "\0";

ex.Names =
{
   kTime : "a",
   kCreatePlayer : "b",
   kReady : "c",
   kLeaderboard : "d",
   kHunt : "e",
   kBuy : "f",
   kEquip : "g",
   kNameEntered : "h",
   kCancelLook : "i",
   kCheckMonsterLimit : "j",
   kServerMessage: "k",
   kStartTutorial : "l",
   kMainTutorialDone : "m",
   kFindMatch: "n",
   kRefreshCrystals: "o",
   kPreferences: "p",
   kAuthorize: "q",
   kCloseConnection : "r",
   kConfirmConnect: "v",
   kFirstConnect: "x",
   
   kCheater: "ch",
   kServerInfo: "si",
   kListPlayers: "lp",
   
   kLobbyMessage: "s",
      ksPickAbility: "t",
      ksUnpickAbility: "u",
   
   kGameMessage: "gm",
      ksGameStart: "gs",
      ksRepick: "a",
      ksEnemyAbilityReroll: "b",
      ksReequipItem: "c",
      ksBallSelected: "d",
      ksTurnDone: "e",
      ksLastBallDeselected: "f",
      ksDeselectAll: "g",
      ksRemoveBalls: "h",
      ksNewEntropy: "i",
      ksNextTurn: "j",
      ksOpponentFound: "k",
      ksCheckField: "cf",
      ksEndGame: "l",
      ksRefreshPlayerData: "m",
      ksAiNoMoves: "n",
      ksAbilityUsed : "o",
      ksSelectedAbilityTargets: "p",
      ksDeselectedAbilityTargets: "q",
      ksAbilityButtonActivated : "r",
      ksTutorialStart: "s",
      ksReady: "t",
      ksReconstruct: "u",
      ksSurrender: "v",
      ksNoMoves: "w",
      ksDetargetAll: "y"
}

ex.Message = function(n,s,data)
{
   this.n = n
   this.s = s
   this.m_data = data
}

ex.CheckAndGetMessage = function(msg,cb)
{
   var valid = true
   
   // if (uPrefs.uh)
   // {
      // if (!uMisc.IsMessageValid(msg))
      // {
         // uDbg.Log("_ii", "invalid message",msg.m_data)
         // cb(null)
         // return
      // }
   // }  
   
   if (uPrefs.uz)
   {
      var dataBuffer = new Buffer(msg,'base64')
      zlib.inflate(dataBuffer,function(err, result)
      {
         if (err)
         {
            //uDbg.Log("_ii", "error decoding buffer from string",msg)
            //uMongo.LogError(msg,"CheckAndGetMessage_DecodingError")
            cb(null)
         }
         else
         {
            msg = JSON.parse(result)
            if (uPrefs.uh)
            {
               if (uMisc.IsMessageValid(msg))
               {
                  cb(msg)
               }
               else
               {
                  uMongo.LogError(msg,"CheckAndGetMessage_MsgInvalid")
                  uDbg.Log('_ii','msg check invalid:',msg)
                  cb(null)
                  //cb(msg)
               }
            }
            else
               cb(msg)
         }
      })
   }
   else
   {
      cb(msg)
   }
}

// ex.CheckAndGetMessage = function(msg,cb)
// {
   // var valid = true
   
   // if (uPrefs.uh)
   // {
      // if (!uMisc.IsMessageValid(msg))
      // {
         // uDbg.Log("_ii", "invalid message",msg.m_data)
         // cb(null)
         // return
      // }
   // }  
   
   // if (uPrefs.uz)
   // {
      // if (msg.m_data)
      // {
         // var dataBuffer = new Buffer(msg.m_data,'base64')
         // zlib.inflate(dataBuffer,function(err, result)
         // {
            // if (err)
            // {
               // uDbg.Log("_ii", "error decoding buffer from string",msg.m_data)
               // cb(null)
            // }
            // else
            // {
               // msg.m_data = JSON.parse(result)
               // cb(msg)
            // }
         // })
      // }
      // else
      // {
         // cb(msg)
      // }
   // }
   // else
   // {
      // cb(msg)
   // }
// }

ex.GameStartMessage = function(yourNumber, game)
{
   this.n = ex.Names.kGameMessage
   this.s = ex.Names.ksGameStart
   this.m_data = {}
   //this.m_data.ss = game.ss
   this.m_data.n = yourNumber
   this.m_data.firstTurn = game.turn
   this.m_data.field = game.field
   this.m_data.maxHP = uPrefs.maxHP
   this.m_data.g = game.id
   this.m_data.abs = []
   //this.m_data.heroClasses = []
   this.m_data.st = game.turnStartTime
   for (var i = 0; i < game.players.length; i++)
   {
      //this.m_data.heroClasses.push(game.players[i].heroClass)
      this.m_data.abs.push([])
      for (var key in game.players[i].abs)
      {
         this.m_data.abs[i].push(game.players[i].abs[key].type)         
      }
      if (i !== yourNumber)
      {
         this.m_data.enemyProfile = game.players[i].profile
      }
      else
      {
         this.m_data.yourProfile = game.players[i].profile
      }
   }                  
}

ex.CheatMessage = function()
{
   this.n = ex.Names.kCheater
   this.s = undefined
   this.m_data = undefined
}

// ex.EntropyMessage = function(entropy,dice,gameId)
// {
   // this.n = ex.Names.kGameMessage
   // this.s = ex.Names.ksNewEntropy
   // this.m_data = {}
   // this.m_data.g = gameId
   // this.m_data.entropy = entropy
   // this.m_data.dice = dice
// }

ex.NextTurnMessage = function(game)
{
   this.n = ex.Names.kGameMessage
   this.s = ex.Names.ksNextTurn
   this.m_data = {}
   this.m_data.turn = game.turn
   this.m_data.g = game.id
   this.m_data.turnNumber = game.turnNumber
   this.m_data.st = game.turnStartTime
}

ex.SendMessage = function(player, msg)
{
   if (!player.ai || msg.n === ex.Names.kCheater)
   {
      ex.SendDataToSocket(player.socket,msg)
      
      //test
      //setTimeout(ex.SendDataToSocket,500,player.socket,msg)
   }
}

/*
ex.SendMessage = function(player, msg)
{
   if (!player.ai || msg.n === ex.Names.kCheater)
   {
      if (uPrefs.uz)
      {
         if (msg.m_data == undefined)
            msg.m_data = {}
         if (msg.s == null)
            msg.s = ""
         zlib.deflate(JSON.stringify(msg.m_data), function(err, buffer)
         {
            msg.m_data = buffer.toString('base64')
            uMys.SendData(player.socket,msg)
         })
      }
      else
      {
         uMys.SendData(player.socket,msg)
      }
   }
}
*/

ex.SendDataToSocket = function(socket,msg)
{
   if (uPrefs.uz)
   {
      zlib.deflate(JSON.stringify(msg), function(err, buffer)
      {
         msg = buffer.toString('base64')
         socket.write(msg+kMessageTerminator+kNullCharacter)
      })
   }
   else
   {
      socket.write(uMisc.Serialize(msg)+kMessageTerminator+kNullCharacter)
   } 
}

ex.SendDataToSocketAndClose = function(socket,msg)
{
   if (uPrefs.uz)
   {
      zlib.deflate(JSON.stringify(msg), function(err, buffer)
      {
         msg = buffer.toString('base64')
         socket.end(msg+kMessageTerminator+kNullCharacter)
      })
   }
   else
   {
      socket.end(uMisc.Serialize(msg)+kMessageTerminator+kNullCharacter)
   } 
}

// ex.SendDataToSocket = function(socket,msg)
// {
   // if (uPrefs.uz)
   // {
      // if (msg.m_data == undefined)
         // msg.m_data = {}
      // if (msg.s == null)
         // msg.s = ""
      // zlib.deflate(JSON.stringify(msg.m_data), function(err, buffer)
      // {
         // msg.m_data = buffer.toString('base64')
         // socket.write(uMisc.Serialize(msg)+kMessageTerminator+kNullCharacter)
      // })
   // }
   // else
   // {
      // socket.write(uMisc.Serialize(msg)+kMessageTerminator+kNullCharacter)
   // } 
// }

ex.SendText = function(player, text)
{
   var msg = new ex.Message(ex.Names.kServerMessage,undefined,text)
   ex.SendMessage(player,msg)
}

ex.SendRefreshPlayerData = function(plr,players,gameId,turnStart)
{
   var msg = new ex.Message(ex.Names.kGameMessage, ex.Names.ksRefreshPlayerData, {})
   msg.m_data.g = gameId
   msg.m_data.pD = []
   if (turnStart != undefined)
      msg.m_data.turnStart = true
   else
      msg.m_data.turnStart = false
   for (var i = 0; i < players.length; i++)
   {
      var pl = players[i]
      msg.m_data.pD.push( {} )
      msg.m_data.pD[i].hp = pl.hp
      msg.m_data.pD[i].sh = pl.shield
      if (pl.item && pl.item.ability.abilityData.chargeable)
      {
         msg.m_data.pD[i].ch = pl.item.ability.chargeNow
      }
      msg.m_data.pD[i].mana = pl.mana
      msg.m_data.pD[i].lDsh = pl.lastDamageToShield
      msg.m_data.pD[i].ab = []
      for (var key in players[i].abilities)
      {
         msg.m_data.pD[i].ab.push( { t : pl.abilities[key].type }) //, charge : players[i].abilities[key].currentCharge } )
      }
      if (i === plr.number)
         msg.m_data.pD[i].en = false
      else
         msg.m_data.pD[i].en = true
     //msg.m_data.playerData[i].heroClass = players[i].heroClass
   }
   ex.SendMessage(plr, msg)   
}

ex.SendNoMoves = function(game,whoNumber,causedByPlayer)
{
   var msg = new ex.Message(ex.Names.kGameMessage, ex.Names.ksNoMoves, {})
   msg.m_data.g = game.id
   msg.m_data.wn = whoNumber
   msg.m_data.cbp = causedByPlayer
   for (var i = 0; i < game.players.length; i++)
   {
      ex.SendMessage(game.players[i], msg)
   }
}

// ex.SendHpNow = function(socket,hpObj,gameId)
// {
   // var msg = new ex.Message(ex.Names.kGameMessage, ex.Names.ksHpNow, {})
   // msg.m_data.gameId = gameId
   // msg.m_data.hp = hpObj
   // ex.SendMessage(socket, msg)
// }