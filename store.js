var uItem = require('./abilities/item.js')

var ex = module.exports


ex.GetItemHash = function(t,st)
{
   return t+"_"+st
}
//t - type, st - subtype, c - coins cost, cr - crystals cost

//1 health/damage cost = 1
//1 mana cost = 1

ex.StoreList = 
[
   { t : "pr", st : "vip", cr : 50 },
   //{ t : "pr", st : "maxL", cr : 25 },
   { t : "tpr", st : "dropCh", cr : 25, tm : 24 },
   
   // { t : "ad", st : "monL", cr : 5 }, 
   // { t : "it", st : uItem.ITEM_MEDIUM_SYMBOL_OF_SNOW, cr : 20, c : 600 },
   // { t : "it", st : uItem.ITEM_MINOR_LOCKET_OF_HEALTH, cr : 20, c : 600 },
   // { t : "it", st : uItem.ITEM_MEDIUM_FALLING_STAR, cr : 20, c : 600 },
   
   { t : "it", st : uItem.ITEM_MINOR_HEALTH_POTION, c : 6 },
   { t : "it", st : uItem.ITEM_MINOR_MANA_POTION, c : 6 },  
   { t : "it", st : uItem.ITEM_HEALTH_VIAL, c : 20, cr : 2 },
   { t : "it", st : uItem.ITEM_MANA_POTION, c : 30, cr : 3 },   
   { t : "it", st : uItem.ITEM_MINOR_DYNAMITE, c : 30, cr : 3 },
   { t : "it", st : uItem.ITEM_KNIFE, c : 30, cr : 3 },
   { t : "it", st : uItem.ITEM_SYMBOL_OF_MANA, c : 40, cr : 4 },
   { t : "it", st : uItem.ITEM_THROWING_AXE, c : 40, cr : 4 },
   { t : "it", st : uItem.ITEM_MINOR_SHIELD, c : 80, cr : 8 },
   
   { t : "it", st : uItem.ITEM_DYNAMITE, c : 50 }, 
   { t : "it", st : uItem.ITEM_AXE, c : 100 },
   { t : "it", st : uItem.ITEM_SWORD, c : 250 },
   { t : "it", st : uItem.ITEM_BOW, c : 300 },
   { t : "it", st : uItem.ITEM_SHIELD, c : 300 },
   { t : "it", st : uItem.ITEM_LONGBOW, c : 600 },
    
   // { t : "it", st : uItem.ITEM_SYMBOL_OF_SNOW, c : 10 },
   // { t : "it", st : uItem.ITEM_SYMBOL_OF_WARLOCK, c : 10 },
   // { t : "it", st : uItem.ITEM_FALLING_STAR, c : 10 },
   // { t : "it", st : uItem.ITEM_MINOR_SYMBOL_OF_MANA_DRAIN, c : 10 },
   // { t : "it", st : uItem.ITEM_SYMBOL_OF_MANA_DRAIN, c : 10 },
   // { t : "it", st : uItem.ITEM_LOCKET_OF_HEALTH, c : 10 },
   // { t : "it", st : uItem.ITEM_GREATER_SYMBOL_OF_MANA, c : 10 },
   // { t : "it", st : uItem.ITEM_POTION_OF_SHIELD, c : 10 },
   // { t : "it", st : uItem.ITEM_DYE, c : 10 },
   // { t : "it", st : uItem.ITEM_MOLOTOV_COCKTAIL, c : 10 },
   // { t : "it", st : uItem.ITEM_DYNAMITE_BUNDLE, c : 30 },
   // { t : "it", st : uItem.ITEM_POTION_OF_DARKNESS, c : 80, cr : 10 },   
]

ex.Store = {}

for (var i = 0; i < ex.StoreList.length; i++)
{
   var hash = ex.GetItemHash(ex.StoreList[i].t, ex.StoreList[i].st)
   ex.Store[hash] = ex.StoreList[i]
}

ex.Inventory = function(profileInventoryObject)
{
   this.l = profileInventoryObject
   for (var key in this.l)
   {
      if (this.l[key].eq)
      {
         this.eqIt = this.l[key]
         break
      }
   }
}

ex.Inventory.prototype.RemoveEquippedItem = function()
{
   if (this.eqIt)
      this.RemoveOneItem(this.eqIt.t, this.eqIt.st)
}

ex.Inventory.prototype.IsEquipped = function()
{
   return (this.eqIt != null)
}

ex.Inventory.prototype.EquipItem = function(type,subtype)
{
   var item = this.l[ex.GetItemHash(type,subtype)]
   if (item)
   {
      if (this.eqIt)
         this.eqIt.eq = false
      this.eqIt = this.l[ex.GetItemHash(type,subtype)]
      this.eqIt.eq = true
      return true
   }
   return false
}

ex.Inventory.prototype.UnequipItem = function()
{
   if (this.eqIt)
   {
      this.eqIt.eq = false
      this.eqIt = null
   }
}

ex.Inventory.prototype.AddOneItem = function(storeItem)
{
   if (storeItem.t === "ad")
      return storeItem
   var h = ex.GetItemHash(storeItem.t, storeItem.st)
   if (!this.l[h])
   {
      this.l[h] = { t : storeItem.t, st : storeItem.st, n : 1, eq : false }
   }
   else
   {
      this.l[h].n++
   }
   return this.l[h]
}

ex.Inventory.prototype.AddItemByType = function(t,st)
{
   var h = ex.GetItemHash(t, st)
   if (!this.l[h])
   {
      this.l[h] = { t : t, st : st, n : 1, eq : false }
   }
   else
   {
      this.l[h].n++
   }   
}

ex.Inventory.prototype.AddGameItem = function(st)
{
   this.AddItemByType("it",st)
}

ex.Inventory.prototype.RemoveOneItem = function(type, subType)
{
   var h = ex.GetItemHash(type,subType)
   if (this.l[h])
   {
      this.l[h].n--
      if (this.l[h].n <= 0)
      {
         delete this.l[h]
      }
   }
   if (this.eqIt && h === ex.GetItemHash(this.eqIt.t,this.eqIt.st) && this.eqIt.n <= 0)
   {
      this.eqIt = null
   }
}
