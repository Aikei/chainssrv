var uPrefs = require('../preferences')
var uDbg = require('../debug')
var uAbility = require('./ability')
var uConsts = require('../constants')

var AbilityShieldOfIce = function()
{
   uAbility.Ability.call(this,uAbility.ABILITY_SHIELD_OF_ICE)
   this.cost[uConsts.BALL_BLUE] = 2
   this.color = uConsts.BALL_BLUE
   this.shield = 3  
   
   this.GetAllTargets = function(targets, game)
   {
      uDbg.Log("Returning targets: ", targets)
      return targets
   }
   
   this.Use = function(allTargets, game, args)
   {
      uDbg.Log("Using ability Shield of Ice")
      this.BaseUse()  
      game.players[game.turn].shield += this.shield
      uDbg.Log("Shield now",game.players[game.turn].shield)
      game.SendRefreshPlayerData()
      uDbg.Log("Refresh player data sent")
   }
}
AbilityShieldOfIce.prototype = new uAbility.Ability(uAbility.ABILITY_SHIELD_OF_ICE)
AbilityShieldOfIce.prototype.constructor = uAbility.AbilityShieldOfIce
uAbility._allAbilityNames.push(uAbility.ABILITY_SHIELD_OF_ICE)

module.exports = AbilityShieldOfIce