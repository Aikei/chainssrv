//abilityBlueRage.js

var uPrefs = require('../preferences')
var uDbg = require('../debug')
var uAbility = require('./ability')
var uAbilityRage = require('./abilityRage')
var uConsts = require('../constants')
var uGlobalEf = require('./globalEffects')

var AbilityBlueRage = function()
{
   uAbilityRage.call(this, uAbility.ABILITY_BLUE_RAGE, uConsts.BALL_BLUE)
   this.color = uConsts.BALL_BLUE 
}

AbilityBlueRage.prototype = new uAbilityRage(uAbility.ABILITY_BLUE_RAGE, uConsts.BALL_BLUE)
AbilityBlueRage.prototype.constructor = AbilityBlueRage
//uAbility._allAbilityNames.push(uAbility.ABILITY_BLUE_RAGE)

module.exports = AbilityBlueRage