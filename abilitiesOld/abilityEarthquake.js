var uPrefs = require('../preferences')
var uDbg = require('../debug')
var uAbility = require('./ability')
var uConsts = require('../constants')
var uBaseAbilityDestroyOrRemoveBalls = require('./base/baseAbilityDestroyOrRemoveBalls')

var thisAbility = function()
{
   uBaseAbilityDestroyOrRemoveBalls.call(this,uAbility.ABILITY_EARTHQUAKE,false) //'false' means remove balls instead of destroying
   this.cost[uConsts.BALL_RED] = 6
   this.color = uConsts.BALL_RED    
}

thisAbility.prototype = new uBaseAbilityDestroyOrRemoveBalls(uAbility.ABILITY_EARTHQUAKE,false)

thisAbility.prototype.GetAllTargets = function(targets, game)
{
   uDbg.Log("Getting all targets")
   return this.GetTargetAllBalls(targets, game)
}   

thisAbility.prototype.constructor = thisAbility
uAbility._allAbilityNames.push(uAbility.ABILITY_EARTHQUAKE)

module.exports = thisAbility