//ef._effects

var ef = module.exports

var uDbg = require('../debug')
var uConsts = require('../constants')
var uPrefs = require('../preferences')
var uMisc = require('../misc')

ef.EFFECT_FROZEN = "Frozen"
ef.EFFECT_POISON = "Poison"
ef.EFFECT_BOMB = "Bomb"
ef.EFFECT_STONE_OF_HEALTH = "StoneOfHealth"
ef.EFFECT_GOBLIN_MINE = "GoblinMine"

//player effects
ef.EFFECT_INVULNERABILITY = "Invulnerability"

ef.Effect = function(type,owner,time)
{
   this.type = type
   this.time = time || 1
   this.startingTime = time
   this.tickInterval = 1
   this.tickIntervalCounter = 0
   this.host = null
   this.owner = null
   this.triggerOnSelect = false
   this.removeOnSelect = false
   this.triggerOnTrigger = false
   this.owner = owner || null
   this.owningEffect = true   
}

ef.Effect.prototype.Trigger = function(turn) { }

ef.Effect.prototype.OnTick = function() { }

ef.Effect.prototype.Tick = function()
{
   this.tickIntervalCounter++
   if (this.tickIntervalCounter === this.tickInterval)
   {
      this.tickIntervalCounter = 0         
      if (this.time > -1)
      {
         this.time--
         this.OnTick()
         if (this.time === 0)
         {
            uDbg.Log("_i","effect",this.type,"tick returning true")
            return true
         }
      }
      else
      {
         this.OnTick()
      }
   }
   return false
}

ef.Effect.prototype.TickBeforeMoving = function()
{
   return this.Tick()
}

ef.Effect.prototype.TickAfterMoving = function()
{
   return false
}

ef.EffectFrozen = function(time)
{
   ef.Effect.call(this,ef.EFFECT_FROZEN,undefined,time)
   this.owningEffect = false
   this.time = time || 2
}
ef.EffectFrozen.prototype = new ef.Effect(ef.EFFECT_FROZEN)
ef.EffectFrozen.prototype.constructor = ef.EffectFrozen

ef.EffectFrozen.prototype.CreateIdenticalEffect = function()
{
   return new ef.EffectFrozen(this.startingTime)
}

ef.EffectPoison = function(targetPlayer,damage,owner)
{
   ef.Effect.call(this,ef.EFFECT_POISON,owner,-1)
   this.tickInterval = 2
   this.tickIntervalCounter = 1
   this.targetPlayer = targetPlayer
   this.damage = damage
}

ef.EffectPoison.prototype = new ef.Effect(ef.EFFECT_POISON)
ef.EffectPoison.prototype.constructor = ef.EffectPoison

ef.EffectPoison.prototype.OnTick = function()
{
   this.targetPlayer.hp -= this.damage
}

ef.EffectPoison.prototype.CreateIdenticalEffect = function()
{
   return new ef.EffectPoison(this.targetPlayer,this.damage,this.owner)
}   

ef.EffectBomb = function(time,damage,targetPlayer,owner)
{
   ef.Effect.call(this,ef.EFFECT_BOMB,owner,time)
   this.damage = damage
   //this.time = time
   this.startingTime = time
   this.targetPlayer = targetPlayer
}
ef.EffectBomb.prototype = new ef.Effect(ef.EFFECT_BOMB)
ef.EffectBomb.prototype.constructor = ef.EffectBomb

ef.EffectBomb.prototype.OnTick = function()
{
   if (this.time === 0)
   {
      this.targetPlayer.hp -= this.damage
      this.host.RemoveThisBall()
   }
}

ef.EffectBomb.prototype.CreateIdenticalEffect = function()
{
   return new ef.EffectBomb(this.startingTime,this.damage,this.targetPlayer,this.owner)
}

ef.EffectStoneOfHealth = function(targetPlayer,heal,owner)
{
   ef.Effect.call(this,ef.EFFECT_STONE_OF_HEALTH,owner,-1)
   this.tickInterval = 2
   this.tickIntervalCounter = 1
   this.targetPlayer = targetPlayer
   this.heal = heal   
}
ef.EffectStoneOfHealth.prototype = new ef.Effect(ef.EFFECT_STONE_OF_HEALTH)
ef.EffectStoneOfHealth.prototype.constructor = ef.EffectStoneOfHealth

ef.EffectStoneOfHealth.prototype.OnTick = function()
{
   this.targetPlayer.hp += this.heal
}

ef.EffectStoneOfHealth.prototype.CreateIdenticalEffect = function()
{
   return new ef.EffectStoneOfHealth(this.targetPlayer,this.heal,this.owner)
}   

ef.EffectGoblinMine = function(targetPlayer,damage,owner)
{
   ef.Effect.call(this,ef.EFFECT_GOBLIN_MINE,owner,-1)
   this.tickInterval = 2
   this.tickIntervalCounter = 1
   this.targetPlayer = targetPlayer
   this.damage = damage
   this.triggerOnSelect = true
   this.removeOnTrigger = true     
}
ef.EffectGoblinMine.prototype = new ef.Effect(ef.EFFECT_GOBLIN_MINE)
ef.EffectGoblinMine.prototype.constructor = ef.EffectGoblinMine

ef.EffectGoblinMine.prototype.Trigger = function(turn)
{
   uDbg.Log("Triggering effect Goblin Mine")
   if (turn === this.targetPlayer.number)
   {
      uDbg.Log("Affecting effect Goblin Mine")
      this.targetPlayer.hp -= this.damage
      return true
   }
   return false
}

ef.EffectGoblinMine.prototype.CreateIdenticalEffect = function()
{
   return new ef.EffectGoblinMine(this.targetPlayer,this.damage,this.owner)
}    

//player effects
ef.EffectInvulnerability = function(owner)
{
   ef.Effect.call(this,ef.EFFECT_INVULNERABILITY,owner,2)
   this.startingTime = this.time     
}
ef.EffectInvulnerability.prototype = new ef.Effect(ef.EFFECT_INVULNERABILITY)
ef.EffectInvulnerability.prototype.constructor = ef.EffectInvulnerability

ef.EffectInvulnerability.prototype.CreateIdenticalEffect = function()
{
   return new ef.EffectInvulnerability(this.owner)
}
