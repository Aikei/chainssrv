var uPrefs = require('../preferences')
var uDbg = require('../debug')
var uAbility = require('./ability')
var uConsts = require('../constants')
var uMisc = require('../misc')

var AbilityPowerOfAbyss = function()
{
   uAbility.Ability.call(this,uAbility.ABILITY_POWER_OF_ABYSS)
   this.cost[uConsts.BALL_PURPLE] = 0
   this.color = uConsts.BALL_PURPLE
   this.selfDamage = 2
   this.additionalMana = 2
   
   this.GetAllTargets = function(targets, game)
   {
      uDbg.Log("Returning targets: ", targets)
      return []
   }
   
   this.Use = function(allTargets, game, args)
   {
      this.BaseUse()
      var numberOfActives = 0
      var ab = null
      for (var key in this.owner.abilities)
      {
         ab = this.owner.abilities[key]
         if (!ab.passive && !ab.EnoughMana())
         {
            numberOfActives++
         }
      }
      if (numberOfActives > 0)
      {
         var dice = uMisc.Random(0,numberOfActives)
         var i = 0
         for (var key in this.owner.abilities)
         {
            ab = this.owner.abilities[key]
            if (!ab.passive && !ab.EnoughMana())
            {
               if (i === dice)
                  break
               i++
            }
         }
         this.owner.mana[ab.color] += this.additionalMana
         if (this.owner.mana[ab.color] > uPrefs.maxMana)
            this.owner.mana[ab.color] = uPrefs.maxMana         
      }
      this.owner.hp -= this.selfDamage
      //game.SendRefreshPlayerData()
   }

}
AbilityPowerOfAbyss.prototype = new uAbility.Ability(uAbility.ABILITY_POWER_OF_ABYSS)
AbilityPowerOfAbyss.prototype.constructor = AbilityPowerOfAbyss
uAbility._allAbilityNames.push(uAbility.ABILITY_POWER_OF_ABYSS)

module.exports = AbilityPowerOfAbyss