var uPrefs = require('../preferences')
var uDbg = require('../debug')
var uAbility = require('./ability')
var uConsts = require('../constants')

var AbilityHeal = function()
{
   uAbility.Ability.call(this,uAbility.ABILITY_HEAL)
   this.cost[uConsts.BALL_GREEN] = 6
   this.color = uConsts.BALL_GREEN
   this.targeting = 4
   this.heal = 7
   this.GetAllTargets = function(targets, game)
   {
      uDbg.Log("Returning targets: ", targets)
      return targets
   }
   
   this.Use = function(allTargets, game, args)
   {
      this.BaseUse()  
      game.ActuallyHealSelf(this.heal)
      game.SendRefreshPlayerData()
   }
}
AbilityHeal.prototype = new uAbility.Ability(uAbility.ABILITY_HEAL)
AbilityHeal.prototype.constructor = AbilityHeal
uAbility._allAbilityNames.push(uAbility.ABILITY_HEAL)

module.exports = AbilityHeal