//baseAbilityApplyGlobalEffect.js

var uPrefs = require('../../preferences')
var uDbg = require('../../debug')
var uAbility = require('../ability')
var uConsts = require('../../constants')
var uGlobalEffects = require('../globalEffects')
var uMisc = require('../../misc')


var BaseAbilityApplyGlobalEffect = function(abilityType,effectType)
{
   uAbility.Ability.call(this,abilityType)
   this.effectType = effectType   
   this.effectArgs = []
}

BaseAbilityApplyGlobalEffect.prototype = new uAbility.Ability(uAbility.ABILITY_NONE)

BaseAbilityApplyGlobalEffect.prototype.GetAllTargets = function(targets, game)
{
   return []
}

BaseAbilityApplyGlobalEffect.prototype.SetEffectArguments = function()
{
   this.effectArgs = Array.prototype.slice.call(arguments)
}

BaseAbilityApplyGlobalEffect.prototype.Use = function(allTargets, game, args)
{
   this.BaseUse()
   game.AddGlobalEffect(uMisc.Construct(uGlobalEffects["Effect"+this.effectType],this.effectArgs))
}   

BaseAbilityApplyGlobalEffect.prototype.constructor = BaseAbilityApplyGlobalEffect

module.exports = BaseAbilityApplyGlobalEffect