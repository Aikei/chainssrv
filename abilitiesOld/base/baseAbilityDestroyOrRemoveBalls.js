var uPrefs = require('../../preferences')
var uDbg = require('../../debug')
var uAbility = require('../ability')
var uConsts = require('../../constants')

var BaseAbilityDestroyOrRemoveBalls = function(abilityType, destroyBalls)
{
   uAbility.Ability.call(this,abilityType)
   if (destroyBalls === undefined)
      this.destroyBalls = true
   else
      this.destroyBalls = destroyBalls
   this.giveManaOnDestroy = true
}

BaseAbilityDestroyOrRemoveBalls.prototype = new uAbility.Ability(uAbility.ABILITY_NONE)

BaseAbilityDestroyOrRemoveBalls.prototype.GetAllTargets = function(targets, game)
{
   return this.GetThisTargetOnly(targets, game)
}

BaseAbilityDestroyOrRemoveBalls.prototype.Use = function(allTargets, game, args)
{
   this.BaseUse()
   if (this.destroyBalls === true)
   {
      game.DamageEnemy(allTargets)
      if (this.giveManaOnDestroy)
         game.ChargeAbilities(allTargets)      
   }
   game.SendRefreshPlayerData()
   game.JustRemoveBalls(allTargets)
   game.MoveBallsAfterRemoving()   
}   

BaseAbilityDestroyOrRemoveBalls.prototype.constructor = BaseAbilityDestroyOrRemoveBalls

BaseAbilityDestroyOrRemoveBalls.prototype._canEliminateNoMoves = true

module.exports = BaseAbilityDestroyOrRemoveBalls