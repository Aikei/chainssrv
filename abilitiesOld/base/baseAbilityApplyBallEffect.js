var uPrefs = require('../../preferences')
var uDbg = require('../../debug')
var uAbility = require('../ability')
var uConsts = require('../../constants')
var uEffects = require('../effects')
var uMisc = require('../../misc')

var BaseAbilityApplyBallEffect = function(abilityType,effectType)
{
   uAbility.Ability.call(this,abilityType)
   this.effectType = effectType   
   this.effectArgs = []
}

BaseAbilityApplyBallEffect.prototype = new uAbility.Ability(uAbility.ABILITY_NONE)

BaseAbilityApplyBallEffect.prototype.GetAllTargets = function(targets, game)
{
   uDbg.Log("GetAllTargets executes, targets length: ",targets.length)
   var allTargets = []
   if (this.targeting === uAbility.ABILITY_TARGETING_BALL)
   {
      allTargets = this.GetThisTargetOnly(targets, game)
   }
   else if (this.targeting === uAbility.ABILITY_TARGETING_SELF)
   {
      allTargets.push(this.owner)      
   }
   else if (this.targeting === uAbility.ABILITY_TARGETING_ENEMY)
   {
      allTargets.push(game.GetEnemyOfPayerNumber(owner.number))
   }
   return allTargets
}

BaseAbilityApplyBallEffect.prototype.SetEffectArguments = function()
{
   this.effectArgs = Array.prototype.slice.call(arguments)
}

BaseAbilityApplyBallEffect.prototype.Use = function(allTargets, game, args)
{
   this.BaseUse()  
   for (var i = 0; i < allTargets.length; i++)
   {     
      var effect = uMisc.Construct(uEffects["Effect"+this.effectType],this.effectArgs)
      allTargets[i].AddEffect(effect)
   }
}   

BaseAbilityApplyBallEffect.prototype.constructor = BaseAbilityApplyBallEffect

module.exports = BaseAbilityApplyBallEffect