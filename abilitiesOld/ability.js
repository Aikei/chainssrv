//ability.js

var uPrefs = require('../preferences')
var uDbg = require('../debug')
var uConsts = require('../constants')

var ex = module.exports

ex.ABILITY_NONE = "None"
ex.ABILITY_FREEZE = "Freeze"
ex.ABILITY_FROST = "Frost"
ex.ABILITY_FIRE = "Fire"
ex.ABILITY_TURN = "Turn"
ex.ABILITY_FLAME = "Flame"
ex.ABILITY_HEAL = "Heal"
ex.ABILITY_POISON = "Poison"
ex.ABILITY_BOMB = "Bomb"
ex.ABILITY_METEORITE_RAIN = "MeteoriteRain"
ex.ABILITY_SHATTER = "Shatter"
ex.ABILITY_SEE_NEXT = "SeeNext"
ex.ABILITY_EARTHQUAKE = "Earthquake"
ex.ABILITY_STONE_OF_HEALTH = "StoneOfHealth"
ex.ABILITY_SHIELD_OF_ICE = "ShieldOfIce"
ex.ABILITY_DARK_MADNESS = "DarkMadness"
ex.ABILITY_CHAIN_LIGHTNING = "ChainLightning"
ex.ABILITY_GOBLIN_MINE = "GoblinMine"
ex.ABILITY_FREEZING_RAIN = "FreezingRain"
ex.ABILITY_GREEN_RAGE = "GreenRage"
ex.ABILITY_RED_RAGE = "RedRage"
ex.ABILITY_BLUE_RAGE = "BlueRage"
ex.ABILITY_PURPLE_RAGE = "PurpleRage"
ex.ABILITY_POWER_OF_ABYSS = "PowerOfAbyss"
ex.ABILITY_INFECTION = "Infection"
ex.ABILITY_MATCH_THREE = "MatchThree"
ex.ABILITY_REMOVE_STONE = "RemoveStone"
ex.ABILITY_POWER_DRAIN = "PowerDrain"
ex.ABILITY_ICE_BLOCK = "IceBlock"

//targeting
ex.ABILITY_TARGETING_BALL = 0
ex.ABILITY_TARGETING_SELF = 1
ex.ABILITY_TARGETING_ENEMY = 2

ex._allAbilityNames = []

ex.Ability = function(type)
{
   this.type = type
   this.subType = "none"
   this.maxTargets = 1
   this.minTargets = 0
   this.minAllTargets = 0
   this.targeting = 1
   this.cost = []
   this.owner = null
   this.hiddenSelect = false
   this.passive = false
   this.color = -1
   this.used = false
   this.targeting = ex.ABILITY_TARGETING_BALL
   
   for (var i = 0; i < uConsts.NUMBER_OF_BALL_TYPES; i++)
   {
      this.cost.push(0)
   }
}

ex.Ability.prototype._canEliminateNoMoves = false

ex.Ability.prototype.CanEliminateNoMoves = function()
{
   uDbg.Log("_i","returning _canEliminateNoMoves:",this._canEliminateNoMoves)
   return this._canEliminateNoMoves
}

ex.Ability.prototype.BaseUse = function()
{
   this.DeductMana()
   this.used = true
}

ex.Ability.prototype.DeductMana = function()
{
   for(var i = 0; i < uConsts.NUMBER_OF_BALL_TYPES; i++)
   {
      if (this.cost[i] > 0)
         this.owner.mana[i] = 0
      //this.owner.mana[i] -= this.cost[i]
   }      
}

ex.Ability.prototype.EnoughMana = function()
{
   for(var i = 0; i < uConsts.NUMBER_OF_BALL_TYPES; i++)
   {
      uDbg.Log("cost: ",this.cost[i])
      uDbg.Log("mana: ",this.owner.mana[i])
      if (this.cost[i] > this.owner.mana[i])
      {
         uDbg.Log("Not enough mana, returning false!")
         return false
      }
   }
   return true      
}

ex.Ability.prototype.CanBeUsed = function(player)
{
   if (this.used)
   {
      uDbg.Log("_ii","Ability",this.type,"has already been used this turn, CanBeUsed() returning false!")
      return false
   }
   player = player || this.owner
   for(var i = 0; i < uConsts.NUMBER_OF_BALL_TYPES; i++)
   {
      uDbg.Log("cost: ",this.cost[i])
      uDbg.Log("mana: ",player.mana[i])
      if (this.cost[i] > player.mana[i])
      {
         uDbg.Log("_ii","Not enough mana for ability",this.ability,", CanBeUsed() returning false!")
         return false
      }
   }
   return true
}

ex.Ability.prototype.Charge = function(by)
{
   uDbg.Log("Charging ability",this.type)
   uDbg.Log("Ability color is",this.color)
   this.owner.mana[this.color] += by
   if (this.owner.mana[this.color] > this.cost[this.color])
   {
      this.owner.mana[this.color] = this.cost[this.color]
   }
}

ex.Ability.prototype.CanBeUsedWithTheseTargetsAndArguments = function(player,game,targets,allTargets,args)
{
   if (!this.CanBeUsed(this.owner) || targets.length > this.maxTargets || targets.length < this.minTargets || allTargets.length < this.minAllTargets)
   {
      uDbg.Log("_ii","CanBeUsedWithTheseTargetsAndArguments first conditional returned undefined")
      uDbg.Log("_ii","targets length: ",targets.length)
      uDbg.Log("_ii","max targets: ",this.maxTargets)
      uDbg.Log("_ii","min targets: ",this.minTargets)
      uDbg.Log("_ii","allTargets length: ",allTargets.length)
      uDbg.Log("_ii","minAllTargets: ",this.minAllTargets)
      return undefined
   }
   uDbg.Log("ability type:",this.type)
   uDbg.Log("before GetAllTargets, targets length: ",targets.length)
   uDbg.Log("before GetAllTargets, all targets length: ",allTargets.length)
   var serverTargets = this.GetAllTargets(targets,game)
   uDbg.Log("server targets: ",serverTargets)
   if (!CheckTargetsForEquality(allTargets,serverTargets))
   {
      uDbg.Log("_ii","CanBeUsedWithTheseTargetsAndArguments second conditional returned undefined")
      return undefined
   }
   return serverTargets
}

ex.Ability.prototype.GetTargetNeighbours = function(targets, game)
{
   uDbg.Log("Getting all targets")
   var allTargets = []
   for (var i = 0; i < targets.length; i++)
   {
      var thisBall = game.field[targets[i].x][targets[i].y]      
      var v = game.GetNeighbours(thisBall)
      v.push(thisBall)      
      allTargets = allTargets.concat(v)
   }
   uDbg.Log("Returning all targets: ",allTargets)
   return allTargets
}

ex.Ability.prototype.GetTargetCrossNeighbours = function(targets, game)
{
   uDbg.Log("Getting all targets")
   var allTargets = []
   for (var i = 0; i < targets.length; i++)
   {
      var thisBall = game.field[targets[i].x][targets[i].y]      
      var v = game.GetCrossNeighbours(thisBall)
      v.push(thisBall)      
      allTargets = allTargets.concat(v)
   }
   uDbg.Log("Returning all targets: ",allTargets)
   return allTargets
}

ex.Ability.prototype.GetTargetChain = function(targets, game)
{
   uDbg.Log("Getting all targets")
   var allTargets = []
   for (var i = 0; i < targets.length; i++)
   {
      var thisBall = game.field[targets[i].x][targets[i].y]            
      var v = game.GetChain(thisBall)      
      allTargets = allTargets.concat(v)
   }
   uDbg.Log("Returning all targets: ",allTargets)
   return allTargets
}

ex.Ability.prototype.GetThisTargetOnly = function(targets, game)
{
   uDbg.Log("_i","Getting all targets for ability",this.type)
   var allTargets = []
   for (var i = 0; i < targets.length; i++)
   {
      //uDbg.Log("_ii","target x: ",targets[i].x,"target y: ",targets[i].y)
      var thisBall = game.field[targets[i].x][targets[i].y]
      //uDbg.Log("_ii","thisBall.x:",thisBall.x,"thisBall.y:",thisBall.y);
      allTargets.push(thisBall)
   }
   //uDbg.Log("Returning all targets: ",allTargets)
   uDbg.Log("_i","returning all targets, length:",allTargets.length)   
   return allTargets   
}

ex.Ability.prototype.GetTargetAllBalls = function(targets, game)
{
   var allBalls = []
   for (var i = 0; i < game.field.length; i++)
   {
      allBalls = allBalls.concat(game.field[i])
   }
   uDbg.Log("_ii","all balls length: ",allBalls.length)
   return allBalls
}

ex.Ability.prototype.GetTargetAllBallsOfSpecificColor = function(color, game)
{
   var allBalls = []
   for (var i = 0; i < game.field.length; i++)
   {
      for (var j = 0; j < game.field[i].length; j++)
      {
         if (game.field[i][j].type === color)
            allBalls.push(game.field[i][j])
      }
   }
   return allBalls   
}

ex.Ability.prototype.GetAllTargets = function(targets, game)
{
   return this.GetThisTargetOnly(targets, game)
}

function CheckTargetsForEquality(playerAllTargets,serverAllTargets)
{
   uDbg.Log("_i","Checking targets for equality")
   if (playerAllTargets.length !== serverAllTargets.length)
   {
      uDbg.Log("_ii","Targets lengths are not equal!")
      uDbg.Log("_ii","Client's targets length:",playerAllTargets.length,", server's targets length:",serverAllTargets.length)
      return false
   }
   uDbg.Log("_i","Lengths match")
   for (var i = 0; i < playerAllTargets.length; i++)
   {
      var found = false
      uDbg.Log("_i","Checking player target number",i)
      for (var j = 0; j < serverAllTargets.length; j++)
      { 
         uDbg.Log("_i","For equality with server target number",j)
         uDbg.Log("_i","player target type:",playerAllTargets[i].type)
         if (playerAllTargets[i].type === "b") //b = ball
         {
            uDbg.Log("_i","Target type is 'b' - ball")
            if (playerAllTargets[i].x === serverAllTargets[j].x && playerAllTargets[i].y === serverAllTargets[j].y)
            {
               found = true
               break
            }
         }
         else if (playerAllTargets[i].type === "p")  //p = player
         {
            uDbg.Log("_i","Target type is 'p' - player")
            uDbg.Log("_i","Player target number is",playerAllTargets[i].number)
            uDbg.Log("_i","Server target number is",serverAllTargets[j].number)
            if (playerAllTargets[i].number === serverAllTargets[j].number)
            {
               found = true
               break
            }
         }
      }
      if (!found)
      {
         uDbg.Log("_i","Equality not found, returning false")
         return false
      }
   }
   uDbg.Log("_i","Equality found, returning true")
   return true
}