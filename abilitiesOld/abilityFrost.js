var uPrefs = require('../preferences')
var uDbg = require('../debug')
var uAbility = require('./ability')
var uConsts = require('../constants')

var AbilityFrost = function()
{
   uAbility.Ability.call(this,uAbility.ABILITY_FROST)
   this.cost[uConsts.BALL_BLUE] = 8
   this.color = uConsts.BALL_BLUE
   this.targeting = 3
   this.damageMin = 8
   this.damageMax = 20
   this.GetAllTargets = function(targets, game)
   {
      uDbg.Log("Returning targets: ", targets)
      return targets
   }
   
   this.Use = function(allTargets, game, args)
   {
      this.BaseUse()  
      game.ActuallyDamageEnemy(uMisc.Random(this.damageMin,this.damageMax+1))
      game.SendRefreshPlayerData()
   }

}
AbilityFrost.prototype = new uAbility.Ability(uAbility.ABILITY_FROST)
AbilityFrost.prototype.constructor = AbilityFrost
//uAbility._allAbilityNames.push(uAbility.ABILITY_FROST)

module.exports = AbilityFrost