var uPrefs = require('../preferences')
var uDbg = require('../debug')
var uAbility = require('./ability')
var uConsts = require('../constants')
var uEffects = require('./effects')
var uBaseAbilityApplyBallEffect = require('./base/baseAbilityApplyBallEffect')

var AbilityPoison = function()
{
   uBaseAbilityApplyBallEffect.call(this,uAbility.ABILITY_POISON,uEffects.EFFECT_POISON)
   this.cost[uConsts.BALL_GREEN] = 3
   this.color = uConsts.BALL_GREEN
   this.damage = 2   
}

AbilityPoison.prototype = new uBaseAbilityApplyBallEffect(uAbility.ABILITY_POISON)

AbilityPoison.prototype.Use = function(allTargets, game, args)
{
   this.SetEffectArguments(game.GetEnemyOfMovingPlayer(),this.damage,this.owner)
   uBaseAbilityApplyBallEffect.prototype.Use.call(this,allTargets,game,args)
}

AbilityPoison.prototype.constructor = AbilityPoison
uAbility._allAbilityNames.push(uAbility.ABILITY_POISON)

module.exports = AbilityPoison