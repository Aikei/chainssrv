var uPrefs = require('../preferences')
var uDbg = require('../debug')
var uAbility = require('./ability')
var uConsts = require('../constants')

var AbilityInfection = function()
{
   uAbility.Ability.call(this,uAbility.ABILITY_INFECTION)
   this.cost[uConsts.BALL_RED] = 3
   this.color = uConsts.BALL_RED
   this.number = 1
   this.maxTargets = uPrefs.fieldWidth*uPrefs.fieldHeight
   
   this.GetAllTargets = function(targets, game)
   {
      uDbg.Log("Getting all targets")
      //this.maxTargets = targets.length
      this.targets = []
      var allTargets = []
      for (var i = 0; i < targets.length; i++)
      {
         var thisBall = game.field[targets[i].x][targets[i].y]
         this.targets.push(thisBall)
         var neighbours = game.GetNeighbours(thisBall)
         for (var j = 0; j < neighbours.length; j++)
         {
            if (neighbours[j].HasOwningEffect())
            {
               neighbours.splice(j,1)
               j--
            }
         }
         if (neighbours.length > 0)
         {
            var n = 0
            if (neighbours.length > 1)
            {
               n = game.GetDice() % neighbours.length
            }
            allTargets.push(neighbours[n])
         }
         // else
         // {
            // allTargets.push(null)
         // }
      }
      uDbg.Log("Returning all targets: ",allTargets)
      return allTargets
   }
   
   this.Use = function(allTargets, game, args)
   {
      this.BaseUse()
      uDbg.Log("_i","targets length: ",this.targets.length)
      uDbg.Log("_i","all targets length: ",allTargets.length)
      uDbg.Log("_i","target 0: {",this.targets[0].x,this.targets[0].y,"}")
      uDbg.Log("_i","all target 0: {",allTargets[0].x,allTargets[0].y,"}")
      for (var i = 0; i < allTargets.length; i++)
      {
         for (var e = 0; e < this.targets[i].effects.length; e++)
         {
            //uDbg.Log("_i","effect owner number: ",this.targets[i].effects[e].owner.number)
            //uDbg.Log("_i","ability owner number: ",this.owner.number)
            var originalEffect = this.targets[i].effects[e]
            if (originalEffect.owner != null && originalEffect.owner.number === this.owner.number)
            {
               var newEffect = originalEffect.CreateIdenticalEffect()
               uDbg.Log("_ii","Correct owner, created effect",newEffect.type)
               allTargets[i].AddEffect(newEffect)
            }
         }
      }
      game.SendRefreshPlayerData()
      // game.DamageEnemy(allTargets)
      // game.ChargeAbilities(allTargets)
      // game.SendRefreshPlayerData()
      // game.JustRemoveBalls(allTargets)
      // game.MoveBallsAfterRemoving()
   }   
}
AbilityInfection.prototype = new uAbility.Ability(uAbility.ABILITY_INFECTION)
AbilityInfection.prototype.constructor = AbilityInfection
uAbility._allAbilityNames.push(uAbility.ABILITY_INFECTION)

module.exports = AbilityInfection