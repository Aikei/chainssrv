//AbilityIceBlock

var uPrefs = require('../preferences')
var uDbg = require('../debug')
var uAbility = require('./ability')
var uConsts = require('../constants')
var uEffects = require('./effects')
var uBaseAbilityApplyBallEffect = require('./base/baseAbilityApplyBallEffect')

var thisAbility = function()
{
   uBaseAbilityApplyBallEffect.call(this,uAbility.ABILITY_ICE_BLOCK,uEffects.EFFECT_ICE_BLOCK)
   this.cost[uConsts.BALL_BLUE] = 3
   this.color = uConsts.BALL_BLUE
   this.targeting = uAbility.ABILITY_TARGETING_SELF
}

thisAbility.prototype = new uBaseAbilityApplyBallEffect(uAbility.ABILITY_ICE_BLOCK)

thisAbility.prototype.Use = function(allTargets, game, args)
{
   this.SetEffectArguments(this.owner)
   uBaseAbilityApplyBallEffect.prototype.Use.call(this,allTargets,game,args)
   game.NewTurn()
}

thisAbility.prototype._canEliminateNoMoves = true

thisAbility.prototype.constructor = thisAbility
uAbility._allAbilityNames.push(uAbility.ABILITY_ICE_BLOCK)

module.exports = thisAbility