var uPrefs = require('../preferences')
var uDbg = require('../debug')
var uAbility = require('./ability')
var uConsts = require('../constants')
var uEffects = require('./effects')
var uBaseAbilityApplyBallEffect = require('./base/baseAbilityApplyBallEffect')

var AbilityGoblinMine = function()
{
   uBaseAbilityApplyBallEffect.call(this,uAbility.ABILITY_GOBLIN_MINE,uEffects.EFFECT_GOBLIN_MINE)
   this.cost[uConsts.BALL_RED] = 4
   this.color = uConsts.BALL_RED
   this.damage = 10
   this.hiddenSelect = true   
}

AbilityGoblinMine.prototype = new uBaseAbilityApplyBallEffect(uAbility.ABILITY_GOBLIN_MINE)

AbilityGoblinMine.prototype.Use = function(allTargets, game, args)
{
   this.SetEffectArguments(game.GetEnemyOfMovingPlayer(),this.damage,this.owner)
   uBaseAbilityApplyBallEffect.prototype.Use.call(this,allTargets,game,args)
}

AbilityGoblinMine.prototype.constructor = AbilityGoblinMine
uAbility._allAbilityNames.push(uAbility.ABILITY_GOBLIN_MINE)

module.exports = AbilityGoblinMine