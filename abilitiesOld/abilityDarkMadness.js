//ability.js

var uPrefs = require('../preferences')
var uDbg = require('../debug')
var uAbility = require('./ability')
var uConsts = require('../constants')
var uBaseAbilityDestroyOrRemoveBalls = require('./base/baseAbilityDestroyOrRemoveBalls')

var thisAbility = function()
{
   uBaseAbilityDestroyOrRemoveBalls.call(this,uAbility.ABILITY_DARK_MADNESS)
   this.cost[uConsts.BALL_PURPLE] = 6
   this.color = uConsts.BALL_PURPLE
   this.giveManaOnDestroy = false   
}

thisAbility.prototype = new uBaseAbilityDestroyOrRemoveBalls(uAbility.ABILITY_DARK_MADNESS)

thisAbility.prototype.GetAllTargets = function(targets, game)
{
   uDbg.Log("Getting all targets")
   return this.GetTargetAllBallsOfSpecificColor(this.color,game)
}   

thisAbility.prototype.constructor = thisAbility
uAbility._allAbilityNames.push(uAbility.ABILITY_DARK_MADNESS)

module.exports = thisAbility