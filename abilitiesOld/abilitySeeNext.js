var uPrefs = require('../preferences')
var uDbg = require('../debug')
var uAbility = require('./ability')
var uConsts = require('../constants')

var AbilitySeeNext = function()
{
   uAbility.Ability.call(this,uAbility.ABILITY_SEE_NEXT)
   this.cost[uConsts.BALL_PURPLE] = 4
   this.color = uConsts.BALL_PURPLE
   this.GetAllTargets = function(targets, game)
   {
      uDbg.Log("Getting all targets")
      var allTargets = []
      uDbg.Log("Returning all targets: ",allTargets)
      return allTargets
   }
   
   this.Use = function(allTargets, game, args)
   {
      this.BaseUse()  
      for (var i = 0; i < 5; i++)
         game.entropy.pop()
   }

   this.CanBeUsedWithTheseTargetsAndArguments = function(player,game,targets,allTargets,args)
   {
      var serverTargets = this.GetAllTargets(targets,game)
      return serverTargets
   }
}
AbilitySeeNext.prototype = new uAbility.Ability(uAbility.ABILITY_SEE_NEXT)
AbilitySeeNext.prototype.constructor = AbilitySeeNext
//uAbility._allAbilityNames.push(uAbility.ABILITY_SEE_NEXT)

module.exports = AbilitySeeNext