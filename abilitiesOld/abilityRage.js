//ability.js

var uPrefs = require('../preferences')
var uDbg = require('../debug')
var uAbility = require('./ability')
var uConsts = require('../constants')
var uGlobalEf = require('./globalEffects')

var AbilityRage = function(abilityType,ballType)
{
   uAbility.Ability.call(this,abilityType)
   this.type = abilityType
   this.ballType = ballType
   this.cost[ballType] = 0
   this.passive = true
   this.subType = "rage2"
   this.mult1 = 0.25
   this.mult2 = 1.5
   this.manaMult = 0.3
   
   this.GetAllTargets = function(targets, game)
   {
      uDbg.Log("Getting all targets, ability",this.type)
      var allTargets = []
      uDbg.Log("Returning all targets: ",allTargets)
      return allTargets
   }
   
   this.Use = function(allTargets, game, args)
   {
      game.AddGlobalEffect(new uGlobalEf.EffectRage(this.ballType, this.owner, this.mult1, this.mult2, this.manaMult))
      // if (args === undefined)
         // game.AddGlobalEffect(new uGlobalEf.EffectRage(this.ballType,this.owner,this.mult1,this.mult2))
      // else
         // game.AddGlobalEffect(new uGlobalEf.EffectRage(-1,this.owner,this.mult1,this.mult2))
   }   
}

AbilityRage.prototype = new uAbility.Ability("Rage")
AbilityRage.prototype.constructor = module.exports.AbilityRage
//uAbility._allAbilityNames.push(uAbility.ABILITY_GREEN_RAGE)

module.exports = AbilityRage