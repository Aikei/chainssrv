var uPrefs = require('../preferences')
var uDbg = require('../debug')
var uAbility = require('./ability')
var uConsts = require('../constants')
var uBaseAbilityDestroyOrRemoveBalls = require('./base/baseAbilityDestroyOrRemoveBalls')

var thisAbility = function()
{
   uBaseAbilityDestroyOrRemoveBalls.call(this,uAbility.ABILITY_SHATTER,false) //false means remove instead of destroying
   this.cost[uConsts.BALL_BLUE] = 4
   this.color = uConsts.BALL_BLUE    
}

thisAbility.prototype = new uBaseAbilityDestroyOrRemoveBalls(uAbility.ABILITY_SHATTER,false)

thisAbility.prototype.GetAllTargets = function(targets, game)
{
   uDbg.Log("Getting all targets")
   var allTargets = []
   for (var i = 0; i < targets.length; i++)
   {
      var thisBall = game.field[targets[i].x][targets[i].y]
      var v = game.GetBallsForEliminateRows(thisBall)
      allTargets = allTargets.concat(v)
   }
   uDbg.Log("Returning all targets: ",allTargets)
   return allTargets
}   

thisAbility.prototype.constructor = thisAbility
uAbility._allAbilityNames.push(uAbility.ABILITY_SHATTER)

module.exports = thisAbility


// var AbilityShatter = function()
// {
   // uAbility.Ability.call(this,uAbility.ABILITY_SHATTER)
   // this.cost[uConsts.BALL_BLUE] = 4
   // this.color = uConsts.BALL_BLUE
   // this.GetAllTargets = function(targets, game)
   // {
      // uDbg.Log("Getting all targets")
      // var allTargets = []
      // for (var i = 0; i < targets.length; i++)
      // {
         // var thisBall = game.field[targets[i].x][targets[i].y]
         // var v = game.GetBallsForEliminateRows(thisBall)
         // allTargets = allTargets.concat(v)
      // }
      // uDbg.Log("Returning all targets: ",allTargets)
      // return allTargets
   // }
   
   // this.Use = function(allTargets, game, args)
   // {
      // this.BaseUse()  
      // game.JustRemoveBalls(allTargets)
      // game.MoveBallsAfterRemoving()
   // }   
// }
// AbilityShatter.prototype = new uAbility.Ability(uAbility.ABILITY_SHATTER)
// AbilityShatter.prototype.constructor = AbilityShatter
// uAbility._allAbilityNames.push(uAbility.ABILITY_SHATTER)

// module.exports = AbilityShatter