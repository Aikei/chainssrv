var uPrefs = require('../preferences')
var uDbg = require('../debug')
var uAbility = require('./ability')
var uConsts = require('../constants')

var AbilityFlame = function()
{
   uAbility.Ability.call(this,uAbility.ABILITY_FLAME)
   this.cost[uConsts.BALL_RED] = 8
   this.color = uConsts.BALL_RED
   this.targeting = 3
   this.damage = 14
   this.GetAllTargets = function(targets, game)
   {
      uDbg.Log("Returning targets: ", targets)
      return targets
   }
   
   this.Use = function(allTargets, game, args)
   {
      this.BaseUse() 
      game.ActuallyDamageEnemy(this.damage)
      game.SendRefreshPlayerData()
   }

}
AbilityFlame.prototype = new uAbility.Ability(uAbility.ABILITY_FLAME)
AbilityFlame.prototype.constructor = AbilityFlame
//uAbility._allAbilityNames.push(uAbility.ABILITY_FLAME)

module.exports = AbilityFlame