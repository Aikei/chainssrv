var uPrefs = require('../preferences')
var uDbg = require('../debug')
var uAbility = require('./ability')
var uConsts = require('../constants')
var uEffects = require('./effects')
var uBaseAbilityApplyBallEffect = require('./base/baseAbilityApplyBallEffect')

var AbilityStoneOfHealth = function()
{
   uBaseAbilityApplyBallEffect.call(this,uAbility.ABILITY_STONE_OF_HEALTH,uEffects.EFFECT_STONE_OF_HEALTH)
   this.cost[uConsts.BALL_GREEN] = 3
   this.color = uConsts.BALL_GREEN
   this.damage = 2   
}

AbilityStoneOfHealth.prototype = new uBaseAbilityApplyBallEffect(uAbility.ABILITY_STONE_OF_HEALTH)

AbilityStoneOfHealth.prototype.Use = function(allTargets, game, args)
{
   this.SetEffectArguments(this.owner,this.damage,this.owner)
   uBaseAbilityApplyBallEffect.prototype.Use.call(this,allTargets,game,args)
}

AbilityStoneOfHealth.prototype.constructor = AbilityStoneOfHealth
uAbility._allAbilityNames.push(uAbility.ABILITY_STONE_OF_HEALTH)

module.exports = AbilityStoneOfHealth