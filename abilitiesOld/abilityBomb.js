var uPrefs = require('../preferences')
var uDbg = require('../debug')
var uAbility = require('./ability')
var uConsts = require('../constants')
var uEffects = require('./effects')
var uBaseAbilityApplyBallEffect = require('./base/baseAbilityApplyBallEffect')

var AbilityBomb = function()
{
   uBaseAbilityApplyBallEffect.call(this,uAbility.ABILITY_BOMB,uEffects.EFFECT_BOMB)
   this.cost[uConsts.BALL_PURPLE] = 4
   this.color = uConsts.BALL_PURPLE
   this.damage = 10
   this.time = 5
   //this.hiddenSelect = true   
}

AbilityBomb.prototype = new uBaseAbilityApplyBallEffect(uAbility.ABILITY_BOMB)

AbilityBomb.prototype.Use = function(allTargets, game, args)
{
   this.SetEffectArguments(this.time, this.damage, game.GetEnemyOfMovingPlayer(), this.owner)
   uBaseAbilityApplyBallEffect.prototype.Use.call(this,allTargets,game,args)
}

AbilityBomb.prototype.constructor = AbilityBomb
uAbility._allAbilityNames.push(uAbility.ABILITY_BOMB)

module.exports = AbilityBomb