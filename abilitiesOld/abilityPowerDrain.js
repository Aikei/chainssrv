var uPrefs = require('../preferences')
var uDbg = require('../debug')
var uAbility = require('./ability')
var uConsts = require('../constants')
var uEffects = require('./effects')

var thisAbility = function()
{
   uAbility.Ability.call(this,uAbility.ABILITY_POWER_DRAIN)
   this.shortestChain = 5
   this.receiveMana = 4   
   this.minAllTargets = this.shortestChain
   this.cost[uConsts.BALL_GREEN] = 1
   this.color = uConsts.BALL_GREEN   
   
   this.Use = function(allTargets, game, args)
   {
      this.BaseUse()     
      var color = args[0]
      for (var i = 0; i < allTargets.length; i++)
      {
         allTargets[i].AddEffect(new uEffects.EffectFrozen(1))
      }
      
      for (var key in this.owner.abilities)
      {
         if (this.owner.abilities[key].color === color)
         {
            this.owner.abilities[key].Charge(this.receiveMana)
            break
         }
      }
   }   
}

thisAbility.prototype = new uAbility.Ability(uAbility.ABILITY_POWER_DRAIN)

thisAbility.prototype.GetAllTargets = function(targets, game)
{   
   return this.GetTargetChain(targets, game)
}

thisAbility.prototype.constructor = thisAbility
uAbility._allAbilityNames.push(uAbility.ABILITY_POWER_DRAIN)

module.exports = thisAbility