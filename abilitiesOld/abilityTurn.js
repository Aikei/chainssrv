var uPrefs = require('../preferences')
var uDbg = require('../debug')
var uAbility = require('./ability')
var uConsts = require('../constants')

var AbilityTurn = function()
{
   uAbility.Ability.call(this,uAbility.ABILITY_TURN)
   this.cost[uConsts.BALL_PURPLE] = 4
   this.color = uConsts.BALL_PURPLE
   
   this.GetAllTargets = function(targets, game)
   {
      var allTargets = []
      for (var i = 0; i < targets.length; i++)
         allTargets.push(game.field[targets[i].x][targets[i].y])
      return allTargets
   }
   
   this.Use = function(allTargets, game, args)
   {
      this.BaseUse()  
      for (var i = 0; i < allTargets.length; i++)
      {
         uDbg.Log("Setting target",i,"type to",args[0])
         allTargets[i].type = args[0]
      }
   }   
}
AbilityTurn.prototype = new uAbility.Ability(uAbility.ABILITY_TURN)
AbilityTurn.prototype.constructor = AbilityTurn
uAbility._allAbilityNames.push(uAbility.ABILITY_TURN)

module.exports = AbilityTurn