var uPrefs = require('../preferences')
var uDbg = require('../debug')
var uAbility = require('./ability')
var uConsts = require('../constants')
var uBaseAbilityDestroyOrRemoveBalls = require('./base/baseAbilityDestroyOrRemoveBalls')

var thisAbility = function()
{
   uBaseAbilityDestroyOrRemoveBalls.call(this,uAbility.ABILITY_METEORITE_RAIN)
   this.cost[uConsts.BALL_RED] = 8
   this.color = uConsts.BALL_RED
   this.number = 10
   this.maxTargets = 10   
}

thisAbility.prototype = new uBaseAbilityDestroyOrRemoveBalls(uAbility.ABILITY_METEORITE_RAIN)

thisAbility.prototype.GetAllTargets = function(targets, game)
{
   uDbg.Log("Getting all targets")
   return game.GetRandomBalls(this.number)
}   

thisAbility.prototype.constructor = thisAbility
uAbility._allAbilityNames.push(uAbility.ABILITY_METEORITE_RAIN)

module.exports = thisAbility


// var AbilityMeteoriteRain = function()
// {
   // uAbility.Ability.call(this,uAbility.ABILITY_METEORITE_RAIN)
   // this.cost[uConsts.BALL_RED] = 8
   // this.color = uConsts.BALL_RED
   // this.number = 10
   // this.maxTargets = 10
   // this.GetAllTargets = function(targets, game)
   // {
      // uDbg.Log("Getting all targets")
      // var allTargets = game.GetRandomBalls(this.number)
      // uDbg.Log("Returning all targets: ",allTargets)
      // return allTargets
   // }
   
   // this.Use = function(allTargets, game, args)
   // {
      // this.BaseUse()  
      // game.DamageEnemy(allTargets)
      // game.ChargeAbilities(allTargets)
      // game.SendRefreshPlayerData()
      // game.JustRemoveBalls(allTargets)
      // game.MoveBallsAfterRemoving()
   // }   
// }
// AbilityMeteoriteRain.prototype = new uAbility.Ability(uAbility.ABILITY_METEORITE_RAIN)
// AbilityMeteoriteRain.prototype.constructor = AbilityMeteoriteRain
// uAbility._allAbilityNames.push(uAbility.ABILITY_METEORITE_RAIN)

// module.exports = AbilityMeteoriteRain