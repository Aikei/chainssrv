var uPrefs = require('../preferences')
var uDbg = require('../debug')
var uAbility = require('./ability')
var uConsts = require('../constants')
var uEffects = require('./effects')
var uBaseAbilityApplyBallEffect = require('./base/baseAbilityApplyBallEffect')

var AbilityFreeze = function()
{
   uBaseAbilityApplyBallEffect.call(this,uAbility.ABILITY_FREEZE,uEffects.EFFECT_FROZEN)
   this.cost[uConsts.BALL_PURPLE] = 3
   this.color = uConsts.BALL_PURPLE
   this.time = 2
   this.hiddenSelect = true   
}

AbilityFreeze.prototype = new uBaseAbilityApplyBallEffect(uAbility.ABILITY_FREEZE)

AbilityFreeze.prototype.Use = function(allTargets, game, args)
{
   this.SetEffectArguments(this.time)
   uBaseAbilityApplyBallEffect.prototype.Use.call(this,allTargets,game,args)
}

AbilityFreeze.prototype.GetAllTargets = function(targets, game)
{
   return this.GetTargetNeighbours(targets,game)
}

AbilityFreeze.prototype.constructor = AbilityFreeze
uAbility._allAbilityNames.push(uAbility.ABILITY_FREEZE)

module.exports = AbilityFreeze