//abilityPurpleRage.js

var uPrefs = require('../preferences')
var uDbg = require('../debug')
var uAbility = require('./ability')
var uAbilityRage = require('./abilityRage')
var uConsts = require('../constants')
var uGlobalEf = require('./globalEffects')

var AbilityPurpleRage = function()
{
   uAbilityRage.call(this,uAbility.ABILITY_PURPLE_RAGE, uConsts.BALL_PURPLE)
   this.color = uConsts.BALL_PURPLE
}

AbilityPurpleRage.prototype = new uAbilityRage(uAbility.ABILITY_PURPLE_RAGE, uConsts.BALL_PURPLE)
AbilityPurpleRage.prototype.constructor = AbilityPurpleRage
//uAbility._allAbilityNames.push(uAbility.ABILITY_PURPLE_RAGE)

module.exports = AbilityPurpleRage