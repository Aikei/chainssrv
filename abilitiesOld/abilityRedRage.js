//abilityRedRage.js

var uPrefs = require('../preferences')
var uDbg = require('../debug')
var uAbility = require('./ability')
var uAbilityRage = require('./abilityRage')
var uConsts = require('../constants')
var uGlobalEf = require('./globalEffects')

var AbilityRedRage = function()
{
   uAbilityRage.call(this, uAbility.ABILITY_RED_RAGE, uConsts.BALL_RED)
   this.color = uConsts.BALL_RED  
}

AbilityRedRage.prototype = new uAbilityRage(uAbility.ABILITY_RED_RAGE, uConsts.BALL_RED)
AbilityRedRage.prototype.constructor = AbilityRedRage
//uAbility._allAbilityNames.push(uAbility.ABILITY_RED_RAGE)

module.exports = AbilityRedRage