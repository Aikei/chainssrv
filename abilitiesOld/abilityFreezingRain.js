//abilityFreezingRain.js

var uPrefs = require('../preferences')
var uDbg = require('../debug')
var uAbility = require('./ability')
var uConsts = require('../constants')
var uGlobalEf = require('./globalEffects')
var uBaseAbilityApplyGlobalEffect = require('./base/baseAbilityApplyGlobalEffect')

var thisAbility = function()
{
   uBaseAbilityApplyGlobalEffect.call(this,uAbility.ABILITY_FREEZING_RAIN,uGlobalEf.EFFECT_FREEZING_RAIN)
   this.cost[uConsts.BALL_BLUE] = 3   
   this.color = uConsts.BALL_BLUE      
}

thisAbility.prototype = new uBaseAbilityApplyGlobalEffect(uAbility.ABILITY_FREEZING_RAIN,uGlobalEf.EFFECT_FREEZING_RAIN)
thisAbility.prototype.constructor = thisAbility
uAbility._allAbilityNames.push(uAbility.ABILITY_FREEZING_RAIN)

module.exports = thisAbility