//ability.js

var uPrefs = require('../preferences')
var uDbg = require('../debug')
var uAbility = require('./ability')
var uConsts = require('../constants')
var uBaseAbilityDestroyOrRemoveBalls = require('./base/baseAbilityDestroyOrRemoveBalls')

var thisAbility = function()
{
   uBaseAbilityDestroyOrRemoveBalls.call(this,uAbility.ABILITY_CHAIN_LIGHTNING)
   this.cost[uConsts.BALL_BLUE] = 4
   this.color = uConsts.BALL_BLUE  
   this.number = 3   //number of additional random balls
}

thisAbility.prototype = new uBaseAbilityDestroyOrRemoveBalls(uAbility.ABILITY_CHAIN_LIGHTNING)

thisAbility.prototype.GetAllTargets = function(targets, game)
{
   uDbg.Log("Getting all targets")
   var allTargets = []
   for (var i = 0; i < targets.length; i++)
   {
      allTargets.push(game.field[targets[i].x][targets[i].y])
   }
   allTargets = game.GetRandomBalls(this.number,allTargets)
   uDbg.Log("Returning all targets: ",allTargets)
   return allTargets
}   

thisAbility.prototype.constructor = thisAbility
uAbility._allAbilityNames.push(uAbility.ABILITY_CHAIN_LIGHTNING)

module.exports = thisAbility