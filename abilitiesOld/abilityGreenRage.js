//abilityGreenRage.js

var uPrefs = require('../preferences')
var uDbg = require('../debug')
var uAbility = require('./ability')
var uAbilityRage = require('./abilityRage')
var uConsts = require('../constants')
var uGlobalEf = require('./globalEffects')

var AbilityGreenRage = function()
{
   uAbilityRage.call(this,uAbility.ABILITY_GREEN_RAGE, uConsts.BALL_GREEN)
   this.color = uConsts.BALL_GREEN
}

AbilityGreenRage.prototype = new uAbilityRage(uAbility.ABILITY_GREEN_RAGE, uConsts.BALL_GREEN)
AbilityGreenRage.prototype.constructor = AbilityGreenRage
//uAbility._allAbilityNames.push(uAbility.ABILITY_GREEN_RAGE)

module.exports = AbilityGreenRage