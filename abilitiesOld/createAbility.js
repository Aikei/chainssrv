var uDbg = require('../debug')
var uAbility = require('./ability')
var uMisc = require('../misc')
var uPrefs = require('../preferences')

// var uAbilityFire = require('./abilityFire')
// var uAbilityFreeze = require('./abilityFreeze')
// var uAbilityTurn = require('./abilityTurn')
// var uAbilityFlame = require('./abilityFlame')
// var uAbilityHeal = require('./abilityHeal')
// var uAbilityFrost = require('./abilityFrost')
// var uAbilityPoison = require('./abilityPoison')
// var uAbilityBomb = require('./abilityBomb')
// var uAbilityMeteoriteRain = require('./abilityMeteoriteRain')
// var uAbilityShatter = require('./abilityShatter')
// var uAbilitySeeNext = require('./abilitySeeNext')
// var uAbilityEarthquake = require('./abilityEarthquake')
// var uAbilityStoneOfHealth = require('./abilityStoneOfHealth')
// var uAbilityShieldOfIce = require('./abilityShieldOfIce')
// var uAbilityDarkMadness = require('./abilityDarkMadness')
// var uAbilityChainLightning = require('./abilityChainLightning')
// var uAbilityGoblinMine = require('./abilityGoblinMine')
// var uAbilityFreezingRain = require('./abilityFreezingRain')
// var uAbilityPowerOfAbyss = require('./abilityPowerOfAbyss')
// var uAbilityGreenRage = require('./abilityGreenRage')
// var uAbilityRedRage = require('./abilityRedRage')
// var uAbilityBlueRage = require('./abilityBlueRage')
// var uAbilityPurpleRage = require('./abilityPurpleRage')
// var uAbilityInfection = require('./abilityInfection')
// var uAbilityMatchThree = require('./abilityMatchThree')
// var uAbilityRemoveBall = require('./abilityRemoveBall')

var fs = require('fs')


var abilityUnits = {}
var abilityTypesByColor = []


module.exports.Init = function()
{
   var abilityFiles = fs.readdirSync("./abilities")
   for (var i = 0; i < uPrefs.numberOfManas; i++)
      abilityTypesByColor.push([])
   for (var i = 0; i < abilityFiles.length; i++)
   {
      if (abilityFiles[i].length > 10 && abilityFiles[i].slice(0,7) === "ability")
      {
         var name = abilityFiles[i].split('ability')
         //uDbg.Log("_ii","name 1:",name[1])
         var u = require('./'+abilityFiles[i])
         name = name[1].slice(0,name[1].length-3)
         var used = false
         for (var j = 0; j < uAbility._allAbilityNames.length; j++)
         {
            if (uAbility._allAbilityNames[j] == name)
            {
               used = true
               break
            }
         }
         if (used)
         {
            abilityUnits[name] = u
            var o = new u()
            if (o.color !== undefined)
               abilityTypesByColor[o.color].push(name)
         }
      }
   }
}

module.exports.Create = function(type,randomOfColor,butNotType)
{
   var uThisAbility
   uDbg.Log('randomOfColor: ',randomOfColor)
   if (randomOfColor !== undefined)
   {
      while(true)
      {
         var num = uMisc.Random(0,abilityTypesByColor[randomOfColor].length) 
         type = abilityTypesByColor[randomOfColor][num]
         uDbg.Log('type: ',type)
         if (butNotType === undefined || type !== butNotType)
            break
      }
   }
   uThisAbility = abilityUnits[type]
   return new uThisAbility()
}