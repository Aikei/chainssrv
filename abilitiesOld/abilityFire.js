//ability.js

var uPrefs = require('../preferences')
var uDbg = require('../debug')
var uAbility = require('./ability')
var uConsts = require('../constants')
var uBaseAbilityDestroyOrRemoveBalls = require('./base/baseAbilityDestroyOrRemoveBalls')

var thisAbility = function()
{
   uBaseAbilityDestroyOrRemoveBalls.call(this,uAbility.ABILITY_FIRE)
   this.cost[uConsts.BALL_RED] = 6
   this.color = uConsts.BALL_RED    
}

thisAbility.prototype = new uBaseAbilityDestroyOrRemoveBalls(uAbility.ABILITY_FIRE)

thisAbility.prototype.GetAllTargets = function(targets, game)
{
   uDbg.Log("Getting all targets")
   var allTargets = []
   for (var i = 0; i < targets.length; i++)
   {
      var thisBall = game.field[targets[i].x][targets[i].y]
      var v = game.GetCrossNeighbours(thisBall)
      v.push(thisBall)
      allTargets = allTargets.concat(v)
   }
   uDbg.Log("Returning all targets: ",allTargets)
   return allTargets
}   

thisAbility.prototype.constructor = thisAbility
uAbility._allAbilityNames.push(uAbility.ABILITY_FIRE)

module.exports = thisAbility