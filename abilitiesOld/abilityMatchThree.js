//abilityMatchThree

var uPrefs = require('../preferences')
var uDbg = require('../debug')
var uAbility = require('./ability')
var uConsts = require('../constants')
var uBaseAbilityDestroyOrRemoveBalls = require('./base/baseAbilityDestroyOrRemoveBalls')

var thisAbility = function()
{
   uBaseAbilityDestroyOrRemoveBalls.call(this,uAbility.ABILITY_MATCH_THREE)
   this.cost[uConsts.BALL_GREEN] = 4
   this.color = uConsts.BALL_GREEN
   this.maxTargets = uPrefs.fieldWidth*uPrefs.fieldHeight   
}

thisAbility.prototype = new uBaseAbilityDestroyOrRemoveBalls(uAbility.ABILITY_MATCH_THREE)

thisAbility.prototype.GetAllTargets = function(targets, game)
{
   uDbg.Log("Getting all targets")
   var result = []
   var tempBallsX = []
   var tempBallsY = []
   
   for (var i = 0; i < game.field.length; i++)
   {
      tempBallsX.push(game.field[i].concat())
      tempBallsY.push(game.field[i].concat())
   }
   
   var balls = game.field
   
   for (var i = 0; i < balls.length; i++)
   {
      for (var j = 0; j < balls[i].length; j++)
      {
         for (var axis = 0; axis < 2; axis++) //axis: 0 = x, 1 = y
         {
            var nFound = 1 //number of found matching balls of the same color
            var checkx = i
            var checky = j
            for (var k = 1; k < 6; k++)
            {
               if (axis === 0)
                  checkx++
               else
                  checky++
               if (checkx < balls.length && checky < balls[0].length && balls[i][j].type === balls[checkx][checky].type
                  && ((axis === 0 && tempBallsX[i][j] !== null) ||  (axis === 1 && tempBallsY[i][j] !== null)) )
               {
                  nFound++
               }
               else if (nFound < 3)
               {
                  break
               }
               else
               {
                  var v = []
                  for (var a = 0; a < nFound; a++)
                  {
                     if (axis === 0)
                     {
                        tempBallsX[i + a][j] = null
                        v.push(balls[i + a][j])
                     }
                     else
                     {
                        tempBallsY[i][j + a] = null
                        v.push(balls[i][j + a])
                     }
                  }
                  result.push(v)
                  break
               }
            }
         }
      }
   }
   var fullResult = []
   for (var i = 0; i < result.length; i++)
   {
      fullResult = fullResult.concat(result[i])
   }
   fullResult = game.RemoveSameBalls(fullResult)
   return fullResult
}   

thisAbility.prototype.constructor = thisAbility
uAbility._allAbilityNames.push(uAbility.ABILITY_MATCH_THREE)

module.exports = thisAbility