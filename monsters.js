var uPlayer = require('./player.js')
var uAbility = require('./abilities/ability.js')
var uMisc = require('./misc.js')
var uItem = require('./abilities/item.js')

var difficultyEasy = 0
var difficultyMedium = 1
var difficultyHard = 2

var difficultyProbability = [ 50, 80, 100 ]

var ex = module.exports

ex.MakeMonster = function(monsterPlayer,pl)
{
   if (pl.profile.mk < 2)
   {
      var difficulty = 0
   }
   else if (pl.profile.mk === 2)
   {
      difficulty = 1
   }
   else
   {
      difficulty = uMisc.Random(0,100)
      if (difficulty < difficultyProbability[0])
         difficulty = 0
      else if (difficulty < difficultyProbability[1])
         difficulty = 1
      else
         difficulty = 2
      if (difficulty === 2 && pl.profile.lv < 5)
         difficulty = 1
   }
   do
   {
      var rht = uMisc.Random(0,uPlayer.monstersByDifficulty[difficulty].length)
      var ht = uPlayer.monstersByDifficulty[difficulty][rht]
   } while (pl.lastMonsterType && pl.lastMonsterType === ht)
   pl.lastMonsterType = ht
   ex.MakeSpecificMonster(monsterPlayer,ht)
}

ex.MakeSpecificMonster = function(player,ht)
{
   player.profile.socialProfile.type = "mon"
   player.profile.ht = ht
   player.monster = true
   switch(player.profile.ht)
   {
      case uPlayer.HTYPE_SLIME_PINK:
         player.hpMax = 50
         player.absNum = 0
         player.gain.co = 1
         player.profile.socialProfile.name = "Pink Slime"
         player.dropChance = 1
         player.possDrop = [ uItem.ITEM_MINOR_HEALTH_POTION, uItem.ITEM_MINOR_MANA_POTION ]
         player.rareChance = 10
         player.rareDrop = [ uItem.ITEM_KNIFE, uItem.ITEM_MINOR_DYNAMITE  ]
         break
         
      case uPlayer.HTYPE_SLIME_GREEN:
         player.hpMax = 50
         player.absNum = 0
         player.gain.co = 1
         player.profile.socialProfile.name = "Green Slime"
         player.dropChance = 1
         player.possDrop = [ uItem.ITEM_MINOR_HEALTH_POTION, uItem.ITEM_MINOR_MANA_POTION ]
         player.rareChance = 10
         player.rareDrop = [ uItem.ITEM_SYMBOL_OF_MANA, uItem.ITEM_KNIFE ]         
         break
         
      case uPlayer.HTYPE_BAT:
         player.hpMax = 40
         player.absNum = uMisc.Random(0,2)
         player.gain.co = 1
         player.profile.socialProfile.name = "Bat"
         player.dropChance = 1
         player.possDrop = [ uItem.ITEM_MINOR_HEALTH_POTION, uItem.ITEM_MINOR_MANA_POTION ]         
         player.rareChance = 10
         player.rareDrop = [ uItem.ITEM_SYMBOL_OF_MANA, uItem.ITEM_MINOR_DYNAMITE, uItem.ITEM_DYE ] 
         break
         
      case uPlayer.HTYPE_EYE_BAT:
         player.hpMax = 40
         player.absNum = uMisc.Random(0,2)
         player.gain.co = 1
         player.profile.socialProfile.name = "One-Eyed Bat"
         player.dropChance = 1
         player.possDrop = [ uItem.ITEM_MINOR_HEALTH_POTION, uItem.ITEM_MINOR_MANA_POTION ]         
         player.rareChance = 10
         player.rareDrop = [ uItem.ITEM_MANA_POTION, uItem.ITEM_SYMBOL_OF_MANA  ] 
         break
         
      case uPlayer.HTYPE_SKELETON_1:
         player.hpMax = 100
         player.absNum = uMisc.Random(1,3)
         player.gain.co = 2
         player.profile.socialProfile.name = "Axe Skeleton"
         player.dropChance = 2
         player.possDrop = [ uItem.ITEM_HEALTH_VIAL, uItem.ITEM_MANA_POTION, uItem.ITEM_KNIFE ]
         player.rareChance = 10
         player.rareDrop = [ uItem.ITEM_DYNAMITE_BUNDLE, uItem.ITEM_MINOR_LOCKET_OF_HEALTH, uItem.ITEM_MINOR_FALLING_STAR, uItem.ITEM_DYE ]         
         //player.possAbs = [ uAbility.ABILITY_POISON, uAbility.ABILITY_MATCH_THREE, uAbility.ABILITY_CHAIN_LIGHTNING, uAbility.ABILITY_REMOVE_STONE, uAbility.ABILITY_TURN ]
         break
         
      case uPlayer.HTYPE_SKELETON_2:
         player.hpMax = 100
         player.absNum = uMisc.Random(1,3)
         player.gain.co = 2
         player.profile.socialProfile.name = "Spear Skeleton"
         player.dropChance = 2
         player.possDrop = [ uItem.ITEM_HEALTH_VIAL, uItem.ITEM_MANA_POTION, uItem.ITEM_DYNAMITE, uItem.ITEM_MOLOTOV_COCKTAIL, uItem.ITEM_POTION_OF_DARKNESS, uItem.ITEM_POTION_OF_SHIELD, uItem.ITEM_KNIFE ]
         player.rareChance = 10
         player.rareDrop = [ uItem.ITEM_DYNAMITE_BUNDLE, uItem.ITEM_MINOR_SYMBOL_OF_MANA_DRAIN, uItem.ITEM_GREATER_SYMBOL_OF_MANA, uItem.ITEM_BOW, uItem.ITEM_MEDIUM_SYMBOL_OF_SNOW ]
         //player.possAbs = [ uAbility.ABILITY_POISON, uAbility.ABILITY_METEORITE_RAIN, uAbility.ABILITY_FIRE, uAbility.ABILITY_GOBLIN_MINE, uAbility.ABILITY_SHIELD_OF_ICE ]
         break
         
      case uPlayer.HTYPE_ORC:
         player.hpMax = 80
         player.absNum = uMisc.Random(2,4)
         player.gain.co = 2
         player.profile.socialProfile.name = "Orc"
         player.dropChance = 2
         player.possDrop = [ uItem.ITEM_HEALTH_VIAL, uItem.ITEM_MANA_POTION, uItem.ITEM_DYNAMITE, uItem.ITEM_POTION_OF_DARKNESS, uItem.ITEM_POTION_OF_SHIELD, uItem.ITEM_KNIFE, uItem.ITEM_MINOR_FALLING_STAR  ]
         player.rareChance = 10
         player.rareDrop = [ uItem.ITEM_DYNAMITE_BUNDLE, uItem.ITEM_DYE, uItem.ITEM_SHIELD, uItem.ITEM_BOW ]         
         //player.possAbs = [ uAbility.ABILITY_TURN, uAbility.ABILITY_SHATTER, uAbility.ABILITY_REMOVE_STONE, uAbility.ABILITY_DARK_MADNESS, uAbility.ABILITY_BOMB ]
         break
      
      case uPlayer.HTYPE_OGRE:
         player.hpMax = 140
         player.absNum = 3
         player.gain.co = 5
         player.profile.socialProfile.name = "Ogre"
         player.dropChance = 5
         player.possDrop = [ uItem.ITEM_DYNAMITE_BUNDLE, uItem.ITEM_MINOR_SYMBOL_OF_SNOW, uItem.ITEM_MINOR_LOCKET_OF_HEALTH, uItem.ITEM_MINOR_FALLING_STAR ]        
         player.rareChance = 5     
         player.rareDrop = [ uItem.ITEM_SYMBOL_OF_WARLOCK, uItem.ITEM_MEDIUM_SYMBOL_OF_SNOW, uItem.ITEM_LOCKET_OF_HEALTH, uItem.ITEM_FALSE_PLANS, uItem.ITEM_GREATER_SYMBOL_OF_MANA ]
         //player.possAbs = [ uAbility.ABILITY_HEAL, uAbility.ABILITY_CHAIN_LIGHTNING, uAbility.ABILITY_METEORITE_RAIN, uAbility.FIRE, uAbility.ABILITY_SHIELD_OF_ICE, uAbility.ABILITY_DARK_MADNESS, uAbility.ABILITY_STONE_OF_HEALTH ]
         break
         
      case uPlayer.HTYPE_DARK_KNIGHT:
         player.hpMax = 120
         player.absNum = 4
         player.gain.co = 5
         player.profile.socialProfile.name = "Dark Knight"
         player.dropChance = 5
         player.possDrop = [ uItem.ITEM_DYNAMITE_BUNDLE, uItem.ITEM_DYE, uItem.ITEM_POTION_OF_DARKNESS, uItem.ITEM_MOLOTOV_COCKTAIL, uItem.ITEM_MINOR_FALLING_STAR ]
         player.rareChance = 5
         player.rareDrop = [ uItem.ITEM_SYMBOL_OF_WARLOCK, uItem.ITEM_FALLING_STAR, uItem.ITEM_SYMBOL_OF_MANA_DRAIN, uItem.ITEM_FALSE_PLANS, uItem.ITEM_GREATER_SYMBOL_OF_MANA ]
         //player.possAbs = [ uAbility.ABILITY_HEAL, uAbility.ABILITY_CHAIN_LIGHTNING, uAbility.ABILITY_METEORITE_RAIN, uAbility.FIRE, uAbility.ABILITY_SHIELD_OF_ICE, uAbility.ABILITY_DARK_MADNESS, uAbility.ABILITY_STONE_OF_HEALTH ]
         break      
   }   
}