var uPlayer = require('./player')
var uPrefs = require('./preferences')

var uFs = require('fs')
var net = require('net')

var ex = module.exports

ex.Init = function()
{ 
   var server = net.createServer(function(connection) 
   {
       connection.on('data', function(data) 
       {
           var str = data.toString()
           if (str.charAt(0) === "\\" && str.charAt(1) === "e")
           {
              str = str.slice(2)
              eval(str)
           }
       })
   })
   
   if (uFs.existsSync('cmdin'))
   {
      uFs.unlinkSync('cmdin')
   }
   
   server.listen('cmdin')
   
   process.on('SIGTERM', function() 
   {
       server.close()
   })
   
   process.on('exit', function() 
   {
       server.close()
   })
}