module.exports.MyDate = function MyDate(time,withClock)
{
   var jsDate = new Date(time*1000)
   this.d = jsDate.getUTCDate()
   this.m = jsDate.getUTCMonth()+1
   this.y = jsDate.getUTCFullYear()
   if (withClock)
   {
      this.h = jsDate.getUTCHours()
      this.mins = jsDate.getUTCMinutes()
      this.s = jsDate.getUTCSeconds()
   }
}

module.exports.GetMyDateString = function(myDate)
{
   return myDate.d.toString()+"."+myDate.m.toString()+"."+myDate.y.toString()
}

module.exports.GetMyDateTimeString = function(myDate)
{
   return myDate.h.toString()+":"+myDate.mins.toString()+":"+myDate.s.toString()
}

module.exports.GetFullTimeString = function(myDate)
{
   return module.exports.GetMyDateString(myDate)+", "+module.exports.GetMyDateTimeString(myDate)
}