var uMisc = require('./misc.js')
var uConsts = require('./constants.js')
var uItem = require('./abilities/item.js')

var ex = module.exports

ex.QUEST_TYPE_KILL_MONSTERS = 0
ex.QUEST_TYPE_WIN_PVP_MATCHES = 1
ex.QUEST_TYPES_NUMBER = 2

ex.DIFFICULTY_EASY = 0
ex.DIFFICULTY_MEDIUM = 1
ex.DIFFICULTY_HARD = 2

ex.Quest = function(type)
{
   this.t = type
   this.st = -1
   this.df = ex.DIFFICULTY_EASY
   this.p = 0        //proress
   this.g = 0        //goal
   this.co_rw = 0    //coins reward
   this.it_rw = 1    //items reward (number of items)
   this.nwq = true   //quest of new type (with items drop)
}

ex.FirstQuest = function()
{
   this.t = ex.QUEST_TYPE_KILL_MONSTERS
   this.st = -1
   this.df = ex.DIFFICULTY_EASY
   this.p = 0
   this.g = 10
   this.co_rw = this.g*10
   this.it_rw = 2
   this.nwq = true   //quest of new type (with items drop)
}

ex.QuestHandler = function(quest)
{
   this.quest = quest
   this.ticked = false
}

ex.QuestHandler.prototype.OnEndGame = function(enemy,result)
{
   this.ticked = false
   if (this.quest)
   {
      switch(this.quest.t)
      {
         case ex.QUEST_TYPE_KILL_MONSTERS:
            if (enemy.monster && result === uConsts.RESULT_WON && 
                  this.quest.st === -1 || this.quest.st === enemy.profile.ht)
            {
               this.ticked = true
               this.quest.p++
            }
            break
            
         case ex.QUEST_TYPE_WIN_PVP_MATCHES:
            if (!enemy.monster && result === uConsts.RESULT_WON)
            {
               this.ticked = true
               this.quest.p++
            }
            break
      }
   }
}

ex.QuestHandler.prototype.MaybeComplete = function(owner)
{
   if (this.quest && this.quest.p >= this.quest.g)
   {   
      owner.AddCoins(this.quest.co_rw)
      this.quest = null
      return true
   }
   return false
}

var normalItems = 
[ 
   uItem.ITEM_KNIFE,
   uItem.ITEM_MINOR_DYNAMITE, 
   uItem.ITEM_MANA_POTION, 
   uItem.ITEM_HEALTH_VIAL, 
   uItem.ITEM_DYNAMITE_BUNDLE, 
   uItem.ITEM_DYNAMITE, 
   uItem.ITEM_MOLOTOV_COCKTAIL, 
   uItem.ITEM_POTION_OF_DARKNESS, 
   uItem.ITEM_POTION_OF_SHIELD, 
   uItem.ITEM_DYE,
   uItem.ITEM_SYMBOL_OF_MANA, 
   uItem.ITEM_MINOR_HEALTH_POTION,
   uItem.ITEM_MINOR_MANA_POTION
]

var rareItems =
[
   uItem.ITEM_SYMBOL_OF_WARLOCK,
   uItem.ITEM_SYMBOL_OF_MANA_DRAIN,
   uItem.ITEM_GREATER_SYMBOL_OF_MANA,
   uItem.ITEM_FALSE_PLANS,
   uItem.ITEM_LOCKET_OF_HEALTH,
   uItem.ITEM_FALLING_STAR,
   uItem.ITEM_SHIELD,
   uItem.ITEM_MINOR_SYMBOL_OF_SNOW,
   uItem.ITEM_MINOR_FALLING_STAR,
   uItem.ITEM_BOW
]

ex.GetItemRewards = function(num)
{
   var itms = []
   for (var i = 0; i < num; i++)
   {
      if (uMisc.Random(0,100) < 10)
      {
         itms.push(rareItems[uMisc.Random(0,rareItems.length-1)])
      }
      else
      {
         itms.push(normalItems[uMisc.Random(0,normalItems.length-1)])
      }
   }
   return itms
}

ex.GetQuest = function(type,df)
{
   var type = type || uMisc.Random(0,ex.QUEST_TYPES_NUMBER)
   var q = new ex.Quest(type)
   
   if (df)
   {
      q.df = df
   }
   else
   {
      q.df = uMisc.Random(0,100)
      if (q.df < 50)
         q.df = ex.DIFFICULTY_EASY
      else if (q.df < 85)
         q.df = ex.DIFFICULTY_MEDIUM
      else
         q.df = ex.DIFFICULTY_HARD
   }
   
   switch(type)
   {
      case ex.QUEST_TYPE_KILL_MONSTERS:
         q.st = -1   //any monster
         if (q.df === ex.DIFFICULTY_EASY)
         {
            q.g = uMisc.Random(10, 20)
            q.it_rw = 2
         }
         else if (q.df === ex.DIFFICULTY_MEDIUM)
         {
            q.g = uMisc.Random(20, 30)
            q.it_rw = 3
         }
         else if (q.df === ex.DIFFICULTY_HARD)
         {
            q.g = uMisc.Random(30, 40)
            q.it_rw = 4
         }
         q.co_rw = q.g*5
         break
         
      case ex.QUEST_TYPE_WIN_PVP_MATCHES:     
         if (q.df === ex.DIFFICULTY_EASY)
         {
            q.g = uMisc.Random(3,6)
            q.it_rw = 2
         }
         else if (q.df === ex.DIFFICULTY_MEDIUM)
         {
            q.g = uMisc.Random(6, 9)
            q.it_rw = 3
         }
         else if (q.df === ex.DIFFICULTY_HARD)
         {
            q.g = uMisc.Random(10, 15)
            q.it_rw = 4
         }
         q.co_rw = q.g*20
         break         
   }
   //test
   //q.g = 1
   return q
}