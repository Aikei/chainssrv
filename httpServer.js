var uHttp = require('http')
var uFs = require('fs')
var uDbg = require('./debug.js')

var httpServer = module.exports

httpServer.server = uHttp.createServer(function (request, response)
{
   uDbg.Log("_ii","Received http request: ", request.url)
   var strings = request.url.split('/')
   if (strings[1] === "cl")
   {      
      var strm = uFs.createReadStream(__dirname+request.url)
      strm.pipe(response)
   }   
})